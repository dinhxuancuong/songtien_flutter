import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:song_tien_app/src/presentation/password/password.dart';
import 'package:song_tien_app/src/presentation/policy/policy.dart';
import 'package:song_tien_app/src/presentation/widgets/widget_button_gradientAni.dart';
import '../../configs/configs.dart';
import '../presentation.dart';
import 'package:rxdart/rxdart.dart';
import 'package:dvhcvn/dvhcvn.dart' as dvhcvn;

class PasswordScreen extends StatefulWidget {
  @override
  _PasswordScreenState createState() => _PasswordScreenState();
}

class _PasswordScreenState extends State<PasswordScreen> {

  final passwordOldController = new BehaviorSubject<bool>();
  final passwordNewController = new BehaviorSubject<bool>();
  final passwordRetypeController = new BehaviorSubject<bool>();


  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    passwordOldController.close();
    passwordNewController.close();
    passwordRetypeController.close();
  }
@override
  void initState() {
    // TODO: implement initState
    super.initState();
    passwordOldController.sink.add(true);
    passwordNewController.sink.add(true);
    passwordRetypeController.sink.add(true);
  }
  @override
  Widget build(BuildContext context) {
    return BaseWidget<PasswordViewModel>(
        viewModel: PasswordViewModel(repository: Provider.of(context)),
        onViewModelReady: (viewModel) {
          viewModel.init();
        },
        builder: (context, viewModel, child) {
          return Scaffold(
            backgroundColor: Color(0xffbE8E8E8) ,
            body: SafeArea(
              child: Column(
                children: [
                SizedBox(
                  height:80,
                  child:  WidgetAppBar(
                  keyTitle: "Đổi Mật Khẩu",
                  trans: false,
                ),),
                 Expanded(child:  _buildBody(context))
                ],
              ),
            ),
          );
        });
  }

  Widget _buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(height: 15,),
          ////Mật khảu cũ
          buildOld(),
          ////Mật khảu cũ
          buildNews(),
          ////Nhập lại mật khẩu
          buildRetype(),
          SizedBox(
            height:140 ,
          ),
          buildButton(),
        ],
      ),
    );
  }

  ////Mật khảu cũ
  buildOld(){
   return StreamBuilder<bool>(
        stream: passwordOldController.stream,
        builder: (context, snapshot) {
          return WidgetTextField(
            obsText: snapshot.data??false,
            hintText: "Mật khẩu cũ",
            suffixIcon: IconButton(
              icon: Icon(
                Icons.remove_red_eye_outlined,
                size: 20,color: snapshot.data== false?Colors.black87:Colors.black26,
              ),
              onPressed: (){
                if(snapshot.data){
                  passwordOldController.sink.add(false);
                }else{
                  passwordOldController.sink.add(true);
                }
              },
            ),
            borderColor: Colors.green,
            mglr: 10,
          );
        }
    );
  }
  ////Mật khảu cũ
  buildNews(){
    return   StreamBuilder<bool>(
        stream: passwordNewController.stream,
        builder: (context, snapshot) {
          return WidgetTextField(
            obsText: snapshot.data,
            hintText: "Mật khẩu mới",
            suffixIcon: IconButton(
              icon: Icon(
                Icons.remove_red_eye_outlined,
                size: 20,color:snapshot.data== false?Colors.black87:Colors.black26,
              ),
              onPressed: (){
                if(snapshot.data){
                  passwordNewController.sink.add(false);
                }else{
                  passwordNewController.sink.add(true);
                }
              },
            ),
            borderColor: Colors.green,
            mglr: 10,
          );
        }
    );
  }
  ////Nhập lại mật khẩu
  buildRetype(){
    return    StreamBuilder<bool>(
        stream: passwordRetypeController.stream,
        builder: (context, snapshot) {
          return WidgetTextField(
            obsText: snapshot.data,
            hintText: "Nhập lại mật khẩu",
            suffixIcon: IconButton(
              icon: Icon(
                Icons.remove_red_eye_outlined,
                size: 20,color:snapshot.data == false?Colors.black87:Colors.black26,
              ),
              onPressed: (){
                if(snapshot.data){
                  passwordRetypeController.sink.add(false);
                }else{
                  passwordRetypeController.sink.add(true);
                }
              },
            ),
            borderColor: Colors.green,
            mglr: 10,
          );
        }
    );
  }

  buildButton() {
    return Padding(
      padding: const EdgeInsets.all(14.0),
      child: WidgetButtonGradientAni(
        trans: false,
        title: "Đồng ý",
        width: Get.width,
        colorStart: AppColors.primary,
        colorEnd: AppColors.primary,
        action: ()async => await showDialogLR(context),
        loading: false,
        textStyle: AppStyles.DEFAULT_MEDIUM.copyWith(
          color:  Colors.white,
        ),
      ),
    );
  }

  showDialogLR(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) => Dialog(
            elevation: 8,
            insetPadding: EdgeInsets.only(left: 10, right: 10),
            shape: RoundedRectangleBorder(
              side: BorderSide.none,
              borderRadius: BorderRadius.circular(16),
            ),
            backgroundColor: HexColor.fromHex("#fdfefe"),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                const SizedBox(
                  height: 30,
                ),
                Text(
                  "Bạn có chắc chắn thay đổi mật khẩu",
                  style: AppStyles.DEFAULT_MEDIUM.copyWith(fontSize: 16),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 15,
                ),
                // Padding(
                //   padding: const EdgeInsets.symmetric(horizontal: 8),
                //   child: Text(
                //     "thay đổi thông tin cá nhân?",
                //     style: AppStyles.DEFAULT_MEDIUM,
                //     textAlign: TextAlign.center,
                //   ),
                // ),
                const SizedBox(
                  height: 12,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    builCancel(),
                    const SizedBox(
                      width: 1,
                    ),
                    buildOk(),
                  ],
                )
              ],
            )));
  }

  //Nút hủy bỏ
  builCancel(){
  return  Expanded(
      child: InkWell(
        borderRadius:
        BorderRadius.only(bottomLeft: Radius.circular(16)),
        onTap: () => Navigator.pop(context, false),
        child: Container(
          decoration: BoxDecoration(
            color: Color(0xff445E37),
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(16)),
          ),
          height: 45,
          child: Center(
            child: Text(
              "Hủy bỏ",
              style: AppStyles.DEFAULT_MEDIUM_BOLD
                  .copyWith(color: Colors.white),
            ),
          ),
        ),
      ));
  }

  ///Nút ok
  buildOk(){
  return  Expanded(
      child: InkWell(
        borderRadius:
        BorderRadius.only(bottomLeft: Radius.circular(16)),
        onTap: () => Navigator.pop(context, true),
        child: Container(
          decoration: BoxDecoration(
              color: Color(0xff445E37),
              borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(16))),
          height: 45,
          child: Center(
            child: Text(
              "OK",
              style: AppStyles.DEFAULT_MEDIUM_BOLD
                  .copyWith(color: Colors.white),
            ),
          ),
        ),
      ));
  }
}
