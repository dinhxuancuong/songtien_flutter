import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:song_tien_app/src/presentation/news/news.dart';
import 'package:song_tien_app/src/presentation/policy/policy.dart';
import 'package:song_tien_app/src/resource/model/app_valid.dart';
import 'package:song_tien_app/src/resource/model/model.dart';
import '../../configs/configs.dart';
import '../presentation.dart';
class NewsScreen extends StatefulWidget {
  @override
  _NewsScreenScreenState createState() => _NewsScreenScreenState();
}

class _NewsScreenScreenState extends State<NewsScreen> {
  NewsViewModel _viewModel;
  final keyRefresh = GlobalKey<RefreshIndicatorState>();
  List<Address> data = [];
  var _controller = ScrollController();
  var _iVisible = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller.addListener(() {
      if (_controller.position.atEdge) {
        if (_controller.position.pixels > 0) {
          if (_iVisible) {
            setState(() {
              _iVisible = false;
            });
          } else {
            if (!_iVisible) {
              setState(() {
                _iVisible = true;
              });
            }
          }
        }
      }
    });
    loadList();
  }

  Future loadList() async {
    keyRefresh.currentState?.show();
    await Future.delayed(Duration(milliseconds: 4000));

    setState(() => this.data = listAdress);
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<NewsViewModel>(
        viewModel: NewsViewModel(repository: Provider.of(context)),
        onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
        builder: (context, viewModel, child) {
          return Scaffold(
            backgroundColor: Color(0xffEFEFEF),
            body: SafeArea(
              child: Column(
                children: [
                  SizedBox(height:80,
                    child: WidgetAppBar(keyTitle: "Tin Tức", trans: false,),),
                  Flexible(child: _buildScreen()),
                ],
              ),
            ),
          );
        });
  }

  _buildScreen() {
    return SingleChildScrollView(
      child: Column(
        children: [
          _buildHeader(),
          _buildBody(context),
        ],
      ),
    );
  }

  List<String> listV = [
    "Tất cả",
    "Sông Tiền",
    "Du lịch đảo",
    "Đảo xanh",
    "Đảo vàng"
  ];

  _buildHeader() {
    return Container(
      height: 55,
      alignment: Alignment.center,
      width: Get.width,
      child: ListView.builder(
          padding: EdgeInsets.only(top: 10, left: 15),
          itemCount: listV?.length,
          shrinkWrap: true,
          physics: ScrollPhysics(),
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            return WidgetButton(
              colorSide:_viewModel.isIndex == index
                  ? Color(0xffA43732)
                  : Color(0xffA1A1A1),
              text: listV[index],
              height: 45,
              color: _viewModel.isIndex == index
                  ? Color(0xffA43732)
                  : Color(0xffA1A1A1),
              horizontal: 2,
              onPress: () => _viewModel.getIndex(index),
            );
          }),
    );
  }

  Widget _buildBody(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      children: [
        StreamBuilder<int>(
            stream: _viewModel.checkController.stream,
            builder: (context, snapshot) {
              return Wrap(
                children: _checkButton(listAdress, snapshot.data, context),
              );
            }),
        SizedBox(
          height: 25,
        )
      ],
    );
  }


  List<Widget> _checkButton(List<Address> address, int index,
      BuildContext context) {
    switch (index) {
      case 0:
        return _listNew(listAdress, context);
        break;
      case 1:
        return [Text("helo")];
        break;
      case 2:
        return [Text("hi")];
        break;
      case 3:
        return [Text("good Job")];
        break;
      default:
        return [Text("What")];
        break;
    }
  }

  _listNew(List<Address> listAd, BuildContext context) {
    List<Widget> list = [];
    for (var item in listAd) {
      list.add(_formNews(item, context));
    }
    return list;
  }

  _formNews(Address address, BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(context, Routers.DetailsNews),
      child: Padding(
        padding: const EdgeInsets.only(left: 14, right: 14, top: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Image.asset(
                address.images,
                height: Get.height * 0.3,
                width: Get.width,
                fit: BoxFit.fill,
              ),
            ),
            SizedBox(
              height: 7,
            ),
            Text(address.title),
            SizedBox(
              height: 3,
            ),
            _rowText("${DateFormat("dd/MM/yyyy").format(DateTime.now())}"),
          ],
        ),
      ),
    );
  }

  Row _rowText(String name) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
          child: Text("${name}", style: TextStyle(color: Colors.black38)),
        ),
        Text(
          "Xem chi tiết",
          style: AppStyles.DEFAULT_SMALL.copyWith(color: AppColors.red),
        )
      ],
    );
  }
}