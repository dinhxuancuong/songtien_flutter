import 'package:flutter/material.dart';
import '../../resource/resource.dart';
import '../base/base.dart';
import 'package:rxdart/rxdart.dart';
class NewsViewModel extends BaseViewModel{

  final OtherRepository repository;
  int isIndex = 0;
  final checkController = new BehaviorSubject<int>();

  NewsViewModel({@required this.repository});

  init()async{
    checkController.sink.add(0);
  }
    getIndex(int index){
    isIndex = index;
    checkController.sink.add(index);
    notifyListeners();
  }


  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    checkController.close();
  }
}