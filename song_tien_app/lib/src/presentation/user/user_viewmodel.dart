import 'package:flutter/material.dart';
import 'package:song_tien_app/src/presentation/base/base.dart';
import 'package:song_tien_app/src/resource/model/model.dart';
import 'package:song_tien_app/src/resource/repo/other_repository.dart';
import 'package:rxdart/rxdart.dart';
import 'package:dvhcvn/dvhcvn.dart' as dvhcvn;
import 'package:image_picker/image_picker.dart';
import 'package:song_tien_app/src/utils/app_utils.dart';
import '../../resource/resource.dart';
import '../base/base.dart';
import 'package:image/image.dart' as ImD;
import 'package:uuid/uuid.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'dart:ui';
import 'package:path_provider/path_provider.dart';

import '../presentation.dart';

class UserViewModel extends BaseViewModel{

  final OtherRepository repository;


  UserViewModel({@required this.repository});

  final searchProvince = new BehaviorSubject<String>();
  final searchDistrict = new BehaviorSubject<String>();

  final searchController = new BehaviorSubject<List<String>>();

  final TextEditingController textProvince = TextEditingController();
  final TextEditingController textDistrict = TextEditingController();

  TextEditingController _birthcontroller = new TextEditingController();

  String textGender = "";
  String birthDays;

  int mobile = 0 ;
  File _imageFile;
  File get imageFile => _imageFile;

  bool uploading = false;
  String postId = Uuid().v4();
  String imageUrl;

  init()async{
    birthDays = "";
    _birthcontroller.text = birthDays;
  }

  String validateName(String value) {
    if (value.length < 3)
      return 'Name must be more than 2 charater';
    else
      return null;
  }

  String validateMobile(String value) {
    if (value.length != 10)
      return 'Mobile Number must be of 10 digit';
    else
      return null;
  }

  ////Chuyển qua trang tỉnh
  navigateAndDisplayProvince(BuildContext context) async {
    unFocus();
    final result = await Navigator.pushNamed(context, Routers.Province);
    searchProvince.sink.add(result);

  }

  ////Chuyển qua trang HUYỆN
  navigateAndDisplayDistrict(BuildContext context) async {
    unFocus();
    final result = await Navigator.pushNamed(context, Routers.District);
    searchDistrict.sink.add(result);

  }

  //// Camera
  captureImageWithCamera() async {
    Navigator.pop(context);
    File file = await ImagePicker.pickImage(source: ImageSource.camera, maxWidth: 970, maxHeight: 680);
    controlUploadAndSave();
    notifyListeners();
    _imageFile = file;

  }


  /// chọn ảnh từ thư mục
  pickerImage() async {
    Navigator.pop(context);
    File file = await ImagePicker.pickImage(source: ImageSource.gallery);
    controlUploadAndSave();
    notifyListeners();
    _imageFile = file;

  }

  controlUploadAndSave() async {
    notifyListeners();
    uploading = true;


    await compressingPhoto();
    AppUtils.UploadAndSaveImage(imageFile, "profiles")
        .then((downloadUrl) => imageUrl = downloadUrl);
    notifyListeners();
    // imageFile = null;
    uploading = false;
    postId = Uuid().v4();
  }

  compressingPhoto() async {
    final directory = await getTemporaryDirectory();
    final path = directory.path;

    ImD.Image image = ImD.decodeImage(imageFile.readAsBytesSync());
    final compressedImageFile = File('$path/img_$postId.jpg')..writeAsBytesSync(ImD.encodeJpg(image, quality: 60));
    notifyListeners();
    _imageFile = compressedImageFile;

  }



  searchTitle(String text)async{
    List<String> list = [];
    String key = "";
    key= text.toLowerCase();

    for(var i = 0 ; i <dvhcvn.level1s.length;i++){
      String level = dvhcvn.level1s.elementAt(i).name;
      if(level.toLowerCase().contains(key)){
         list.add(level);
      }
    }
    await searchController.sink.add(list);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    searchController.close();
    searchDistrict.close();
    searchProvince.close();
  }

}

class Dog {
  final int id;
  final String name;
  final int age;

  Dog({this.id, this.name, this.age});

  // Convert a Dog into a Map. The keys must correspond to the names of the
  // columns in the database.
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'age': age,
    };
  }
}