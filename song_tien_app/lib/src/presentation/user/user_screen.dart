import 'dart:io';
import 'dart:ui';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:provider/provider.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:song_tien_app/src/configs/configs.dart';
import '../presentation.dart';


class UserScreen extends StatefulWidget {
  @override
  _UserScreenState createState() => _UserScreenState();
}

class _UserScreenState extends State<UserScreen> with ResponsiveWidget {
  DeviceScreenType type;

  final textFieldFocusNode = FocusNode();
  UserViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<UserViewModel>(
        viewModel: UserViewModel(repository: Provider.of(context)),
        onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
        builder: (context, viewModel, child) {
          return Scaffold(
              backgroundColor: Color(0xffbE8E8E8),
              // resizeToAvoidBottomPadding: false,
              // resizeToAvoidBottomInset: false,
              body: SafeArea(child: buildUi(context: context),));
        });
  }

  buildScreen(BuildContext context){
    print(type);
    return Column(
      children: [
        buildAppbar(),
        Expanded(child: _buildBodyMobile(context))
      ],
    );
  }

  //// Trang nhập thông tin BODY
  Widget _buildBodyMobile(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(
            height: 20,
          ),

          ///Anhr đàu tiên
          buildAvatar(context),
          SizedBox(
            height: 15,
          ),
          WidgetTextField(
              hintText: "Tên của bạn",
              radius: 10,
              mglr: 15,
              height: 55,
              contentPd:
                  EdgeInsets.only(left: 15, right: 15, bottom: 4, top: 4)),

          buildBirth(context),

          ///Ô chọn giới tính
          buildGender(context),

          WidgetTextField(
            mglr: 15,
            height: 55,
            contentPd: EdgeInsets.only(left: 15, right: 15, bottom: 4, top: 4),
            hintText: "Email",
            formFieldValidator: (value) => EmailValidator.validate(value)
                ? null
                : "Please enter a valid email",
            radius: 10,
          ),

          WidgetTextField(
            mglr: 15,
            height: 55,
            contentPd: EdgeInsets.only(left: 15, right: 15, bottom: 4, top: 4),
            textInputType: TextInputType.phone,
            formFieldValidator: (value) => _viewModel.validateMobile(value),
            onSaved: (val) {
              _viewModel.mobile = val;
            },
            radius: 10,
            hintText: "Số điện thoại",
          ),

          ///ô chọn tỉnh huyện
          buildAddress(context),

          WidgetTextField(
            mglr: 15,
            height: 55,
            contentPd: EdgeInsets.only(left: 15, right: 15, bottom: 4, top: 4),
            radius: 10,
            hintText: "Địa chỉ",
          ),

          SizedBox(
            height: 80,
          ),

          ////nÚT CẬP NHẬT
          buildButton(context),

          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }

  ///// bảng chọn chụp ảnh và camera
  takeImage(context) {
    return showDialog(
      context: context,
      builder: (context) {
        return SimpleDialog(
          title: Text("Gallery Post",
              style: TextStyle(
                  color: Colors.black87, fontWeight: FontWeight.bold)),
          children: [
            SimpleDialogOption(
              child: Text("Capture Camera",
                  style: TextStyle(color: Colors.black87)),
              onPressed: () => _viewModel.captureImageWithCamera(),
            ),
            SimpleDialogOption(
              child: Text("Capture Gallery",
                  style: TextStyle(color: Colors.black87)),
              onPressed: () => _viewModel.pickerImage(),
            ),
            SimpleDialogOption(
              child: Text("Cancel", style: TextStyle(color: Colors.black87)),
              onPressed: () => Navigator.pop(context),
            ),
          ],
        );
      },
    );
  }

  ///Phần thêm ảnh ở đầu trang
  buildAvatar(BuildContext context) {
    return Stack(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(100),
          child: _viewModel.imageFile == null
              ? Image.asset(
                  AppImages.imgPeople,
                  fit: BoxFit.fill,
                  height: 110,
                  width: 110,
                )
              : Image.file(
                  _viewModel.imageFile,
                  fit: BoxFit.fill,
                  height: 110,
                  width: 110,
                ),
        ),
        Positioned(
            bottom: 2,
            right: 12,
            child: InkWell(
              onTap: () {
                takeImage(context);
              },
              child: InkWell(
                onTap: () {
                  takeImage(context);
                },
                child: CircleAvatar(
                  minRadius: 11,
                  backgroundColor: Colors.red,
                  child: Icon(
                    Icons.camera_alt,
                    color: Colors.white,
                    size: 13,
                  ),
                ),
              ),
            ))
      ],
    );
  }

  buildBirth(BuildContext context) {
    return WidgetTextField(
      mglr: 15,
      height: 55,
      contentPd: EdgeInsets.only(left: 15, right: 15, bottom: 4, top: 4),
      controller: TextEditingController()..text = _viewModel.birthDays,
      textStyle: TextStyle(fontSize: 12),
      suffixIcon: Icon(
        Icons.calendar_today,
        color: Colors.red,
      ),
      onTap: () {
        showDatePicker(
                context: context,
                initialDate: DateTime.now(),
                firstDate: DateTime(2001),
                lastDate: DateTime(2022))
            .then((date) {
          setState(() {
            _viewModel.birthDays = DateFormat("dd/MM/yyyy").format(date);
          });
        });
      },
      formFieldValidator: (birth) {
        if (birth.isEmpty)
          return "ngày sinh không hợp lệ";
        else
          return null;
      },
      readOnly: true,
      hintText: "Sinh nhật",
      radius: 10,
    );
  }

  ///Ô chọn giới tính
  buildGender(BuildContext context) {
    return WidgetTextField(
      controller: TextEditingController()..text = _viewModel.textGender,
      onTap: () async {
        await voidOnButtonModalGender(context);
      },
      readOnly: true,
      hintText: "Nam / nữ",
      contentPd: EdgeInsets.only(left: 15, right: 15, bottom: 4, top: 4),
      hintStyle: TextStyle(
        fontSize: 14,
      ),
      radius: 10,
      mglr: 15,
      height: 40,
      suffixIcon: Icon(
        Icons.arrow_drop_down_sharp,
        color: Colors.red,
        size: 30,
      ),
    );
  }

  //// Ô chọn tỉnh huyện
  buildAddress(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        buildProvince(context),
        buildDistrict(context),
      ],
    );
  }

  ///NÚT cập nhật
  buildButton(BuildContext context) {
    return WidgetButton(
      onPress: () async {
        await showDialogLR(context);
      },
      text: "Cập nhật",
      color: Color(0xffb445E37),
      height: 50,
      width: Get.width,
    );
  }

  ///Ô nhập Tỉnh
  buildProvince(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width * 0.5,
        child: StreamBuilder<String>(
            stream: _viewModel.searchProvince.stream,
            builder: (context, snapshot) {
              return buildTextField(
                  fullName: "Tỉnh / thành",
                  starName: snapshot.data,
                  result: true,
                  context: context,
                  onTap: () => _viewModel.navigateAndDisplayProvince(context));
            }));
  }

  //Ô nhập huyện
  buildDistrict(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width * 0.5,
        child: StreamBuilder<String>(
            stream: _viewModel.searchDistrict.stream,
            builder: (context, snapshot) {
              return buildTextField(
                  fullName: "Quận / huyện",
                  starName: snapshot.data,
                  context: context,
                  check: true,
                  onTap: () => _viewModel.navigateAndDisplayDistrict(context));
            }));
  }

  buildAppbar() {
    return SizedBox(
      height: 80,
        child:  WidgetAppBar(
      keyTitle: "Thông tin cá nhân",
      trans: false,
    ));
  }

////// hiện lên modal nam nữ
  voidOnButtonModalGender(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
            height: 180,
            color: Color(0xffE8E8E8),
            child: Column(
              children: [
                SizedBox(
                  height: 15,
                ),
                _textFiledGender(context, "Nam"),
                _textFiledGender(context, "Nữ"),
              ],
            ),
          );
        });
  }

  Widget buildTextField(
      {String starName,
      String fullName,
      double height = 48,
      bool result = false,
      bool check = false,
      BuildContext context,
      VoidCallback onTap}) {
    return GestureDetector(
      onTap: () async => onTap(),
      child: Container(
        width: double.infinity,
        padding: EdgeInsets.only(left: 10, right: 10),
        margin: EdgeInsets.only(
            top: 5, bottom: 5, left: result ? 15 : 5, right: result ? 5 : 15),
        height: height,
        decoration: buildDecoration(),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              starName == null ? fullName : starName,
              style: AppStyles.TEXT_COLOR_LIGHT.copyWith(
                  color: starName == null ? Colors.black54 : Colors.black),overflow: TextOverflow.visible,maxLines: 1,
            ),
            Spacer(flex: type == DeviceScreenType.mobile?1:7,),
            Expanded(child: Icon(Icons.arrow_drop_down_sharp, color: Colors.red, size: 30)),
          ],
        ),
      ),
    );
  }

  Decoration buildDecoration([Color color = Colors.white]) {
    return BoxDecoration(
        color: color,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.0),
            spreadRadius: 0,
            blurRadius: 5,
            offset: Offset(0, 0), // changes position of shadow
          ),
        ],
        borderRadius: BorderRadius.circular(10));
  }

  _textFiledGender(BuildContext context, String name) {
    return WidgetTextField(
      height: 30,
      hintStyle: TextStyle(
        fontSize: 14,
      ),
      textStyle: TextStyle(
        fontSize: 14,
      ),
      onTap: () {
        setState(() {
          _viewModel.textGender = name;
        });
        Navigator.pop(context);
      },
      mglr: 10,
      readOnly: true,
      hintText: name,
      suffixIcon: Icon(
        Icons.check_circle_outline_sharp,
        color: _viewModel.textGender != name ? Colors.white10 : Colors.green,
      ),
      radius: 5,
    );
  }

  showDialogLR(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) => Dialog(
            elevation: 8,
            insetPadding: const EdgeInsets.only(left: 10, right: 10),
            shape: RoundedRectangleBorder(
              side: BorderSide.none,
              borderRadius: BorderRadius.circular(16),
            ),
            backgroundColor: HexColor.fromHex("#fdfefe"),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                const SizedBox(
                  height: 10,
                ),
                Text(
                  "Bạn có chắc chắn",
                  style: AppStyles.DEFAULT_MEDIUM.copyWith(fontSize: 16),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 4,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: Text(
                    "thay đổi thông tin cá nhân?",
                    style: AppStyles.DEFAULT_MEDIUM,
                    textAlign: TextAlign.center,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                        child: InkWell(
                      borderRadius:
                          BorderRadius.only(bottomLeft: Radius.circular(16)),
                      onTap: () => Navigator.pop(context, false),
                      child: Container(
                        decoration: BoxDecoration(
                          color: Color(0xff445E37),
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(16)),
                        ),
                        height: 45,
                        child: Center(
                          child: Text(
                            "Hủy bỏ",
                            style: AppStyles.DEFAULT_MEDIUM_BOLD
                                .copyWith(color: Colors.white),
                          ),
                        ),
                      ),
                    )),
                    const SizedBox(
                      width: 1,
                    ),
                    Expanded(
                        child: InkWell(
                      borderRadius:
                          BorderRadius.only(bottomLeft: Radius.circular(16)),
                      onTap: () => Navigator.pop(context, true),
                      child: Container(
                        decoration: BoxDecoration(
                            color: Color(0xff445E37),
                            borderRadius: BorderRadius.only(
                                bottomRight: Radius.circular(16))),
                        height: 45,
                        child: Center(
                          child: Text(
                            "OK",
                            style: AppStyles.DEFAULT_MEDIUM_BOLD
                                .copyWith(color: Colors.white),
                          ),
                        ),
                      ),
                    )),
                  ],
                )
              ],
            )));
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    type = DeviceScreenType.desktop;
    return buildScreen(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    type = DeviceScreenType.mobile;
    return buildScreen(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    type = DeviceScreenType.tablet;
    return buildScreen(context);
  }
}
