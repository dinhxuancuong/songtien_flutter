import 'package:flutter/material.dart';
import '../../resource/resource.dart';
import '../base/base.dart';

class PolicyViewModel extends BaseViewModel{

  final OtherRepository repository;

  PolicyViewModel({@required this.repository});

  init()async{
    final NetworkState<OtherApplication> rs = await repository.getMoreApps();
    final otherApp = rs.data;
    print("Other apps: ${otherApp?.toJson() ?? "Nullllll!"}");
  }
}