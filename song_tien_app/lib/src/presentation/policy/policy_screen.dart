import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:song_tien_app/src/presentation/policy/policy.dart';
import '../../configs/configs.dart';
import '../presentation.dart';
import 'package:rxdart/rxdart.dart';
import 'package:dvhcvn/dvhcvn.dart' as dvhcvn;

class PolicyScreen extends StatefulWidget {
  @override
  _PolicyScreenState createState() => _PolicyScreenState();
}

class _PolicyScreenState extends State<PolicyScreen> {
  @override
  Widget build(BuildContext context) {
    return BaseWidget<PolicyViewModel>(
        viewModel: PolicyViewModel(repository: Provider.of(context)),
        onViewModelReady: (viewModel) {
          viewModel.init();
        },
        builder: (context, viewModel, child) {
          return Scaffold(
            body: SafeArea(
              child: Column(
                children: [
                  SizedBox(
                    height:80,
                      child: WidgetAppBar(
                    keyTitle: "Điều Khoản Chính Sách",
                    trans: false,
                  )),
                  Flexible(child: _buildBody(context)),
                ],
              ),
            ),
          );
        });
  }

  Widget _buildBody(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height,
        color: Color(0xffbE8E8E8),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Text(
                    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
                    " software like Aldus PageMaker including versions of Lorem Ipsum."),
              ),
              SizedBox(
                height: 10,
              ),
            ],
          ),
        ));
  }
}
