import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:song_tien_app/src/configs/configs.dart';
import '../../resource/resource.dart';
import '../base/base.dart';
import 'package:rxdart/rxdart.dart';
class PlaceViewModel extends BaseViewModel{

  final OtherRepository repository;
  final ticketController = new BehaviorSubject<Ticket>();
  final checkController = new BehaviorSubject<bool>();
  final placeController = new BehaviorSubject<int>();
  PlaceViewModel({@required this.repository});

  init()async{
    checkController.sink.add(false);
    ticketController.sink.add(null);
    placeController.sink.add(1);
  }


  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    ticketController.close();
    checkController.close();
    placeController.close();
  }

  TextStyle textStyle() {
    return AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(color: Colors.black);
  }

  List<Object> list = [
    "05/02/2021",
    DateFormat("HH:mm").format(DateTime.now()),
    DateFormat("HH:mm").format(DateTime.now()),
    "B001 - Saigon Water taxi",
    "Bến Thủy Bạch Đằng",
    "Nguyễn Thị Huyền",
    true,
    "2.5000.000",
  ];

  List<String> listString = [
    "Ngày khởi hành :",
    "Giờ khởi hành:",
    "Giờ kết thúc:",
    "Mã số tàu (Tên):",
    "Địa điểm xuất bến:",
    "Người hướng dẫn:",
    "Trạng thái:",
    "Phí (giá vé):"
  ];


  List<String> tabs = [
    "Thông tin chuyến",
    "Thông tin đặt chỗ",
    "Danh sách khách"
  ];

  check(int id,) async {
    if (id !=1) {
      checkController.sink.add(false);
    } else {
      checkController.sink.add(true);
    }
  }

}