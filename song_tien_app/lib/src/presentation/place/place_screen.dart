import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:song_tien_app/src/presentation/place/place.dart';
import 'package:song_tien_app/src/presentation/widgets/widget_button_gradientAni.dart';
import 'package:song_tien_app/src/presentation/widgets/widget_dotborder.dart';
import 'package:song_tien_app/src/resource/model/model.dart';
import '../../configs/configs.dart';
import '../presentation.dart';
import 'package:rxdart/rxdart.dart';

class PlaceScreen extends StatefulWidget {
  //  // Ticket ticket;
  final Address address;
  final  Ticket ticket;

  const PlaceScreen({Key key, this.address, this.ticket}) : super(key: key);


  @override
  _PlaceScreenState createState() => _PlaceScreenState();
}

class _PlaceScreenState extends State<PlaceScreen>
    with SingleTickerProviderStateMixin,ResponsiveWidget {
  DeviceScreenType type;

  PlaceViewModel _viewModel;
 int _activeTabIndex;
  TabController _tabController;

 @override
  void initState() {
   // TODO: implement initState
   super.initState();
   // within your initState() method
   // _tabController.addListener(_setActiveTabIndex);
 }

  void _setActiveTabIndex() {
    _activeTabIndex = _tabController.index;
    print(_activeTabIndex);
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<PlaceViewModel>(
        viewModel: PlaceViewModel(repository: Provider.of(context)),
        onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
        // child: _buildBody(context),
        // childMobile: _buildBodyMobile(context),
        builder: (context, viewModel, child) {

          return SafeArea(
            child: Scaffold(
                backgroundColor: Color(0xffE8E8E8),
                body:buildUi(context: context)

          ));
        });
  }

  buildNestedScroll(BuildContext context){
    return  DefaultTabController(
        length: 3,
        child: NestedScrollView(
          // physics: NeverScrollableScrollPhysics(),
            headerSliverBuilder: (context, isSc) {
              final index = DefaultTabController.of(context).index??0;
              print(index);
              return [
                SliverList(
                  delegate: new SliverChildBuilderDelegate(
                        (context, innerBoxIsScrolled) {
                      return _buildBody(context);
                    },
                    childCount: 1,
                  ),
                ),
              ];
            },
            body: buildTabBar()
        )
    );
  }

  Widget _buildBody(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      decoration: BoxDecoration(
        color: Color(0xffE8E8E8),
      ),
      child: Stack(
        children: [
          buildHeader(),
          Padding(
            padding: const EdgeInsets.only(top: 160),
            child: Column(
              children: [
                buildTitle(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  buildHeader() {
    return Container(
      height:type == DeviceScreenType.mobile?210:255,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: ExactAssetImage(
                AppImages.imgSongTien2,
              ),
              fit: BoxFit.fill)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          IconButton(
              icon: Icon(
                Icons.arrow_back_ios_outlined,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.pop(context);
              }),
        ],
      ),
    );
  }

  /// Tiều đề  Tham quan
  buildTitle() {
    var size = MediaQuery.of(context).size;
    return Container(
      height: AppSizeConfig.screenHeight * 0.2,
      width: double.infinity,
      margin: EdgeInsets.only(left: 10, right: 10, bottom: 8),
      decoration: buildDecoration(),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
              width: AppSizeConfig.screenWidth * 0.9,
              child: Text(
                widget.address.title,
                style: AppStyles.DEFAULT_LARGE_BOLD
                    .copyWith(color: Color(0xff5A7D48), fontSize: 17,fontFamily: "Xanh_Mono"),
                textAlign: TextAlign.center,
              )),
          SizedBox(
            height: 5,
          ),
          WidgetStart(
            rating: 5,
          )
        ],
      ),
    );
  }

  buildTabBar() {
    return Column(
      children: [
        Expanded(
          child: Container(
            // color: Colors.red,
              margin: EdgeInsets.only(left: 10, right: 10,bottom: 5),
              width: double.infinity,
              decoration: buildDecoration(),
              child: Column(
                children: [
                  buildTabs(),
                  SizedBox(height: 10,),
                  buildTabsView(),
                ],
                // children: [buildTabs(), buildTabsView()],
              )),
        ),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: StreamBuilder(
          stream: _viewModel.checkController,
          builder: (context,snapshot){
            return snapshot.data?buildButton():Container(height: 0,);
          },
        ),
      )
      ],
    );
  }


  buildTabsView() {
    return Flexible(
        child: TabBarView(
          physics: NeverScrollableScrollPhysics(),
          controller: _tabController,
          children: [buildProcess(),buildInfo(),
            Container(
            height: Get.height,
            child: ListView(
              padding: EdgeInsets.only(top: 5),
              shrinkWrap: true,
              children: _listClient(listTicket),
            ),
          )
          ],
        ));
  }

  buildTabs() {
    return Container(
      padding: EdgeInsets.only(left: 10,right: 10),
      decoration: BoxDecoration(
          color: Colors.white.withOpacity(0.0),
          border: Border(bottom: BorderSide(color: Colors.grey,))),
      child: TabBar(
        onTap: (index) => _viewModel.check(index),
        controller: _tabController,
          labelStyle: AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(
              fontSize: 50, fontWeight: FontWeight.bold, color: Colors.black),
          unselectedLabelColor: Colors.red,
          unselectedLabelStyle: AppStyles.DEFAULT_MEDIUM,
          isScrollable: true,
          indicatorPadding:EdgeInsets.symmetric(vertical: 10),
          labelColor: Colors.black,
          indicatorSize: TabBarIndicatorSize.label,
          indicator: UnderlineTabIndicator(
            borderSide: BorderSide(
              width: 1.5,
              color: AppColors.red,
            ),
            insets: EdgeInsets.symmetric(horizontal: 32,vertical: 0),
          ),
          physics: NeverScrollableScrollPhysics(),
          labelPadding: EdgeInsets.only(top: 5, right: 5,),
          tabs: List.generate(
            _viewModel.tabs.length,
                (index) => Tab(
              icon: SizedBox(
                width: Get.width*0.3-10,
                child: Text(
                  _viewModel.tabs[index],
                  textAlign: TextAlign.center,
                  style:  AppStyles.DEFAULT_SMALL_HIDE.copyWith(color: Colors.black38),
                ),
              ),
            ),
          )),
    );
  }

  ///Danh sách khách
  _listClient(List<Ticket> listTick) {
    List<Widget> list = [];
    for (var i = 0; i < listTick.length; i++) {
      list.add(Stack(
        alignment: Alignment(0, 0),
        children: [
          GestureDetector(
            onTap: () => Navigator.pushNamed(context, Routers.Client,arguments: widget.ticket),
            child: Container(
              // padding: EdgeInsets.only(left: 5,right: 5,top: 10,bottom: 5),
              // color: Colors.white,
              margin:EdgeInsets.only(left: 20,right: 20,top: 10,bottom: 5) ,
              height: 70,
              width: double.infinity,
              color: i%2==0?Color(0xffF4F9F2):Color(0xffE8E8E8),
              child: WidgetDotBorder(
                gap: 4,
                drawBottomBorder: true,
                drawLeftBorder: true,
                drawRightBorder: true,
                drawTopBorder: true,
                color: Color(0xff5A7D48),
                child: Padding(
                  padding: EdgeInsets.only(left: 20,right: 20,top: 10,bottom: 5) ,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        listTick[i].codeName,
                        style: AppStyles.DEFAULT_MEDIUM.copyWith(color: Colors.black26),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          listTick[i].check
                              ? Text("Đã xác nhận",
                                  style: AppStyles.DEFAULT_SMALL
                                      .copyWith(color: Color(0xff445E37)))
                              : Text("Chờ xác nhận",
                                  style: AppStyles.DEFAULT_SMALL
                                      .copyWith(color: Colors.red)),
                          Text(
                            listTick[i].code,
                            style: AppStyles.DEFAULT_MEDIUM
                                .copyWith(color: Colors.black26),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            left: 10,
            child: CircleAvatar(
              backgroundColor: Color(0xff5A7D48),
              child: Text("${i+1}"),
              maxRadius: 9,
            ),
          )
        ],
      ));
    }
    return list;
  }

  Decoration buildDecoration([Color color = Colors.white]) {
    return BoxDecoration(
        color: color,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
        borderRadius: BorderRadius.circular(5));
  }

  ////Thông tin chuyến
  Widget buildProcess() {
    var size = MediaQuery.of(context).size;
    return StreamBuilder<bool>(
        stream: _viewModel.checkController.stream,
        builder: (context, snapshot) {
          if (snapshot.data == null) {
            return Container();
          }
          return Column(
            children: [
              Expanded(
                child: ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: listTicket.length,
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    print(listTicket[0].departureDay);
                    return WidgetDeclaration(
                      list: _viewModel.listString,
                      select: snapshot.data,
                      ticket: widget.ticket,
                      onTap: (){},
                    );
                  },
                ),
              )
            ],
          );
        });
  }

  ///Thông tin đặt chỗ
  buildInfo() {
    return Container(
        margin: EdgeInsets.only(bottom: 10,top: 5),
        padding: EdgeInsets.only(left: 15, right: 15),

        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _rowPlace("Tổng số chỗ", 16, true),
            _rowPlace("Số chỗ đã xác nhận", 8),
            _rowPlace("Số chỗ đang chờ xác nhận", 3),
            _rowPlace("Số chỗ còn trống", 5),
            SizedBox(height: 10,),
            Expanded(child: _listPlace()),
          ],
        ));
  }

  buildButton() {
    return StreamBuilder<int>(
        stream: _viewModel.placeController.stream,
        builder: (context, snapshot) {
          return snapshot.data == 1
              ? WidgetButtonGradientAni(
                  title: "place",
                  colorStart: AppColors.primary,
                  colorEnd: AppColors.primary,
                  width: Get.width,
                  loading: false,
                  action: () {
                    // _viewModel.unFocus();
                    Navigator.pushNamed(context, Routers.PlaceIfomation,arguments: widget.ticket);
                  },
                  textStyle:
                      AppStyles.DEFAULT_MEDIUM.copyWith(color: AppColors.white))
              : Container();
        });
  }

  ListView _listPlace() {
    return ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: 8,
      itemBuilder: (context, index) {
        return Padding(
          padding: EdgeInsets.only(left: 10, right: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                children: [
                  Flex(
                    direction: Axis.horizontal,
                    children: [
                      Image.asset(
                        AppImages.imgChair,
                        fit: BoxFit.fill,
                        height: 30,
                      ),
                      Image.asset(
                        AppImages.imgChair,
                        fit: BoxFit.fill,
                        height: 30,
                      )
                    ],
                  ),
                ],
              ),
              Spacer(),
              Column(
                children: [
                  Flex(
                    direction: Axis.horizontal,
                    children: [
                      Image.asset(
                        AppImages.imgChair,
                        fit: BoxFit.fill,
                        height: 30,
                      ),
                      Image.asset(
                        AppImages.imgChair,
                        fit: BoxFit.fill,
                        height: 30,
                      )
                    ],
                  ),
                ],
              )
            ],
          ),
        );
      },
    );
  }

  _rowPlace(starName, endName, [bool check = false]) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(starName,
            style: check
                ? _viewModel.textStyle()
                : AppStyles.DEFAULT_SMALL.copyWith(fontSize: 14)),
        Text(
          "${endName}",
          style: check
              ? _viewModel.textStyle()
              : AppStyles.DEFAULT_SMALL.copyWith(fontSize: 14),
        )
      ],
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    type = DeviceScreenType.desktop;
    // TODO: implement buildDesktop
    return buildNestedScroll(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    type = DeviceScreenType.mobile;
    // TODO: implement buildDesktop
    return buildNestedScroll(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    type = DeviceScreenType.tablet;
    // TODO: implement buildDesktop
    return buildNestedScroll(context);
  }
}




