import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:song_tien_app/src/presentation/place_infomation/place_infomation.dart';
import 'package:song_tien_app/src/resource/model/future.dart';
import 'package:song_tien_app/src/resource/model/model.dart';
import '../../configs/configs.dart';
import '../presentation.dart';
import 'package:intl/intl.dart';

class PlaceInformationScreen extends StatefulWidget {
  final Ticket ticket;

  const PlaceInformationScreen({Key key, this.ticket}) : super(key: key);

  @override
  _PlaceInformationScreenState createState() => _PlaceInformationScreenState();
}

class _PlaceInformationScreenState extends State<PlaceInformationScreen> {
  int _phone;

  PlaceInformationViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    AppSizeConfig().init(context);
    return BaseWidget<PlaceInformationViewModel>(
        viewModel: PlaceInformationViewModel(repository: Provider.of(context)),
        onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
        builder: (context, viewModel, child) {
          return WillPopScope(
            onWillPop: () async {
              _viewModel.onScope();
              return Future.value(false);
            },
            child: Scaffold(
              backgroundColor: Color(0xffDEDEDE),
              body: SafeArea(
                child: Column(
                  children: [
                    SizedBox(
                      height:80,
                      child: WidgetAppBar(
                        actionBack: () {
                          _viewModel.onScope();
                        },
                        keyTitle: _viewModel.isCheck
                            ? "Đặt chỗ"
                            : "Thông tin đặt chỗ",
                        trans: false,
                      ),
                    ),
                    _viewModel.isCheck
                        ? Expanded(child: _buildSeat(context))
                        : Expanded(
                            child: _buildInfoPlace(context),
                          )
                  ],
                ),
              ),
            ),
          );
        });
  }

  _buildSeat(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            height: 20,
          ),
          ////Bảng thông tin đặt chỗ
          buildInfoRV(),

          //Bảng thông tin thanh toán
          buildInfoPay(),

          ///Dòng lưu ý
          buildNote(),
          SizedBox(
            height: 10,
          ),
          WidgetButton(
            horizontal: 30,
            text: "Hoàn tất",
            color: Color(0xff5A7D48),
            colorSide: Color(0xff5A7D48),
            width: double.infinity,
            onPress: () {},
            height: 45,
          ),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }

  Widget _buildInfoPlace(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.only(left: 15, right: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              height: 20,
            ),
            Text(
              "Thông tin đặt chỗ",
              style: AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(fontSize: 14),
            ),
            _textField(
                name: "Mã chuyến",
                readOnly: true,
                controller: widget.ticket.code),
            _textField(
                name: "Người liên hệ",
                readOnly: true,
                controller: widget.ticket.instructorName),
            _textField(
                // controller: widget.ticket.code,
                maxLines: 6,
                suffixText: "Ghi chú",
                textAlign: TextAlign.start,
                name: "",
                textInputType: TextInputType.text),
            SizedBox(
              height: 15,
            ),
            Text(
              "Thông tin hành khách",
              style: AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(fontSize: 14),
            ),
            _rowTextField("Mã chỗ ngồi", widget.ticket.code,
                onTap: () => modalReservations(context)),
            _textField(
              name: "Tên khách hàng",
              // readOnly: true,
            ),
            _textField(
                name: "Số điện thoại",
                // readOnly: true,
                textInputType: TextInputType.number),
            SizedBox(
              height: 45,
            ),
            buildButton(),
            SizedBox(
              height: 20,
            )
          ],
        ),
      ),
    );
  }

  buildNote() {
    return Padding(
      padding: EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Icon(
            Icons.star,
            size: 5,
            color: Colors.red,
          ),
          SizedBox(
            width: 3,
          ),
          Text("Khách hàng thanh toán sau tại quầy"),
        ],
      ),
    );
  }

////Bảng Thông tin thanh toán
  buildInfoPay() {
    return Padding(
      padding: const EdgeInsets.only(left: 15, right: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(
            "Thông tin thanh toán",
            style: AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(fontSize: 14),
          ),
          SizedBox(
            height: 5,
          ),
          _textField(
              name: "Giá vé", controller: widget.ticket.pay, readOnly: true),
          _textField(name: "Số Lượng", controller: "2", readOnly: true),
          _textField(
              name: "Thành tiền",
              controller: widget.ticket.pay,
              readOnly: true),
          _textField(
              name: "VAT (10%)", controller: widget.ticket.pay, readOnly: true),
          _textField(
              name: "Tổng", controller: widget.ticket.pay, readOnly: true,color: Colors.green)
        ],
      ),
    );
  }

  ///Bảng thông tin đặt chỗ
  buildInfoRV() {
    return Padding(
      padding: const EdgeInsets.only(left: 15, right: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(
            "Thông tin đặt chỗ",
            style: AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(fontSize: 14),
          ),
          SizedBox(
            height: 5,
          ),
          _textField(
              name: "Mã chuyến",
              readOnly: true,
              controller: widget.ticket.code),
          _textField(
              name: "Người liên hệ",
              readOnly: true,
              controller: widget.ticket.instructorName),
          _textField(
              name: "Thời gian đặt",
              readOnly: true,
              controller:
                  "${DateFormat("dd/MM/yyyy - hh:mm").format(DateTime.now())}"),
          _textField(name: "Số chỗ", readOnly: true, controller: "2"),
          _textField(
              name: "",
              readOnly: true,
              textAlign: TextAlign.start,
              maxLines: 4,
              controller: "Khách hàng không đem theo nhiều hành lý"),
          SizedBox(
            height: 10,
          ),
          Text(
            "Thông tin hành khách",
            style: AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(fontSize: 14),
          ),
          SizedBox(
            height: 5,
          ),
          buildPassenger(),
          SizedBox(
            height: 15,
          ),
        ],
      ),
    );
  }

  ////Bảng thông tin khách hàng
  buildPassenger() {
    return Container(
      padding: EdgeInsets.only(left: 22, right: 22),
      margin: EdgeInsets.only(
        top: 5,
      ),
      height: 50,
      decoration: buildDecoration(),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "A1",
            style: AppStyles.TEXT_COLOR_LIGHT.copyWith(
              color: Color(0xff5A7D48),
            ),
          ),
          Text(
            "Huyền Trương",
            textAlign: TextAlign.left,
          ),
          SizedBox(
            width: 20,
          ),
          Text("0364111999")
        ],
      ),
    );
  }

  _textField(
      {String name,
      TextInputType textInputType,
      String suffixText,
      List<TextInputFormatter> input,
      String controller,
      bool readOnly = false,
      int maxLines = null,
      Color color = Colors.grey,
      TextAlign textAlign = TextAlign.end}) {
    return Stack(
      alignment: Alignment(0, 0),
      children: [
        WidgetTextField(
          readOnly: readOnly,
          maxLines: maxLines,
          controller: TextEditingController()..text = controller,
          listTextInputFormatter: [],
          textAlign: textAlign,
          textInputType: textInputType,
          mglr: 0,
          top: 6,
          hintText: suffixText,
          hintStyle: AppStyles.TEXT_COLOR_LIGHT.copyWith(
            color: color,
          ),
          height: 55,
          contentPd: EdgeInsets.only(left: 15, right: 15, bottom: 4, top: 4),
          radius: 5,
        ),
        Positioned(left: 17, child: Text(name, style: _viewModel.textStyle))
      ],
    );
  }

  Widget _rowTextField(String starName, String endName,
      {double height = 45,
      bool result = false,
      bool checkColor = false,
      Function onTap}) {
    return InkWell(
      onTap: () => onTap() ?? {},
      child: Container(
        width: double.infinity,
        padding: EdgeInsets.only(left: 15, right: 15),
        margin: EdgeInsets.only(
          top: 4,
          bottom: 4,
        ),
        height: height,
        decoration: buildDecoration(),
        child: result
            ? Text(starName)
            : Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    starName,
                    style: AppStyles.TEXT_COLOR_LIGHT.copyWith(
                      color: checkColor ? Color(0xff5A7D48) : Color(0xffDEDEDE),
                    ),
                  ),
                  Text(endName)
                ],
              ),
      ),
    );
  }

  Decoration buildDecoration([Color color = Colors.white]) {
    return BoxDecoration(
        color: color,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.0),
            spreadRadius: 2,
            blurRadius: 0,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
        borderRadius: BorderRadius.circular(5));
  }

  buildButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: WidgetButton(
              height: 50,
              horizontal: 5,
              text: "Thêm khách hàng",
              fontSize: 12,
              color: Colors.white,
              onPress: () {
                modalReservations(context);
              },
              colorSide: Color(0xff0FF2A8068),
              colorsText: Color(0xff0FF2A8068)),
        ),
        Expanded(
          child: WidgetButton(
              onPress: () {
                _viewModel.checkPage();
              },
              horizontal: 5,
              height: 50,
              text: "Đặt chỗ",
              fontSize: 12,
              color: Color(0xff445E37),
              colorSide: Color(0xff0FF2A8068)),
        )
      ],
    );
  }

  ////modal bảng đặt chỗ
  modalReservations(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
            height: Get.height * 0.4,
            color: Color(0xffbfbfbf),
            child: Column(
              children: [
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Thêm chỗ ngồi",
                  style: AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(fontSize: 15),
                  textAlign: TextAlign.center,
                ),
                Divider(
                  color: Colors.white,
                  thickness: 0,
                  indent: 13,
                  endIndent: 13,
                ),
                Expanded(
                  child: Container(
                    height: 200,
                    width: double.infinity,
                    // padding: EdgeInsets.only(left: 50,right: 50),
                    margin: EdgeInsets.only(left: 10, right: 10, bottom: 10),
                    decoration: buildDecoration(Color(0xffEFEFEF)),
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: FAKE_FURNITUREDATATWO.length,
                      itemBuilder: (context, index) {
                        return Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                WidgetFurnitureItem(
                                  furniture: FAKE_FURNITUREDATATWO[index],
                                ),
                              ],
                            ),
                            Expanded(
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  WidgetFurnitureItem(
                                    furniture: FAKE_FURNITUREDATATWO[index],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        );
                      },
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: WidgetButton(
                    text: "Thêm chỗ ngồi",
                    circular: 20,
                    color: Color(0xff445E37),
                    width: double.infinity,
                    height: 45,
                    colorSide: Color(0xff445E37),
                    onPress: () {
                      Navigator.pop(context);
                    },
                  ),
                ),
                SizedBox(
                  height: 10,
                )
              ],
            ),
          );
        });
  }
}
