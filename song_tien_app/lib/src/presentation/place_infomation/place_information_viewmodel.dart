import 'package:flutter/material.dart';
import '../../resource/resource.dart';
import '../base/base.dart';
import 'package:rxdart/rxdart.dart';
import '../../configs/configs.dart';
class PlaceInformationViewModel extends BaseViewModel {

  final OtherRepository repository;
  final pageController = new BehaviorSubject<bool>();
  TextEditingController textEditingController;
  bool isCheck = false;
  // bool get isCheck => _isCheck;
  PlaceInformationViewModel({@required this.repository});

  init() async {
  }

  TextStyle textStyle =
    AppStyles.TEXT_COLOR_LIGHT.copyWith(
      color: Colors.grey.withOpacity(0.5),
    );

  checkPage(){
    isCheck = !isCheck;
  notifyListeners();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    pageController.close();
  }

  /// nút xử lí back button
  onScope() {
    if (isCheck) {
      print("true");
      isCheck = !isCheck;
      notifyListeners();
    } else {
      Navigator.pop(context);
    }
  }


  String validateMobile(String value) {
    if (value.length != 10)
      return 'Mobile Number must be of 10 digit';
    else
      return null;
  }
}