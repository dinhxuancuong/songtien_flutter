import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:song_tien_app/src/presentation/notification/notification.dart';
import 'package:song_tien_app/src/presentation/policy/policy.dart';
import 'package:song_tien_app/src/resource/model/model.dart';
import 'package:song_tien_app/src/resource/model/person.dart';
import 'package:song_tien_app/src/resource/model/user.dart';
import '../../configs/configs.dart';
import '../presentation.dart';
import 'package:rxdart/rxdart.dart';
import 'package:dvhcvn/dvhcvn.dart' as dvhcvn;

class NotificationScreen extends StatefulWidget {


  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}
NotificationViewModel _viewModel;
class _NotificationScreenState extends State<NotificationScreen> {
  @override
  Widget build(BuildContext context) {
    return BaseWidget<NotificationViewModel>(
        viewModel: NotificationViewModel(repository: Provider.of(context)),
        onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: SafeArea(
              child: Column(
                children: [
                  SizedBox(
                    height:80,
                      child: WidgetAppBar(keyTitle:  "Thông Báo",trans: false)),
                  Expanded(child: _buildBody(context))
                ],
              ),
            ),
          );
        });
  }



  Widget _buildBody(BuildContext context) {
    return SizedBox.expand(
      child: ListView.builder(
        padding: EdgeInsets.only(top: 5),
        itemCount: listData.length,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          Person person = listData[index];
          return Padding(
            padding: const EdgeInsets.only(
              top: 4,
            ),
            child: GestureDetector(
              onTap: (){
               _viewModel.listColor.add(index);
                Navigator.pushNamed(context, Routers.NotificationDetails,arguments:person );
                setState(() {

                });
              },
              child: Column(
                children: [
                  WidgetCustomListItem(
                    thumbnail: CircleAvatar(
                        maxRadius: 30,
                        backgroundColor: Colors.white,
                        child: Padding(
                          padding: const EdgeInsets.all(3.0),
                          child: Image.asset(
                            AppImages.imgSongTien,
                            fit: BoxFit.fill,
                          ),
                        )),
                    child: Description(
                      title: person.title,
                      subTiTle: person.content,
                      dateTime: person.dateTime,
                    ),
                    color: _viewModel.checkColor(index)
                  ),
                  _viewModel.checkDash(index)
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
