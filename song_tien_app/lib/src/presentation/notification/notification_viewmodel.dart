import 'package:flutter/material.dart';
import '../../resource/resource.dart';
import '../base/base.dart';

class NotificationViewModel extends BaseViewModel{

  final OtherRepository repository;
   int index;
  NotificationViewModel({@required this.repository});
  List<int> listColor = [];
  init()async{
  }

  Color checkColor(int index){
   if(listColor.contains(index)){
     return Color(0xffE8E8E8);
   }else{
     return Colors.white;
   }
  }

  Widget checkDash(int index){
    if(listColor.contains(index)){
      return Container();
    }else{
      return Divider(
        height: 13,
        color: Colors.grey[400],
        thickness: 1,
      );
    }
  }
}