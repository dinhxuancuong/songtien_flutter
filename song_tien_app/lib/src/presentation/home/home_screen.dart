import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:song_tien_app/src/configs/configs.dart';
import 'package:song_tien_app/src/presentation/home/home.dart';
import 'package:song_tien_app/src/presentation/widgets/widget_carousel.dart';
import 'package:song_tien_app/src/resource/model/app_valid.dart';
import 'package:song_tien_app/src/resource/model/model.dart';
import 'package:responsive_builder/responsive_builder.dart';
import '../presentation.dart';


class HomeScreen extends StatefulWidget {
  @override
  _HomeScreentate createState() => _HomeScreentate();
}

class _HomeScreentate extends State<HomeScreen> with ResponsiveWidget {
  HomeViewModel _viewModel;
  DeviceScreenType type;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<HomeViewModel>(
      viewModel: HomeViewModel(repository: Provider.of(context)),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
            backgroundColor: Color(0xffEFEFEF),
            body: buildUi(context: context));
      },
    );
  }

  Widget _buildHeader() {
    return Stack(
      overflow: Overflow.visible,
      alignment: Alignment.topCenter,
      children: [
        DottedBorder(
          borderType: BorderType.RRect,
          radiusBottomLeft: Radius.circular(100),
          radiusBottomRight: Radius.circular(100),
          dashPattern: [8, 5],
          strokeWidth: 1.5,
          color: Colors.green[800],
          child: Container(
            width: Get.width,
            height: type == DeviceScreenType.mobile?Get.height*0.4:Get.height/1/3-20,
            decoration: BoxDecoration(
              color: Color(0xffADBFA3),
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(100),
                bottomRight: Radius.circular(100),
              ),
            ),
          ),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(height: 10,),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(child: WidgetLogo()),
                GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(context, Routers.Notification);
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(right: 15),
                    child: CircleAvatar(
                        radius: 20,
                        backgroundColor: Colors.white,
                        child: Icon(
                          Icons.notifications_none_sharp,
                          size: 30,
                        )),
                  ),
                ),
                // SizedBox(width: 13),
              ],
            ),
            Padding(
                padding: EdgeInsets.only(left: 15, right: 15),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: WidgetCarousel(
                    listImG: listSTR,
                  ),
                )),
          ],
        ),
      ],
    );
  }

  List<String> listSTR = [
    AppImages.imgLand1,
    AppImages.imgLand2,
    AppImages.imgLand3,
    AppImages.imgIsLand3,
    AppImages.imgIsland1,
    AppImages.imgIsLand2
  ];

  Widget _buildBody() {
    return AnimatedSwitcher(
      transitionBuilder: (child, animation) =>
          RotationTransition(turns: animation, child: child),
      layoutBuilder: (widget, list) => Stack(children: [widget, ...list]),
      switchInCurve: Curves.easeInBack,
      switchOutCurve: Curves.easeInBack.flipped,
      duration: Duration(seconds: 1),
      child: GestureDetector(child: _buildColumn()),
    );
  }

  _buildColumn() {
    return Padding(
      padding: const EdgeInsets.only(left: 15, right: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ///kHOẢNG CÁCH
          _rangeHeight(height: 15),

          _rowText(
            name: AppLocalizations.of(context).translate("sightseeing_program"),
            onTap: () =>Navigator.pushNamed(context, Routers.Tour)),

          _rangeHeight(),

          ///Chương trình tham quan
          _buildProgram(),

          buildImages(),

          _rangeHeight(),

          _rowText(
            name: AppLocalizations.of(context).translate("news"),
            onTap: ()=> Navigator.pushNamed(context, Routers.News)
          ),

          _rangeHeight(),

          ///Tin tức
          _buildNews(),

          _rangeHeight(height: 20),
        ],
      ),
    );
  }

  buildImages(){
    return Image.asset(
     AppImages.imgLand1,
      fit: BoxFit.fill,
        height: Get.height/1/4,
        width: double.infinity,
    );
  }

  ////
  ///khoảng cách
  _rangeHeight({double height = 10}) => SizedBox(
        height: height,
      );

  _buildImage() {
    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: Image.asset(
        AppImages.imgLand1,
        fit: BoxFit.fill,
        width: Get.width,
        height: 250,
      ),
    );
  }

  Future<Null> refereshList() async {
    Future.delayed(Duration(seconds: 2));
  }

  _buildProgram() {
    return Container(
      height: type == DeviceScreenType.mobile?Get.height*0.4:Get.height*0.4-40,
      width: double.infinity,
      child: RefreshIndicator(
        onRefresh: refereshList,
        child: ListView.separated(
          scrollDirection: Axis.horizontal,
          shrinkWrap: false,
          itemCount: listAdress.length,
          separatorBuilder: (context, index) => SizedBox(
            width: 12,
          ),
          itemBuilder: (context, index) => GestureDetector(
            onTap: ()=> Navigator.pushNamed(context, Routers.DetailsTour,arguments: listAdress[index]),
            child: WidgetStackVisible(
              title: listAdress[index].title,
              images: listAdress[index].images,
              type: type,
              // ),
            ),
          ),
        ),
      ),
    );
  }

  _buildNews() {
    return Container(
      height: DeviceScreenType.mobile == type ?Get.height*0.4:Get.height*0.3,
      width: double.infinity,
      child: ListView.separated(
        scrollDirection: Axis.horizontal,
        shrinkWrap: false,
        itemCount: 3,
        separatorBuilder: (context, index) => SizedBox(
          width: 10,
        ),
        itemBuilder: (context, index) => _displayNews(),
      ),
    );
  }

  Widget _displayNews() {
    return InkWell(
      onTap: () => Navigator.pushNamed(context, Routers.DetailsNews),
      child: Container(
        decoration: buildDecoration(radius: 10),
        width: Get.width-75,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.only(topLeft: Radius.circular(10),topRight: Radius.circular(10)),
              child: Image.asset(
                AppImages.imgLand2,
                width: Get.width,
                height: Get.height*0.2+20,
                fit: BoxFit.fill,
              ),
            ),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Biệt thự đảo xu hướng mới trên thị trường bất động sản",
                  textAlign: TextAlign.start,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                "${DateFormat("dd/MM/yyyy").format(DateTime.now())}",
                style: TextStyle(color: Colors.black38),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Row _rowText({String name,Function onTap}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          name,
          style:
              AppStyles.DEFAULT_LARGE.copyWith(color: Colors.red, fontSize: 15),
        ),
        GestureDetector(
          onTap: () =>onTap()??null,
          child: Text(
            "Xem tất cả",
            style: AppStyles.DEFAULT_SMALL.copyWith(color: AppColors.primary),
          ),
        )
      ],
    );
  }

  Decoration buildDecoration({Color color = Colors.white,double radius = 10}) {
    return BoxDecoration(
        color: color,
        boxShadow: [
          BoxShadow(
            color: Colors.white.withOpacity(0.1),
            spreadRadius: 2,
            blurRadius: 3,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
        borderRadius: BorderRadius.circular(radius));
  }
  // buildViews(bool loading){
  //   return
  // }

  Widget _buildIconLogin(String imageUrl, Function onTap, bool loading) {
    return InkWell(
      onTap: loading ? null : onTap,
      child: Container(
        width: 48,
        height: 48,
        child: Image.asset(
          imageUrl,
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  OutlineInputBorder _buildBorderTextField(Color color) {
    return OutlineInputBorder(
      borderRadius: BorderRadius.circular(150),
      borderSide: BorderSide(
        color: color,
        width: 1.2,
      ),
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    type = DeviceScreenType.desktop;
    // TODO: implement buildDesktop
    return  SingleChildScrollView(
      child: Column(
        children: [
          _buildHeader(),
          _buildBody(),
        ],
      ),
    );
  }

  @override
  Widget buildMobile(BuildContext context) {
    type = DeviceScreenType.mobile;
    // TODO: implement buildMobile
    return  SingleChildScrollView(
      child: Column(
        children: [
          _buildHeader(),
          _buildBody(),
        ],
      ),
    );
  }

  @override
  Widget buildTablet(BuildContext context) {
    type = DeviceScreenType.tablet;
    // TODO: implement buildMobile
    return  SingleChildScrollView(
      child: Column(
        children: [
          _buildHeader(),
          _buildBody(),
        ],
      ),
    );
  }
}
