import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:song_tien_app/src/utils/app_utils.dart';
import '../../resource/resource.dart';
import '../base/base.dart';
import 'package:image/image.dart' as ImD;
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'dart:ui';
import 'package:path_provider/path_provider.dart';

import '../presentation.dart';


class AccountViewModel extends BaseViewModel{

  final OtherRepository repository;

  AccountViewModel({@required this.repository});

  File _imageFile;
  File get imageFile => _imageFile;

  bool uploading = false;
  String postId = Uuid().v4();
  String imageUrl;

  init()async{

  }
  //// Camera
  captureImageWithCamera() async {
    Navigator.pop(context);
    File file = await ImagePicker.pickImage(source: ImageSource.camera, maxWidth: 970, maxHeight: 680);
    controlUploadAndSave();
    notifyListeners();
      _imageFile = file;

  }


  /// chọn ảnh từ thư mục
  pickerImage() async {
    Navigator.pop(context);
    File file = await ImagePicker.pickImage(source: ImageSource.gallery);
    controlUploadAndSave();
    notifyListeners();
    _imageFile = file;

  }

  controlUploadAndSave() async {
    notifyListeners();
      uploading = true;


    await compressingPhoto();
    AppUtils.UploadAndSaveImage(imageFile, "profiles")
        .then((downloadUrl) => imageUrl = downloadUrl);
    notifyListeners();
      // imageFile = null;
      uploading = false;
      postId = Uuid().v4();
  }

  compressingPhoto() async {
    final directory = await getTemporaryDirectory();
    final path = directory.path;

    ImD.Image image = ImD.decodeImage(imageFile.readAsBytesSync());
    final compressedImageFile = File('$path/img_$postId.jpg')..writeAsBytesSync(ImD.encodeJpg(image, quality: 60));
    notifyListeners();
    _imageFile = compressedImageFile;

  }
  Future<void> signOut() async {
    await FirebaseAuth.instance.signOut();
    Navigator.pushNamedAndRemoveUntil(
        context, Routers.Login,(router) => false,);
  }

}