import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:song_tien_app/src/configs/configs.dart';
import 'package:song_tien_app/src/presentation/account/account.dart';
import 'package:song_tien_app/src/presentation/login/login.dart';
import 'package:song_tien_app/src/presentation/tour/tour.dart';
import 'package:song_tien_app/src/resource/model/model.dart';
import 'package:song_tien_app/src/utils/app_utils.dart';
import 'package:uuid/uuid.dart';
import '../presentation.dart';
import 'dart:io';
import 'dart:ui';
import 'package:firebase_auth/firebase_auth.dart';

class AccountScreen extends StatefulWidget {
  final User user;

  AccountScreen({Key key, this.user}) : super(key: key);
  @override
  _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  AccountViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<AccountViewModel>(
        viewModel: AccountViewModel(repository: Provider.of(context)),
        onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
        builder: (context, viewModel, child) {
          return Scaffold(
            backgroundColor:  Color(0xffbE8E8E8),
            resizeToAvoidBottomPadding: false,
            body: SafeArea(
              child: Column(
                children: [
                  Container(
                    height:80,
                    child: WidgetAppBar(
                      keyTitle: "account",
                      trans: true,
                      isBack: false,
                    ),
                  ),
                  Expanded(child: _buildBody(context)),
                ],
              ),
            ),
          );
        });
  }



  Widget _buildBody(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(5),
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 30,
            ),

            ///Ảnh
            buildAvatar(widget.user.photoURL),
            SizedBox(
              height: 20,
            ),
            Text(
              "${widget.user.displayName == null ? "" : widget.user.displayName}",
              style: AppStyles.DEFAULT_MEDIUM_BOLD,
            ),
            SizedBox(
              height: 40,
            ),

            _textField("Thông tin cá nhân", Icons.person,0),
            _textField("Điều khoản chính sách", Icons.security,1),
            _textField("Thay đổi mật khẩu", Icons.compass_calibration_rounded,2),
            _textField("Đăng xuất", Icons.widgets_sharp,3),
            SizedBox(height: 20,)
          ],
        ),
      ),
    );
  }

////Anh chọn ở đầu tiên
  buildAvatar(String avatar) {
    return Stack(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(100),
          child: avatar == null
              ? Image.asset(
                  AppImages.imgPeople,
                  fit: BoxFit.fill,
                  height: 110,
                  width: 110,
                )
              : Image.network(
                  avatar,
                  fit: BoxFit.fill,
                  height: 110,
                  width: 110,
                ),
        ),
        Positioned(
            bottom: 2,
            right: 12,
            child: InkWell(
              onTap: () {
                takeImage(context);
              },
              child: CircleAvatar(
                minRadius: 11,
                backgroundColor: Colors.red,
                child: Icon(
                  Icons.camera_alt,
                  color: Colors.white,
                  size: 13,
                ),
              ),
            ))
      ],
    );
  }

///// bảng chọn chụp ảnh và camera
  takeImage(context) {
    return showDialog(
      context: context,
      builder: (context) {
        return SimpleDialog(
          title: Text("Gallery Post",
              style: TextStyle(
                  color: Colors.black87, fontWeight: FontWeight.bold)),
          children: [
            SimpleDialogOption(
              child: Text("Capture Camera",
                  style: TextStyle(color: Colors.black87)),
              onPressed: () => _viewModel.captureImageWithCamera(),
            ),
            SimpleDialogOption(
              child: Text("Capture Gallery",
                  style: TextStyle(color: Colors.black87)),
              onPressed: () => _viewModel.pickerImage(),
            ),
            SimpleDialogOption(
              child: Text("Cancel", style: TextStyle(color: Colors.black87)),
              onPressed: () => Navigator.pop(context),
            ),
          ],
        );
      },
    );
  }

  _textField(String name, IconData icons,int id) {
    return WidgetTextField(
      readOnly: true,
      onTap: ()async => await _navigation(id),
      prefixIcon: Icon(
        icons,
        color: Colors.black45,
      ),
      hintText: name,
      textStyle:
          AppStyles.DEFAULT_MEDIUM.copyWith(fontSize: 16, color: Colors.black),
      radius: 7,
      mglr: 15,
      hintStyle:
          AppStyles.DEFAULT_MEDIUM.copyWith(fontSize: 14, color: Colors.black),
    );
  }

  Object _navigation(int id)async{
    switch(id){
      case 0:
       return await Navigator.pushNamed(context,Routers.User);
        break;
      case 1:
        return await  Navigator.pushNamed(context, Routers.Policy);
        break;
      case 2:
        return await Navigator.pushNamed(context, Routers.Password);
        break;
      case 3:
        return await showDialogLR(context);
        break;
    }
  }
  
  ////Ô CHọn Tour
  buildTour() {
    return SizedBox.expand(
      child: ListView.builder(
        padding: EdgeInsets.only(top: 5),
        itemCount: listAdress.length,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          Address address = listAdress[index];
          return InkWell(
            onTap: () {
              Navigator.pushNamed(context, Routers.DetailsTour,
                  arguments: address);
            },
            child: Padding(
              padding: const EdgeInsets.only(top: 2, left: 5, right: 5),
              child: Column(
                children: [
                  WidgetCustomListItem(
                    thumbnail: ClipRRect(
                        borderRadius: BorderRadius.circular(5),
                        child: Padding(
                          padding: const EdgeInsets.all(0.0),
                          child: Image.asset(
                            address.images,
                            fit: BoxFit.fill,
                            height: 100,
                            width: 250,
                          ),
                        )),
                    child: Programme(
                      address: address,
                    ),
                  ),
                  Divider(
                    height: 13,
                    color: Colors.grey.withOpacity(0.2),
                    thickness: 10,
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  showDialogLR(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) => Dialog(
            elevation: 8,
            insetPadding: EdgeInsets.only(left: 10, right: 10),
            shape: RoundedRectangleBorder(
              side: BorderSide.none,
              borderRadius: BorderRadius.circular(16),
            ),
            backgroundColor: HexColor.fromHex("#fdfefe"),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                const SizedBox(
                  height: 30,
                ),
                Text(
                  "Bạn chắc chắn đăng xuất tài khoản",
                  style: AppStyles.DEFAULT_MEDIUM.copyWith(fontSize: 16),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 15,
                ),
                const SizedBox(
                  height: 12,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    builCancel(),
                    const SizedBox(
                      width: 1,
                    ),
                    buildOk(),
                  ],
                )
              ],
            )));
  }

  //Nút hủy bỏ
  builCancel() {
    return Expanded(
        child: InkWell(
      borderRadius: BorderRadius.only(bottomLeft: Radius.circular(16)),
      onTap: () => Navigator.pop(context, false),
      child: Container(
        decoration: BoxDecoration(
          color: Color(0xff445E37),
          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(16)),
        ),
        height: 45,
        child: Center(
          child: Text(
            "Hủy bỏ",
            style: AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(color: Colors.white),
          ),
        ),
      ),
    ));
  }

  ///Nút ok
  buildOk() {
    return Expanded(
        child: InkWell(
      borderRadius: BorderRadius.only(bottomLeft: Radius.circular(16)),
      onTap: () async => await _viewModel.signOut(),
      child: Container(
        decoration: BoxDecoration(
            color: Color(0xff445E37),
            borderRadius: BorderRadius.only(bottomRight: Radius.circular(16))),
        height: 45,
        child: Center(
          child: Text(
            "OK",
            style: AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(color: Colors.white),
          ),
        ),
      ),
    ));
  }

  @override
  bool get wantKeepAlive => true;
}
