import 'package:flutter/material.dart';
import '../../resource/resource.dart';
import '../base/base.dart';
import 'package:rxdart/rxdart.dart';
class DistrictViewModel extends BaseViewModel{

  final OtherRepository repository;

  DistrictViewModel({@required this.repository});
  final searchDistrict = new BehaviorSubject<List<String>>();
  final TextEditingController districtController = TextEditingController();
  String district = "Quận/ huyện";
  List listDistrict = [];


  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    searchDistrict.close();
  }
  init()async{
    final NetworkState<OtherApplication> rs = await repository.getMoreApps();
    final otherApp = rs.data;
    print("Other apps: ${otherApp?.toJson() ?? "Nullllll!"}");
  }
}