import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:song_tien_app/src/presentation/trip/trip.dart';
import 'package:song_tien_app/src/presentation/widgets/widget_button_gradientAni.dart';
import 'package:song_tien_app/src/resource/model/app_valid.dart';
import 'package:song_tien_app/src/resource/model/model.dart';
import '../../configs/configs.dart';
import '../presentation.dart';

class TripScreen extends StatefulWidget {
  final Address address;

  TripScreen({Key key, this.address}) : super(key: key);

  @override
  _TripScreenState createState() => _TripScreenState();
}

class _TripScreenState extends State<TripScreen>
    with SingleTickerProviderStateMixin, ResponsiveWidget {
  DeviceScreenType type;

  TripViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    AppSizeConfig().init(context);
    return BaseWidget<TripViewModel>(
        viewModel: TripViewModel(repository: Provider.of(context)),
        onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
        builder: (context, viewModel, child) {
          return SafeArea(
            child: Scaffold(
                backgroundColor: Colors.white,
                body: buildUi(context: context)),
          );
        });
  }

  List<String> listString = [
    "Mã số Chương trình",
    "Tên chuyến",
    "Ngày khởi hành :",
    "Giờ khởi hành:",
    "Giờ kết thúc:",
    "Mã số tàu - Tên:",
    "Địa điểm xuất bến:",
    "Người hướng dẫn:",
    "Phí (giá vé):"
  ];

  _buildButton() {
    return Padding(
      padding: const EdgeInsets.only(left: 15, right: 15),
      child: WidgetButtonGradientAni(
          title: "save",
          colorStart: AppColors.primary,
          colorEnd: AppColors.primary,
          width: Get.width,
          loading: false,
          action: () {
            // _viewModel.unFocus();
          },
          textStyle: AppStyles.DEFAULT_MEDIUM.copyWith(color: AppColors.white)),
    );
  }

  _textField({String name, TextInputType textInputType, String suffixText,List<TextInputFormatter> input}) {
    return Stack(
      alignment: Alignment(0, 0),
      children: [
        WidgetTextField(
          listTextInputFormatter: [],
          textAlign: TextAlign.end,
          textInputType: textInputType,
          mglr: 15,
          top: 6,
          hintText: suffixText,
          height: 55,
          contentPd: EdgeInsets.only(left: 15, right: 15, bottom: 4, top: 4),
          radius: 5,
        ),
        Positioned(
            left: 28,
            child: Text(name,
                style: AppStyles.TEXT_COLOR_LIGHT.copyWith(
                  color: Colors.grey.withOpacity(0.5),
                )))
      ],
    );
  }

  _rowTextField(String starName,
      {String endName ="",
      double height = 45,
      bool start = false,
      bool checkColor = false,
      bool checkIcon = false,
      String name = "",
      Function onTap}) {
    return Container(
      alignment: Alignment.centerLeft,
      width: double.infinity,
      padding: EdgeInsets.only(left:15, right: 15),
      margin: EdgeInsets.only(top: 6, bottom: 6, left: 15, right: 15),
      height: height,
      decoration: buildDecoration(),
      child: start
          ? Text(
              starName,
              style: AppStyles.TEXT_COLOR_LIGHT.copyWith(
                color: Color(0xffDEDEDE),
              ),
            )
          : Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  starName,
                  style: AppStyles.TEXT_COLOR_LIGHT.copyWith(
                    color: Color(0xffDEDEDE),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [

                    checkIcon
                        ? name == null
                            ? Container()
                            : Text(
                                "${name}",
                                style: AppStyles.DEFAULT_MEDIUM
                                    .copyWith(fontWeight: FontWeight.w500),
                              )
                        : SizedBox(),
                    checkIcon
                        ? IconButton(
                            onPressed: () => onTap(),
                            icon: Icon(
                              Icons.arrow_drop_down_sharp,
                              color: AppColors.primary,
                              size: 28,
                            ))
                        : Text(endName)
                  ],
                )
              ],
            ),
    );
  }


  buildHeader() {
    return Container(
      height: type ==DeviceScreenType.mobile?270:230,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: ExactAssetImage(
                AppImages.imgSongTien2,
              ),
              fit: BoxFit.fill)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          IconButton(
              icon: Icon(
                Icons.arrow_back_ios_outlined,
                color: Colors.white,
              ),
              onPressed: () {}),
        ],
      ),
    );
  }

  Widget _buildBody(BuildContext context) {
    return SingleChildScrollView(
        child: Column(
      children: [
        Container(
          padding: const EdgeInsets.only(bottom: 5),
          decoration: BoxDecoration(
            color: Color(0xffE8E8E8),
          ),
          child: Stack(
            children: [
              buildHeader(),
              Padding(
                padding: const EdgeInsets.only(top: 170),
                child: Column(
                  children: [
                    buildTitle(),
                    SizedBox(
                      height: 14,
                    ),
                    buildCreate()
                  ],
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 15,
        ),
        _buildButton(),
        SizedBox(
          height: 15,
        )
      ],
    ));
  }

  buildCreate() {
    return Column(
      children: [
        Text(
          AppLocalizations.of(context).translate("create_title").toUpperCase(),
          style: AppStyles.DEFAULT_MEDIUM_BOLD
              .copyWith(fontWeight: FontWeight.w700, color: AppColors.primary),
          textAlign: TextAlign.center,
        ),
        SizedBox(
          height: 14,
        ),
        _rowTextField(listString[0], endName: widget.address.code),
        _textField(name: listString[1]),
        _rowTextField(
          listString[2],
          endName: widget.address.code,
          checkIcon: true,
          name: _viewModel.birthDays,
          onTap: () {
            showDatePicker(
                    context: context,
                    initialDate: DateTime.now(),
                    firstDate: DateTime(2001),
                    lastDate: DateTime(2022))
                .then((date) {
              setState(() {
                _viewModel.birthDays = DateFormat("dd/MM/yyyy").format(date);
              });
            });
          },
        ),
        _textField(
            name: listString[3],
            textInputType: TextInputType.datetime,
            suffixText: "HH:MM"),

        _textField(
            name: listString[4],
            textInputType: TextInputType.datetime,
            suffixText: "HH:MM"),

        _rowTextField(listString[5], checkIcon: true),

        _textField(
          name:listString[6],
            textInputType: TextInputType.text,
            suffixText: ""
        ),

        _rowTextField(listString[7],
             checkIcon: true,
              onTap: (){},
               ),
        _textField(name:listString[8],
            suffixText: "VND",
            input:[CurrencyTextInputFormatter()],
            textInputType: TextInputType.number )
      ],
    );
  }
  ///
  /// Tiều đề  Tham quan
  buildTitle() {
    var size = MediaQuery.of(context).size;
    return Container(
      height: AppSizeConfig.screenHeight * 0.2,
      width: double.infinity,
      margin: EdgeInsets.only(left: 15, right: 15, bottom: 8),
      decoration: buildDecoration(),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
              width: AppSizeConfig.screenWidth * 0.9,
              child: Text(
                widget.address.title,
                style: AppStyles.DEFAULT_LARGE_BOLD
                    .copyWith(color: Color(0xff5A7D48), fontSize: 17),
                textAlign: TextAlign.center,
              )),
          SizedBox(
            height: 5,
          ),
          WidgetStart(
            rating: widget.address.raitting,
          )
        ],
      ),
    );
  }

  Decoration buildDecoration([Color color = Colors.white]) {
    return BoxDecoration(
        color: color,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.0),
            spreadRadius: 2,
            blurRadius: 5,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
        borderRadius: BorderRadius.circular(5));
  }

  //// Nội dung tour
  Widget buildDescription() {
    return WidgetDescription(
      address: widget.address,
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    type = DeviceScreenType.desktop;
    // TODO: implement buildDesktop
    return _buildBody(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    type = DeviceScreenType.mobile;
    // TODO: implement buildDesktop
    return _buildBody(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    type = DeviceScreenType.tablet;
    // TODO: implement buildDesktop
    return _buildBody(context);
  }
}
