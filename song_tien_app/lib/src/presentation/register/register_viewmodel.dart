import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:song_tien_app/src/presentation/otp/otp.dart';
import 'package:song_tien_app/src/presentation/presentation.dart';
import 'package:toast/toast.dart';
import '../../resource/resource.dart';
import '../base/base.dart';

class RegisterViewModel extends BaseViewModel {

  TextEditingController phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();


  TextEditingController pinController = TextEditingController();
  GlobalKey<FormState> formSignUp = GlobalKey<FormState>();



  String _verificationID;
  String get verificationID => _verificationID;
  final String phoneNumber;
  bool obSecureText = true;

  FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  RegisterViewModel({this.phoneNumber});

  init() {
    // verifyPhoneNumber();
  }


  showPassword() {
    obSecureText = !obSecureText;
    notifyListeners();
  }


  switchScreen() {
    unFocus();
    obSecureText = true;
    // isSignIn = !isSignIn;
    phoneController.clear();
    passwordController.clear();
    confirmPasswordController.clear();
    Navigator.pop(context);
    notifyListeners();
  }

  //
  // signUp() {
  //   unFocus();
  //   if (formSignUp.currentState.validate()) {
  //     Navigator.pushNamed(context, Routers.Otp,arguments: phoneController.text);
  //     // handleRegister(
  //     //   phone: this.phoneController.text,
  //     //   password: this.passwordController.text,
  //     // );
  //   }
  // }

  String convertPhoneNumber(String phoneNumber) {
    if (phoneNumber.startsWith("+84")) return phoneNumber;
    var result = phoneNumber.replaceFirst("0", "+84");
    return result;
  }

  Future<dynamic> showNotification(
      {@required String keyTitle,
        bool trans = true,
        String keyAction,
        Function action}) async {
    await showDialog(
      context: context,
      builder: (context) => DialogError(
        keyTitle: keyTitle,
        action: action,
        trans: trans,
        keyAction: keyAction,
      ),
    );
  }


  get verificationCompleted => (AuthCredential authCredential) async {

    firebaseAuth
        .signInWithCredential(authCredential)
        .then((authResult) async {
      final user = authResult?.user;
      if (user == null) {
        handleFailure(msg: "login_failure");
        setLoading(false);
      } else
        handleFailure(msg: "register_success");
        // login(user);
    }).catchError((Object error) {
      handleFailure(msg: "login_failure");
      setLoading(false);
    });
  };


// sự kiện khi quá trình xác thực lỗi
// thông báo lỗi ra app
  get verificationFailed => (FirebaseAuthException exception) async {
    if (exception.code == 'invalid-phone-number') {
      handleFailure(msg:'The provided phone number is not valid.');
    print(exception.message);
    setLoading(false);
  }else{
      setLoading(false);
      handleFailure(msg: exception.message);}
  };


  get codeSent => (String verificationId, [int forceResendingToken]) async {
    // verify code success
    var arguments = VerifyArguments(
      phoneNumber: convertPhoneNumber(phoneController.text),
      verificationId: verificationId??"",
    );
    if(arguments.verificationId !="") {
      Navigator.pushNamed(context, Routers.Otp,arguments: arguments);
    }
    setLoading(false);
  };

// sự kiện tự động timeout khi nhận code otp
  get codeAutoRetrievalTimeout => (String verificationId) async {
    setLoading(false);
    print('codeAutoRetrievalTimeout $verificationId');
  };

// khi bấm vào nút login số điện thoại sẽ gọi hàm này để thực thi tất cả

  void sendPhoneNumberToFirebase() {
    if (formSignUp.currentState.validate()) {
      firebaseAuth.verifyPhoneNumber(
          phoneNumber: convertPhoneNumber(phoneController.text),
          verificationCompleted: verificationCompleted,
          verificationFailed: verificationFailed,
          codeAutoRetrievalTimeout: codeAutoRetrievalTimeout,
          codeSent: codeSent
      );
    }

  }



  Future handleFailure({String msg}) async {
    await showNotification(
      keyTitle: msg ?? "login_failure",
      trans: msg != null,
    );
  }
}


class VerifyArguments {
  String phoneNumber;
  String verificationId;
  bool isRegister;
  UserModel user;

  VerifyArguments(
      {this.phoneNumber, this.verificationId,this.isRegister,this.user});
}
