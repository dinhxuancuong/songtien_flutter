import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:song_tien_app/src/configs/configs.dart';
import 'package:song_tien_app/src/presentation/login/login.dart';
import 'package:song_tien_app/src/presentation/register/register.dart';
import 'package:song_tien_app/src/presentation/widgets/widget_button_gradientAni.dart';
import 'package:song_tien_app/src/resource/model/app_valid.dart';

import '../presentation.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {


  RegisterViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<RegisterViewModel>(
      viewModel: RegisterViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          body: SingleChildScrollView(
            child: Column(
              children: [
                _buildHeader(),
                _buildBody(),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildHeader() {
    return Column(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(60),
          ),
          child: Image.asset(
            AppImages.imgIsLand3,
            width: Get.width,
            height: 210,
            fit: BoxFit.fill,
          ),
        ),
        const SizedBox(height: 10),
        WidgetLogo(),
      ],
    );
  }

  Widget _buildBody() {
    return AnimatedSwitcher(
      transitionBuilder: (child, animation) => RotationTransition(turns: animation, child: child),
      layoutBuilder: (widget, list) => Stack(children: [widget, ...list]),
      switchInCurve: Curves.easeInBack,
      switchOutCurve: Curves.easeInBack.flipped,
      duration: Duration(seconds: 1),
      child: GestureDetector(
        // onTap: _viewModel.unFocus,
        child: StreamBuilder<bool>(
          stream: _viewModel.loadingSubject,
          builder: (context, snapshot) {
            bool loading = snapshot.data ?? false;
            return Container(
              width: Get.width,
              padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
              color: Colors.transparent,
              child:_buildSignUp(loading),
            );
          },
        ),
      ),
    );
  }


  Widget _buildSignUp(bool loading) {
    return Column(
      children: [
        Container(
          width: Get.width,
          height: 110,
          alignment: Alignment.center,
          padding: EdgeInsets.only(top: 10),
          child: Text(
            AppLocalizations.of(context).translate("register"),
            style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(
              color: AppColors.primary,
              fontSize: 24,
            ),
          ),
        ),
        Form(
          key: _viewModel.formSignUp,
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                _buildTextField(
                  validator: (value) =>AppValid.checkValidate(ValidateType.Number, value),
                  readOnly: loading,
                  inputType: TextInputType.phone,
                  controller: _viewModel.phoneController,
                  hintText: "phone",
                ),
                SizedBox(height: 8),
                _buildTextField(
                  // validator: (value) =>AppValid.checkValidate(ValidateType.Password, value),
                  readOnly: loading,
                  controller: _viewModel.passwordController,
                  hintText: "password",
                  isPassword: true,
                ),
                SizedBox(height: 8),
                _buildTextField(
                  // validator: (value) =>AppValid.checkValidate(ValidateType.Password, value),
                  readOnly: loading,
                  controller: _viewModel.confirmPasswordController,
                  hintText: "confirm_password",
                  isPassword: true,
                ),
              ],
            ),
          ),
        ),
        const SizedBox(height: 25),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            WidgetButtonGradientAni(
              width: Get.width - 15,
              title: "register",
              colorStart: AppColors.primary,
              colorEnd: AppColors.primary,
              action: _viewModel.sendPhoneNumberToFirebase,
              loading: loading,
              textStyle: AppStyles.DEFAULT_MEDIUM.copyWith(
                color: AppColors.white,
              ),
            ),
            const SizedBox(height: 80),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  AppLocalizations.of(context).translate("have_account"),
                  style: AppStyles.DEFAULT_MEDIUM.copyWith(
                    color: AppColors.black,
                  ),
                ),
                const SizedBox(width: 5),
                GestureDetector(
                  onTap: () => _viewModel.switchScreen(),
                  child: Text(
                    AppLocalizations.of(context).translate("login"),
                    style: AppStyles.DEFAULT_MEDIUM.copyWith(
                      color: AppColors.red,
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ],
    );
  }



  Widget _buildIconLogin(String imageUrl, Function onTap, bool loading) {
    return InkWell(
      onTap: loading ? null : onTap,
      child: Container(
        width: 48,
        height: 48,
        child: Image.asset(
          imageUrl,
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Widget _buildTextField({
    TextEditingController controller,
    String hintText,
    TextInputType inputType,
    FormFieldValidator<String> validator,
    bool isPassword = false,
    bool readOnly = false,
  }) {
    return TextFormField(
      controller: controller,
      readOnly: readOnly,
      obscureText: isPassword ? _viewModel.obSecureText : false,
      validator: validator,
      keyboardType: inputType ?? TextInputType.text,
      decoration: InputDecoration(
        suffixIcon: isPassword
            ? Padding(
          padding: const EdgeInsets.only(right: 12),
          child: IconButton(
            icon: Icon(
              _viewModel.obSecureText ? Icons.remove_red_eye_outlined : Icons.remove_red_eye,
              color: AppColors.grey,
            ),
            color: Colors.black,
            onPressed: () => _viewModel.showPassword(),
          ),
        )
            : null,
        focusedBorder: _buildBorderTextField(AppColors.primary),
        enabledBorder: _buildBorderTextField(AppColors.primary),
        errorBorder: _buildBorderTextField(AppColors.red),
        focusedErrorBorder: _buildBorderTextField(AppColors.primary),
        contentPadding: EdgeInsets.fromLTRB(25.0, 16.0, 25.0, 16.0),
        filled: true,
        fillColor: Colors.white,
        hintText: AppLocalizations.of(context).translate(hintText),
        hintStyle: AppStyles.DEFAULT_MEDIUM.copyWith(
          color: AppColors.grey,
          fontSize: 16,
        ),
      ),
    );
  }

  OutlineInputBorder _buildBorderTextField(Color color) {
    return OutlineInputBorder(
      borderRadius: BorderRadius.circular(150),
      borderSide: BorderSide(
        color: color,
        width: 1.2,
      ),
    );
  }
}

