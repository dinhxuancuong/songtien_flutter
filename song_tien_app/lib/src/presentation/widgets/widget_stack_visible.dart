import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:song_tien_app/src/presentation/presentation.dart';
import 'package:responsive_builder/responsive_builder.dart';


class WidgetStackVisible extends StatelessWidget{
  final String title;
  final String images;
  final DeviceScreenType type;

  const WidgetStackVisible({Key key, this.title, this.images, this.type,}) : super(key: key);




   @override Widget build(BuildContext context) {
    // TODO: implement build
   return Stack(
      overflow: Overflow.visible,
      // alignment: Alignment.bottomCenter,
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: Image.asset(
            images,
            height: Get.height*0.3,
            width:Get.width-100,
            fit: BoxFit.fill,
          ),
        ),
        Positioned(
          left: 0,
          right: 0,
          height: type == DeviceScreenType.mobile?Get.height/2/5:Get.height/2/6,
          bottom:type == DeviceScreenType.mobile?28:25,

          child: Container(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Flexible(
                    child: Text(
                      title,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(height: 5,),
                  WidgetStart(rating: 5,)
                ],
              ),
            ),
            margin:
            EdgeInsets.only(left: 12, right:12 ),
            width: double.infinity,
            decoration: buildDecoration(),
          ),
        )
      ],
    );
  }
}
Decoration buildDecoration([Color color = Colors.white]) {
  return BoxDecoration(
      color: color,
      boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(0.5),
          spreadRadius: 2,
          blurRadius: 3,
          offset: Offset(0, 3), // changes position of shadow
        ),
      ],
      borderRadius: BorderRadius.circular(5));
}