import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:song_tien_app/src/configs/configs.dart';
import 'package:song_tien_app/src/resource/model/model.dart';

class WidgetDescription extends StatelessWidget{

   final Address address;

  const WidgetDescription({Key key, this.address}) : super(key: key);

  @override Widget build(BuildContext context) {
    // TODO: implement build
     return Container(
      padding: const EdgeInsets.only(left: 20,right: 20,top: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text(
                "Mã số chương trình: ",
                style: AppStyles.DEFAULT_MEDIUM_BOLD
                    .copyWith(color: Color(0xff5A7D48)),
              ),
              Spacer(),
              Text(
               address.code,
                style: AppStyles.DEFAULT_LARGE_BOLD
                    .copyWith(fontSize: 17, fontWeight: FontWeight.w400),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text(
                "Loại: ",
                style: AppStyles.DEFAULT_MEDIUM_BOLD
                    .copyWith(color: Color(0xff5A7D48)),
              ),
              Spacer(),
              Text(
              address.category,
                style: AppStyles.DEFAULT_LARGE_BOLD
                    .copyWith(fontSize: 17, fontWeight: FontWeight.w400),
              ),
            ],
          ),
          Text("Nội dung Tour tham quan: ",
              style: AppStyles.DEFAULT_MEDIUM_BOLD
                  .copyWith(color: Color(0xff5A7D48))),
          SizedBox(
            height: 10,
          ),
          Expanded(child: Text(address.content)),
          // SizedBox(
          //   height: 15,
          // ),
        ],
      ),
    );
  }
}
