import 'dart:math';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:readmore/readmore.dart';
import 'package:song_tien_app/src/configs/configs.dart';
import 'package:song_tien_app/src/presentation/place/place.dart';
import 'package:song_tien_app/src/presentation/presentation.dart';
import 'package:song_tien_app/src/resource/model/app_valid.dart';
import 'package:song_tien_app/src/resource/model/model.dart';

class WidgetRow extends StatelessWidget {
  final Decoration decoration;
  final Ticket ticket;
  final Function onTap;
  final bool select;
  final bool check;
 final Address address;

   WidgetRow({Key key, this.decoration, this.ticket, this.onTap, this.select, this.address,this.check = true}) : super(key: key);




  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: [
        Column(
          children: [
            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.only(left: 5, right: 5),
              margin: EdgeInsets.only(left: 5, right: 5, top: 5, bottom: 5),
              height: 40,
              width: double.infinity,
              decoration: decoration,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(ticket.departureDay,style: AppStyles.DEFAULT_MEDIUM.copyWith(color: Color(0xff445E37)),),
                  Text(" - ",style: AppStyles.DEFAULT_MEDIUM.copyWith(color: Color(0xff445E37))),
                  Text(ticket.code,style: AppStyles.DEFAULT_MEDIUM.copyWith(color: Color(0xff445E37))),
                  Text(" - ",style: AppStyles.DEFAULT_MEDIUM.copyWith(color: Color(0xff445E37))),
                  Flexible(
                      child: Text(
                    ticket.title,style: AppStyles.DEFAULT_MEDIUM.copyWith(color: Color(0xff445E37)),
                    overflow: TextOverflow.ellipsis,
                  )),
                  InkWell(
                    onTap: () => onTap() ?? AppSize(),
                    child: Icon(
                      select
                          ? Icons.arrow_drop_down_sharp
                          : Icons.arrow_drop_up_sharp,color: Color(0xff445E37),
                      size: 30,
                    ),
                  )
                ],
              ),
            ),
            select
                ? SizedBox(
                    child: WidgetDeclaration(
                      select: check,
                      ticket: ticket,
                     list: listString,
                      onTap:  () async {
                        print("mamamam");
                        await Navigator.push(context, MaterialPageRoute(builder: (context) => PlaceScreen(address: address,ticket: ticket,)));
                      },
                  ))
                : Container()
          ],
        ),
      ],
    );
  }

  AppSize() {}
  List<String> listString = [
    "Ngày khởi hành :",
    "Giờ khởi hành:",
    "Giờ kết thúc:",
    "Mã số tàu (Tên):",
    "Địa điểm xuất bến:",
    "Người hướng dẫn:",
    "Trạng thái:",
    "Phí (giá vé):"
  ];
}
