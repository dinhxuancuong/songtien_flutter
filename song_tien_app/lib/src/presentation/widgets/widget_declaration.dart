import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:song_tien_app/src/configs/configs.dart';
import 'package:song_tien_app/src/presentation/place/place.dart';
import 'package:song_tien_app/src/resource/model/model.dart';

import '../presentation.dart';

class WidgetDeclaration extends StatelessWidget {
   List<String>  list;
   Ticket ticket;
   bool select;
   Function onTap;
   TextStyle textStyle = AppStyles.DEFAULT_MEDIUM_BOLD
       .copyWith(fontWeight: FontWeight.w600);
   WidgetDeclaration(
      {Key key, this.list, this.ticket, this.select = true,this.onTap,this.textStyle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(
      padding: EdgeInsets.only(left: 15, right: 15, top: 5, bottom: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          _buildRow(list[0], ticket.departureDay),
          _buildRow(list[1], ticket.departureTime),
          _buildRow(list[2], ticket.TimeEnd),
          _buildRow(list[3], ticket.codeName),
          _buildRow(list[4], ticket.address),
          _buildRow(list[5], ticket.instructorName),
          _buildRow(list[6], ticket.check),
          _buildRow(list[7], "${ticket.pay} VND"),
          select
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    InkWell(
                      onTap: () => onTap(),
                      child: Icon(
                            Icons.search,
                            color: Colors.pinkAccent,
                            size: 30,
                      ),
                    ),
                    Center(
                        child: Text(
                        AppLocalizations.of(context).translate("details_title"),
                      style: TextStyle(color: Colors.pink, fontSize: 16),
                      textAlign: TextAlign.center,
                    ))
                  ],
                )
              : Container(),
          SizedBox(
            height: 2,
          )
        ],
      ),
    );
  }

  _buildRow(String title, Object name) {
    return Padding(
      padding: const EdgeInsets.only(top: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Flexible(
            flex: 2,
            // fit: FlexFit.loose,
            child: Text(
              title,
              // textAlign: TextAlign.,
              style:textStyle,
            ),
          ),
          SizedBox(
            width: 15,
          ),
          Flexible(
              child: Text("${name}",
                  textAlign: TextAlign.end,
                  style: AppStyles.TEXT_COLOR_LIGHT.copyWith(
                    color: Colors.black45,
                  )))
        ],
      ),
    );
  }
}
