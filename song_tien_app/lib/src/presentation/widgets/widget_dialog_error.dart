import 'package:flutter/material.dart';

class DialogError extends StatelessWidget {
  final String keyTitle;
  final String keyAction;
  final bool trans;
  final Function action;

  const DialogError(
      {this.keyTitle, this.trans = true, this.action, this.keyAction});

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Dialog(
          elevation: 8,
          shape: RoundedRectangleBorder(
            side: BorderSide.none,
            borderRadius: BorderRadius.circular(16),
          ),
          backgroundColor: Colors.white60,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(
                height: 10,
              ),
              Text("Đồng ý"),
              const SizedBox(
                height: 6,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Text(keyTitle,style: TextStyle(color: Colors.black,fontSize: 15),),
              ),
              const SizedBox(
                height: 12,
              ),
              InkWell(
                borderRadius:
                BorderRadius.only(bottomLeft: Radius.circular(16)),
                onTap: action ?? () => Navigator.pop(context, true),
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(16),
                          bottomLeft: Radius.circular(16))),
                  height: 45,
                  child: Center(
                    child:   Text("Đồng ý"),
                  ),
                ),
              )
            ],
          )),
    );
  }
}