import 'package:flutter/material.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class WidgetStart extends StatelessWidget{
  double rating;
  double size;
  WidgetStart({Key key, this.rating,this.size = 14}) : super(key: key);

  @override Widget build(BuildContext context) {
    // TODO: implement build
    return SmoothStarRating(
      color: Colors.orangeAccent,
      rating: rating,
      isReadOnly: false,
      size: size,
      filledIconData: Icons.star,
      halfFilledIconData: Icons.star_half,
      defaultIconData: Icons.star_border,
      starCount: 5,
      allowHalfRating: false,
      spacing: 1.0,
      // onRated: (value) {
      //   // rating = value;
      //   print("rating value -> $value");
      //   // print("rating value dd -> ${value.truncate()}");
      // },
    );
  }
}
