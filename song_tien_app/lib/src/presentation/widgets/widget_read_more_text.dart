import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:readmore/readmore.dart';
import 'package:song_tien_app/src/configs/configs.dart';

class WidgetReadMoreText extends StatelessWidget {
  final String name;
  final int trimLines;
  WidgetReadMoreText({Key key, this.name,this.trimLines = 1}) : super(key: key);


  @override Widget build(BuildContext context) {
    // TODO: implement build
    return  ReadMoreText(
      name,
      trimLength:50 ,
      locale: Locale.fromSubtags(),
      trimLines:trimLines,
      trimMode: TrimMode.Length,
      trimCollapsedText: '...',
      colorClickableText: Colors.grey[300],
      style: AppStyles.TEXT_ADRESS,
      textAlign: TextAlign.start,
    );

  }
}