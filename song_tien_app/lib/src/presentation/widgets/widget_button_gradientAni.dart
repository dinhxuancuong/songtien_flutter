
import 'package:flutter/material.dart';
import 'package:song_tien_app/src/configs/configs.dart';

import '../presentation.dart';

class WidgetButtonGradientAni extends StatefulWidget {
  final bool loading;
  final String title;
  final Function action;
  final Color colorStart;
  final Color colorEnd;
  final Color colorLoading;
  final Alignment alignmentStart;
  final Alignment alignmentEnd;
  final EdgeInsets padding;
  final double height;
  final double width;
  final TextStyle textStyle;
  final bool trans;

  const WidgetButtonGradientAni(
      {this.alignmentStart,
        this.alignmentEnd,
        this.colorLoading,
        this.colorEnd,
        this.colorStart,
        this.loading = false,
        this.padding,
        this.height,
        this.textStyle,
        this.trans = true,
        @required this.width,
        @required this.title,
        @required this.action});

  static const double HEIGHT = 45.0;

  @override
  _WidgetButtonGradientAniState createState() =>
      _WidgetButtonGradientAniState();
}

class _WidgetButtonGradientAniState extends State<WidgetButtonGradientAni>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation<double> _sizeHeightAnimation;
  Animation<double> _sizeWidthAnimation;

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  void initState() {
    super.initState();
    _controller =
        AnimationController(duration: Duration(milliseconds: 200), vsync: this);
    _sizeHeightAnimation = TweenSequence(<TweenSequenceItem<double>>[
      TweenSequenceItem(
          tween: Tween<double>(
              begin: widget.height ?? WidgetButtonGradientAni.HEIGHT,
              end: (widget.height ?? WidgetButtonGradientAni.HEIGHT) * 1.2),
          weight: (widget.height ?? WidgetButtonGradientAni.HEIGHT) * 1.2),
      TweenSequenceItem(
          tween: Tween<double>(
              begin: (widget.height ?? WidgetButtonGradientAni.HEIGHT) * 1.2,
              end: widget.height ?? WidgetButtonGradientAni.HEIGHT),
          weight: widget.height ?? WidgetButtonGradientAni.HEIGHT),
    ]).animate(_controller);
    _sizeWidthAnimation = TweenSequence(<TweenSequenceItem<double>>[
      TweenSequenceItem(
          tween: Tween<double>(begin: widget.width, end: widget.width * 1.2),
          weight: widget.width * 1.2),
      TweenSequenceItem(
          tween: Tween<double>(begin: widget.width * 1.2, end: widget.width),
          weight: widget.width),
    ]).animate(_controller);
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _controller,
      builder: (_, child) {
        return Container(
          height: _sizeHeightAnimation.value,
          width: _sizeWidthAnimation.value,
          child: child,
        );
      },
      child: RaisedButton(
        onPressed: widget.loading
            ? null
            : () {
          if (_controller.status == AnimationStatus.completed)
            _controller.reverse();
          else if (_controller.status == AnimationStatus.dismissed)
            _controller.forward();
          Future.delayed(Duration(milliseconds: 500), () {
            widget.action.call();
          });
        },
        shape:
        RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
        padding: const EdgeInsets.all(0.0),
        child: Ink(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  widget.colorStart ?? AppColors.black,
                  widget.colorEnd ?? AppColors.grey
                ],
                end: widget.alignmentStart ?? Alignment.centerLeft,
                begin: widget.alignmentEnd ?? Alignment.centerRight,
              ),
              borderRadius: BorderRadius.circular(30.0)),
          child: Container(
            padding: widget.padding ?? const EdgeInsets.all(0.0),
            alignment: Alignment.center,
            child: !widget.loading
                ? Text(
              widget.trans
                  ? AppLocalizations.of(context).translate(widget.title)
                  : widget.title,
              textAlign: TextAlign.center,
              style: widget.textStyle ?? TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
            )
                : Center(
              child: WidgetLoading(
                dotOneColor: widget.colorLoading ?? Colors.white,
                dotTwoColor: widget.colorLoading ?? Colors.white,
                dotThreeColor: widget.colorLoading ?? Colors.white,
                dotType: DotType.circle,
                duration: Duration(milliseconds: 1000),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
