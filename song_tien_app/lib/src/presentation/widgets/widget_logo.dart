import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:song_tien_app/src/configs/configs.dart';

class WidgetLogo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(
          AppImages.imgLogo,
          width: 90,
          height: 120,
          fit: BoxFit.fill,
        ),
        const SizedBox(width: 12),
        Container(
          height: 40,
          width: 1.8,
          color: AppColors.primary,
        ),
        const SizedBox(width: 12),
        Flexible(
          child: Container(
            width: 165,
            height:50,
            child: Text(
              "SÔNG TIÊN CORPORATION",
              style: AppStyles.DEFAULT_LARGE.copyWith(
                fontSize: 18,
                color: AppColors.primary,fontWeight: FontWeight.w500
              ),
            ),
          ),
        ),
      ],
    );
  }
}
