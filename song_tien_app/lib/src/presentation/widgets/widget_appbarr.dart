import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:song_tien_app/src/configs/configs.dart';


class WidgetAppBar extends StatelessWidget {
  final Color colorButton;
  final Color colorTitle;
  final double sizeIconBack;
  final double height;
  final String keyTitle;
  final Widget title;
  final Function actionBack;
  final Widget leading;
  final List<Widget> actions;
  final Alignment alignTitle;
  final bool trans;
  final bool isBack;
  final bool isSpace;
  final MainAxisAlignment align;

  const WidgetAppBar({
    this.height,
    this.title,
    this.align,
    this.leading,
    this.colorButton = Colors.black,
    this.colorTitle = Colors.white,
    this.alignTitle,
    this.sizeIconBack = 18,
    this.keyTitle,
    this.isSpace = false,
    this.trans = true,
    this.isBack = true,
    this.actionBack,
    this.actions,
  });

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        width: Get.width,
        height: Get.height + (height ?? 0),
        color: AppColors.primary,
        child: Row(
          mainAxisAlignment: align ?? MainAxisAlignment.start,
          children: [
            leading ??
                WidgetButtonBack(
                  action: actionBack,
                  size: sizeIconBack,
                  color: colorButton,
                  isBack: isBack,
                ),
            if (isSpace) const SizedBox(width: 8),
            Expanded(
              child: title ??
                  (keyTitle == null
                      ? const SizedBox()
                      : Align(
                    alignment: alignTitle ?? Alignment.center,
                    child: Text(
                      trans ? AppLocalizations.of(context).translate(keyTitle) : keyTitle,
                      style: AppStyles.DEFAULT_TEXT_TINOS.copyWith(
                        color: colorTitle ?? Colors.white,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  )),
            ),
            if (isSpace) const SizedBox(width: 8),
            actions == null
                ? Opacity(
              opacity: 0,
              child: IconButton(
                icon: Icon(
                  Icons.arrow_back_ios_rounded,
                  color: colorButton,
                  size: sizeIconBack,
                ),
              ),
            )
                : Row(children: actions),
          ],
        ),
      ),
    );
  }
}

class WidgetButtonBack extends StatelessWidget {
  final Color color;
  final double size;
  final action;
  final bool isBack;

  const WidgetButtonBack({
    this.color,
    this.size,
    this.action,
    this.isBack = true,
  });

  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: isBack ? 1 : 0,
      child: IconButton(
        icon: Container(
          height: 20,
          decoration: BoxDecoration(
            border: Border.all(width: 0.8,color: Colors.white,),
            shape: BoxShape.circle,
          ),
            child: Icon(Icons.arrow_back_ios,color: Colors.white,size: 12,),alignment: Alignment(0.2,0),),
        onPressed: isBack ? action ?? () => Navigator.pop(context) : null,
      ),
    );
  }
}
