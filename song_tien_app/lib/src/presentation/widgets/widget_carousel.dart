
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:song_tien_app/src/configs/configs.dart';


class WidgetCarousel extends StatefulWidget {

 final List<String> listImG;

  const WidgetCarousel({Key key, this.listImG}) : super(key: key);
  @override
  _WidgetCarouselState createState() => _WidgetCarouselState();
}

class _WidgetCarouselState extends State<WidgetCarousel> {
  int _currentIndex = 0;
  // List cardList = [AppImages.slide1, AppImages.slide2, AppImages.slide3];
  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    var getScreenHeight = MediaQuery.of(context).size.height;
    // TODO: implement build
    return Stack(
      children: <Widget>[
        CarouselSlider(
          options: CarouselOptions(
            height: Get.height*0.3-20,
            autoPlay: true,
            autoPlayInterval: Duration(seconds: 3),
            autoPlayAnimationDuration: Duration(milliseconds: 800),
            autoPlayCurve: Curves.fastOutSlowIn,
            pauseAutoPlayOnTouch: true,
            aspectRatio: 2.0,
            viewportFraction: 1.0,
            onPageChanged: (index, reason) {
              setState(() {
                _currentIndex = index;
              });
            },
          ),
          items: widget.listImG.map((card) => Image.asset(
              card,
              fit: BoxFit.fill,
              width: Get.width,
            )).toList(),
        ),
        Positioned(
          bottom: 2,
         left: 0,
         right: 0,
         // left : MediaQuery.of(context).size.width*0.4,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.center,
            children: map<Widget>(widget.listImG, (index, url) {
              return AnimatedContainer(
                duration: Duration(milliseconds: 350),
                width: _currentIndex == index?14:10,
                height: 10.0,
                margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                decoration: BoxDecoration(
                  border:Border.all(width: 1,color: Colors.white) ,
                  // shape: BoxShape.circle,
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                  color: _currentIndex == index ? Colors.grey : Colors.white,
                ),
              );
            }),
          ),
        ),
      ],
    );
  }

  Widget _buildDot(int index) {
    bool isChange = _currentIndex == index;
    return AnimatedContainer(
      duration: Duration(milliseconds: 350),
      width: isChange ? 14 : 8,
      height: 8,
      margin: EdgeInsets.symmetric(vertical: 8, horizontal: 2),
      decoration: BoxDecoration(
        border: Border.all(
          width: 1,
          color: Colors.white,
        ),
        borderRadius: BorderRadius.all(Radius.circular(15)),
        color: isChange ? AppColors.primary : AppColors.white,
      ),
    );
  }

}
