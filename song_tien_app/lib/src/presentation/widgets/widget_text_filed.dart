import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:song_tien_app/src/configs/configs.dart';


typedef FormValidator<T> = String Function(T value);
typedef FormFieldSetter<T> = void Function(T newValue);

class WidgetTextField extends StatefulWidget  {
  String text;

  InputDecoration inputDecoration;
  FormValidator formFieldValidator;
  FormFieldSetter onSaved;
  ValueChanged<String> onChanged;
  String valueKey;
  String labelText;
  String hintText;
  Icon icon;
  Color borderColor;
  String prefixText;
  Function onTap;
  Widget prefixIcon;
  Widget suffixIcon;
  String suffixText;
  TextInputType textInputType;
  TextInputAction textInputAction;
  bool obsText;
  double mglr;
  double pdlr;
  String initialValue;
  double top;
  TextAlign textAlign;
  double radius;
  bool readOnly;
  EdgeInsetsGeometry contentPd;
  bool enabled;
  TextStyle hintStyle;
  int maxLines;
  double height;
  int minLines;
  TextStyle suffixStyle;
  TextStyle textStyle;
  List<TextInputFormatter> listTextInputFormatter;
  TextEditingController controller;
   TextStyle prefixStyle;
   Widget prefix;
  WidgetTextField(
      {Key key,
        this.maxLines = 1,
        this.hintStyle = AppStyles.TEXT_COLOR_LIGHT,
        this.suffixStyle,
        this.suffixText,
        this.prefixStyle= AppStyles.TEXT_COLOR_LIGHT,
        this.borderColor = Colors.white,
        this.prefixText,
        this.onChanged,
        this.listTextInputFormatter,
        // this.textInputFormatter,
        this.contentPd ,
        this.textAlign = TextAlign.start,
        this.minLines,
        this.readOnly = false,
        this.textInputAction,
        this.onTap,
        this.textStyle = AppStyles.TEXT_INPUT,
        this.text,
        this.inputDecoration,
        this.formFieldValidator,
        this.onSaved,
        this.height = 50,
        this.valueKey,
        this.labelText,
        this.hintText,
        this.icon,
        this.initialValue,
        this.prefixIcon,
        this.suffixIcon,
        this.textInputType,
        this.pdlr = 0,
        this.mglr = 30,
        this.top = 5,
        this.radius = 30.0,
        this.obsText = false,
        this.controller,
        this.enabled = true,
      this.prefix})
      : super(key: key);

  @override
  _WidgetTextFiledState createState() => _WidgetTextFiledState();
}

class _WidgetTextFiledState extends State<WidgetTextField> {

  // @override
  // void initState() {
  //   // TODO: implement initState
  //   super.initState();
  //   widget.onTap();
  // }



  @override
  Widget build(BuildContext context) {
    var maskFormatter = new MaskTextInputFormatter(mask: '+# (###) ###-##-##', filter: { "#": RegExp(r'[0-9]') });
    // TODO: implement build
    return Container(
      alignment: Alignment.center,
      // decoration: buildDecoration(),
      // width: double.infinity,
      margin: EdgeInsets.only(left: widget.mglr, right: widget.mglr, top: widget.top,bottom: widget.top),
      // padding: EdgeInsets.only(left: widget.pdlr,right: widget.pdlr),
      child: TextFormField(
          maxLines: widget.maxLines,
        inputFormatters: widget.listTextInputFormatter,
        textAlignVertical: TextAlignVertical.center,
        textAlign: widget.textAlign,
        onChanged: (value) => widget.onChanged(value),
        readOnly: widget.readOnly,
        textInputAction: widget.textInputAction,
        onTap: ()  => widget.onTap == null?AppShare():widget.onTap(),
        controller: widget.controller,
        style: TextStyle(fontFamily: "",color: Colors.black,fontSize: 14,),
        key: ValueKey(widget.valueKey),
        autocorrect: false,
        textCapitalization: TextCapitalization.none,
        enableSuggestions: false,
        validator: widget.formFieldValidator,
        keyboardType: widget.textInputType,
        initialValue:widget.initialValue,
        onSaved: widget.onSaved,
        minLines: widget.minLines,
        decoration: new InputDecoration(
          // isCollapsed: true,\
            prefixText: widget.prefixText,
            hoverColor: Colors.white,
            focusColor:Colors.white ,
            suffixIcon: widget.suffixIcon,
            contentPadding: widget.contentPd,
            prefixIcon:  widget.prefixIcon,
            prefixStyle: widget.prefixStyle,
            labelText: widget.labelText,
            hintText:widget.hintText,
            labelStyle: widget.textStyle,
            suffixText: widget.suffixText,
            suffixStyle: widget.suffixStyle,
            focusedBorder: new OutlineInputBorder(
              borderSide: new BorderSide(color: widget.borderColor),
              borderRadius:  BorderRadius.all(
                Radius.circular(widget.radius,),
              ),
            ),
            enabledBorder: new OutlineInputBorder(
              borderSide: new BorderSide(color: widget.borderColor),
              borderRadius:  BorderRadius.all(
                Radius.circular(widget.radius),
              ),
            ),
            border: new OutlineInputBorder(
              borderSide: new BorderSide(color: widget.borderColor),
              borderRadius:  BorderRadius.all(
                Radius.circular(widget.radius),
              ),
            ),
            enabled: widget.enabled,

            filled: true,
            hintStyle: widget.hintStyle,
            fillColor: Colors.white,
          prefix: widget.prefix
        ),
        // enabled: false,
        // readOnly: true,
        obscureText: widget.obsText,
      ),
    );
  }

  Decoration buildDecoration([Color color = Colors.white]) {
    return BoxDecoration(
        color: color,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.0),
            spreadRadius: 2,
            blurRadius: 3,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
        borderRadius: BorderRadius.circular(5));
  }
 void
 AppShare(){}
}
class UpperCaseTextFormatter implements TextInputFormatter {

  const UpperCaseTextFormatter();

  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    return TextEditingValue(text: newValue.text?.toUpperCase(), selection: newValue.selection);
  }

}

class SpecialMaskTextInputFormatter extends MaskTextInputFormatter {

  static String maskA = "S.####";
  static String maskB = "S.######";

  SpecialMaskTextInputFormatter({
    String initialText
  }): super(
      mask: maskA,
      filter: {"#": RegExp(r'[0-9]'), "S": RegExp(r'[AB]')},
      initialText: initialText
  );

  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.text.startsWith("A")) {
      if (getMask() != maskA) {
        updateMask(mask: maskA);
      }
    } else {
      if (getMask() != maskB) {
        updateMask(mask: maskB);
      }
    }
    return super.formatEditUpdate(oldValue, newValue);
  }

}