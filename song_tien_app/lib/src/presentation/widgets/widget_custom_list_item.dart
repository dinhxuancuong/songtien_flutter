import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:song_tien_app/src/configs/configs.dart';
import 'package:song_tien_app/src/presentation/presentation.dart';
import 'package:song_tien_app/src/presentation/widgets/widget_read_more_text.dart';
import 'package:song_tien_app/src/resource/model/app_valid.dart';
import 'package:song_tien_app/src/resource/model/model.dart';

class WidgetCustomListItem extends StatelessWidget {
  final Widget thumbnail;
  final Color color;
  final Widget child;

  WidgetCustomListItem({
    this.child,
    this.thumbnail,
    this.color,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: color,
      padding: const EdgeInsets.symmetric(vertical: 5.0),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: thumbnail,
              ),
              Expanded(
                flex: 2,
                child: child,
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class Description extends StatelessWidget {
  Description({
    Key key,
    this.title,
    this.subTiTle,
    this.dateTime,
  }) : super(key: key);

  final String title;
  final String subTiTle;
  final DateTime dateTime;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(5.0, 0.0, 0.0, 0.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(title, style: AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(),),
          const Padding(padding: EdgeInsets.symmetric(vertical: 2.0)),
          Container(height:35,width: double.infinity,child: Text(subTiTle,textAlign: TextAlign.start,maxLines: 2,  overflow: TextOverflow.ellipsis,)),
          const Padding(padding: EdgeInsets.symmetric(vertical: 3.0)),
          Text("${DateFormat("dd/MM/yyyy HH:mm").format(dateTime)}"),
        ],
      ),
    );
  }

  BoxDecoration myBoxDecoration() {
    return BoxDecoration(
      border: Border.all(color: Colors.grey),
    );
  }
}

class Programme extends StatelessWidget {

  final Address address;

  const Programme({Key key, this.address}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(5.0, 0.0, 0.0, 0.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(right: 15),
            width: Get.width,
            child: Text(
              address.title,
              style: AppStyles.DEFAULT_MEDIUM_BOLD
                  .copyWith(color: Color(0xff5A7D48,)),
              textAlign: TextAlign.start,maxLines: 2,overflow: TextOverflow.ellipsis,
            ),
          ),
          SizedBox(height: 5,),
          WidgetStart(
            rating: 5,
          ),
          SizedBox(height: 8,),
          RichText(
              text: TextSpan(
                  style:AppStyles.DEFAULT_RICH_TEXT.copyWith(fontSize: 15),
                  children: [
                TextSpan(text: "Mã số chương trình: "),
                TextSpan(text: address.code)
              ])),
          SizedBox(height: 6,),
          RichText(
              text: TextSpan(
                  style:AppStyles.DEFAULT_RICH_TEXT.copyWith(fontSize: 15),
                  children: [
                TextSpan(text: "Loại: "),
                TextSpan(text: "${address.category}")
              ])),
          SizedBox(height: 8,),
        ],
      ),
    );
  }

  BoxDecoration myBoxDecoration() {
    return BoxDecoration(
      border: Border.all(color: Colors.grey),
    );
  }
}
