import 'package:flutter/material.dart';


// typedef FormValidator<T> = String Function(T value);
// typedef FormFieldSetter<T> = void Function(T newValue);

class WidgetTextInput extends StatefulWidget {

  double horizontal;
  InputDecoration inputDecoration;
  // FormValidator formFieldValidator;
  FormFieldSetter onSaved;
  String valueKey;
  String labelText;
  String hintText;
  TextStyle helperStyle;
  TextStyle hintStyle;
  Icon icon;
  EdgeInsetsGeometry contentPd;
  String prefixText;
  VoidCallback onTap;
  Widget prefixIcon;
  Widget suffixIcon;
  String suffixText;
  TextStyle suffixStyle;
  TextInputType textInputType;
  TextInputAction textInputAction;
  bool obsText;
  double left;
  double height;
  double right;
  double top;
  double radius;
  TextStyle textStyle;
  bool readOnly;
  bool enabled;

  TextEditingController controller;
  WidgetTextInput(
      {Key key,
        this.hintStyle,
        this.contentPd,
        this.prefixText,
        this.readOnly = false,
        this.textInputAction,
        this.onTap,
        this.horizontal,
        this.inputDecoration,
        this.textStyle,
        this.hintText,
        this.helperStyle,
        this.height,
        this.onSaved,
        this.valueKey,
        this.labelText,
        this.icon,
        this.prefixIcon,
        this.suffixText,
        this.suffixStyle,
        this.suffixIcon,
        this.textInputType,
        this.left = 30,
        this.right = 30,
        this.top = 10,
        this.radius = 30.0,
        this.obsText = false,
        this.controller,
        this.enabled = true})
      : super(key: key);

  @override
  _WidgetTextInputState createState() => _WidgetTextInputState();
}

class _WidgetTextInputState extends State<WidgetTextInput> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      height: widget.height,
      // width: double.infinity,
      margin: EdgeInsets.only(left: widget.left, right: widget.right, top: widget.top),
      child: TextFormField(
        readOnly: widget.readOnly,
        textInputAction: widget.textInputAction,
        onTap: ()  =>  widget.onTap == null?AppShare():widget.onTap(),
        controller: widget.controller,
        style: TextStyle(fontFamily: "",color: Colors.black,fontSize: 12),
        key: ValueKey(widget.valueKey),
        autocorrect: false,
        textCapitalization: TextCapitalization.none,
        enableSuggestions: false,
        // validator: widget.formFieldValidator,
        keyboardType: widget.textInputType,
        onSaved: widget.onSaved,
        decoration: new InputDecoration(
            hoverColor: Colors.white,
            focusColor:Colors.white ,
            suffixIcon: widget.suffixIcon,
            contentPadding: widget.contentPd,
            prefixIcon:  widget.prefixIcon,
            prefixText: widget.prefixText,
            labelText: widget.labelText,
            hintText:widget.hintText,
            suffixText:widget.suffixText ,
            suffixStyle: widget.suffixStyle,
            helperStyle: widget.helperStyle,
            labelStyle: widget.textStyle,
            focusedBorder: new OutlineInputBorder(
              borderSide: new BorderSide(color: Colors.white),
              borderRadius:  BorderRadius.all(
                Radius.circular(widget.radius),
              ),
            ),
            enabledBorder: new OutlineInputBorder(
              borderSide: new BorderSide(color: Colors.white),
              borderRadius:  BorderRadius.all(
                Radius.circular(widget.radius),
              ),
            ),
            border: new OutlineInputBorder(
              borderSide: new BorderSide(color: Colors.white),
              borderRadius:  BorderRadius.all(
                Radius.circular(widget.radius),
              ),
            ),

            enabled: widget.enabled,
            filled: true,
            hintStyle: widget.hintStyle,
            fillColor: Colors.white),
        // enabled: false,
        // readOnly: true,
        obscureText: widget.obsText,
      ),
    );
  }
  void
  AppShare(){}
}
