import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:song_tien_app/src/resource/model/future.dart';

class WidgetFurnitureItem extends StatefulWidget {
  final Furniture furniture;

  WidgetFurnitureItem({this.furniture});

  @override
  _FurnitureItemState createState() => _FurnitureItemState();
}

class _FurnitureItemState extends State<WidgetFurnitureItem> {
  // final List<Furniture> furinitures = <Furniture>[];
   List<Furniture> listFurniture = [];
  final Set<Furniture> _saved = new Set<Furniture>();
  int result = 1;


  @override
  Widget build(BuildContext context) {
    // final provider = Provider.of<BooKingViewModel>(context, listen: false);
    List listfUTURE (int id,bool result){
      if(widget.furniture.Id == id){
        if (result) {
          listFurniture.remove(widget.furniture);
        } else {
          listFurniture.add(widget.furniture);
        }
        return listFurniture;
      }


    }
    Color color;
    _check() {
      switch (widget.furniture.status) {
        case TableStatus.EMPTY:
          color = Colors.tealAccent;
          break;
        case TableStatus.BOOKED:
          color = Colors.yellow;
          break;
        case TableStatus.CAME:
          color = Colors.red;
          break;
        case TableStatus.SAME:
          color = Colors.tealAccent;
          break;
        default:
          color = Colors.cyan;
          break;
      }
      return color;
    }

    List<Align> _stacks() {
      bool _already = listFurniture.contains(widget.furniture);
      Color color;
      final list = <Align>[];
      list.add(
        Align(
            alignment: Alignment(0,0),
            child: GestureDetector(
              onTap: () {
                setState(() {
                  int id = widget.furniture.Id;
                  listfUTURE(id, _already);
                  // provider.listFurnitureClone.addAll(provider.listFurniture);
                }
                );},
              child: Stack(
                children: [
                  Container(
                    width: 80,
                    height: 100,
                    decoration: BoxDecoration(
                        color: _already?Colors.white:null,
                      // color: Colors.pink,
                        borderRadius: BorderRadius.circular(12)
                    ),

                  ),
                  Positioned(
                    height: 25,
                    top: 17,
                    left: 5,
                    child: SizedBox(
                      height: 25,
                      width: 55,
                      child: Container(
                        decoration: BoxDecoration(
                          color: _check(),
                          shape: BoxShape.circle,
                        ),
                        child: Center(
                          child: Text(
                            "",
                            // '${widget.furniture.booked}/${widget.furniture.chairsSlot}',
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),

                    ),
                  ),
                ],
              ),
            )),
      );
      for (var i = 0; i < widget.furniture.chairsSlot; i++) {
        if (i <= widget.furniture.booked) {
          color = Colors.yellow;
        } else if (i == 5 && widget.furniture.booked == 15) {
          color = Colors.cyan;
        } else {
          color = _check();
        }
        var degree = 360 / widget.furniture.chairsSlot * i + 45;

        var radian = degree * (pi / 180);
        list.add(
          Align(
            alignment: Alignment(sin(radian), cos(radian)),
            child: Transform.rotate(
              angle: -radian,
              child: Image.asset(
                'assets/images/chair.png',
                height: 12,
                color: color,
                alignment: Alignment.center,
              ),
            ),
          ),
        );
      }
      return list;
    }

    return SizedBox(
      height: 85,
      width: 90,
      child: Container(
        padding: EdgeInsets.all(5),
        child: InkWell(
          onTap: () {},
          child: Stack(
            children: _stacks(),
          ),
        ),
      ),
    );
  }
}
