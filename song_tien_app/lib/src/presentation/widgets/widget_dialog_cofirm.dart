import 'package:flutter/material.dart';
import 'package:song_tien_app/src/configs/configs.dart';

class WidgetDialogConfirm extends StatelessWidget {
  final String keyTitle;
  final String content;
  final bool transTitle;
  final bool transContent;
  final Function actionCancel;
  final Function actionConfirm;

   WidgetDialogConfirm(
      {this.keyTitle,
        this.transTitle = true,
        @required this.content,
        this.transContent = false,
        this.actionCancel,
        this.actionConfirm});

  @override
  Widget build(BuildContext context) {
    return Dialog(
        elevation: 8,
        shape: RoundedRectangleBorder(
          side: BorderSide.none,
          borderRadius: BorderRadius.circular(16),
        ),
        backgroundColor: HexColor.fromHex("#fdfefe"),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            const SizedBox(
              height: 10,
            ),
            Text(
              ";â;a;",
              // keyTitle == null
              //     ? AppLocalizations.of(context).translate("confirm")
              //     : transTitle
              //     ? AppLocalizations.of(context).translate(keyTitle)
              //     : keyTitle,
              style: AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(fontSize: 16),
              textAlign: TextAlign.center,
            ),
             SizedBox(
              height: 4,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Text(
                transContent
                    ? AppLocalizations.of(context).translate(content)
                    : content,
                style: AppStyles.DEFAULT_MEDIUM,
                textAlign: TextAlign.center,
              ),
            ),
            const SizedBox(
              height: 12,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                    child: InkWell(
                      borderRadius:
                      BorderRadius.only(bottomLeft: Radius.circular(16)),
                      onTap: () => Navigator.pop(context, false),
                      child: Container(
                        decoration: BoxDecoration(
                          color: AppColors.primary,
                          borderRadius:
                          BorderRadius.only(bottomLeft: Radius.circular(16)),
                        ),
                        height: 45,
                        child: Center(
                          child: Text(
                             "kssksk",
                            style: AppStyles.DEFAULT_MEDIUM_BOLD
                                .copyWith(color: Colors.white),
                          ),
                        ),
                      ),
                    )),
                const SizedBox(
                  width: 1,
                ),
                Expanded(
                    child: InkWell(
                      borderRadius:
                      BorderRadius.only(bottomLeft: Radius.circular(16)),
                      onTap: actionConfirm ?? () => Navigator.pop(context, true),
                      child: Container(
                        decoration: BoxDecoration(
                            color: AppColors.primary,
                            borderRadius: BorderRadius.only(
                                bottomRight: Radius.circular(16))),
                        height: 45,
                        child: Center(
                          child: Text(
                            "Name",
                            style: AppStyles.TEXT_INPUT,
                          ),
                        ),
                      ),
                    )),
              ],
            )
          ],
        ));
  }
}