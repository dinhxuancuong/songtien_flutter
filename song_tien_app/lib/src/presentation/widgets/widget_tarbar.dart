import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:song_tien_app/src/configs/configs.dart';

import '../presentation.dart';

class WidgetTarbar extends StatefulWidget {
  final Widget child;
  final Decoration decoration;
  final List<Widget> tabs;
  final ValueChanged<int> onTap;
  WidgetTarbar({Key key, this.decoration, this.tabs, this.child, this.onTap})
      : super(key: key);

  @override
  _WidgetTarbarState createState() => _WidgetTarbarState();
}

class _WidgetTarbarState extends State<WidgetTarbar> with SingleTickerProviderStateMixin{
  TabController _tabController;
  int currentTab = 0;
  @override
  void initState() {
    _tabController = TabController(length: 3, vsync: this);
    // _tabController.animation..addListener(() => _viewModel.swipingTab(_tabController));
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    AppSizeConfig().init(context);
    // TODO: implement build
    return DefaultTabController(
        initialIndex: 1,
        length: widget.tabs.length,
        child: Container(
            width: double.infinity,
            decoration: widget.decoration,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      padding: EdgeInsets.symmetric(horizontal: 2, vertical: 0),
                      decoration: BoxDecoration(
                          color: Colors.white.withOpacity(0.0),
                          border: Border(
                              bottom:
                                  BorderSide(color: Colors.grey, width: 0.8))),
                      child: TabBar(
                        controller: _tabController,
                        labelStyle: AppStyles.DEFAULT_MEDIUM,
                        indicator: UnderlineTabIndicator(
                          borderSide: BorderSide(
                            width: 2,
                            color: AppColors.red,
                          ),
                          insets: EdgeInsets.symmetric(horizontal: 40),
                        ),
                        isScrollable: false,
                        physics: NeverScrollableScrollPhysics(),
                        onTap: (value) => widget.onTap(value),
                        labelPadding: EdgeInsets.only(top: 15),
                        tabs: widget.tabs,
                      ),
                    ),
                  Expanded(
                      child: Container(
                    margin: EdgeInsets.only(top: 10),
                    child: widget.child,
                  ))
                ])));
  }
  // swipingTab(TabController controller) {
  //   currentTab = (controller.animation.value).round();
  //   notifyListeners();
  // }
  AppShared() {}
}
