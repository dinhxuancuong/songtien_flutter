import 'package:flutter/material.dart';

class WidgetButton extends StatefulWidget {
   String text;
   Color color;
   double circular;
   double height;
   double width;
   Color colorSide;
   Color colorsText;
   VoidCallback onPress;
  double horizontal;
  double fontSize;
  WidgetButton(
      {this.text,
        this.colorsText = Colors.white,
      this.color = Colors.red,
      this.circular = 25.0,
      this.height = 50,
      this.width = 130,
      this.onPress,
      this.horizontal = 22,
      this.fontSize = 15,
      this.colorSide = Colors.white});
  @override
  _WidgetButtonState createState() => _WidgetButtonState();
}

class _WidgetButtonState extends State<WidgetButton> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      padding: EdgeInsets.symmetric(horizontal: widget.horizontal),
      child: ButtonTheme(
            height: widget.height,
            minWidth: widget.width,
            buttonColor: widget.color,
            child: FlatButton(
                color: widget.color,
                padding: EdgeInsets.symmetric(horizontal: 0),
                shape: RoundedRectangleBorder(
                  side:
                      new BorderSide(color: widget.colorSide), //the outline color
                  borderRadius: BorderRadius.circular(widget.circular),
                ),
                child: Text(
                  widget.text,
                  style:
                      TextStyle(fontSize: widget.fontSize, fontFamily: "Roboto",color:widget.colorsText ),
                ),
                textColor: Colors.white,
                onPressed: () => widget.onPress()
                // dynamic result = await widget.auth.signInAnon();

                )),
    );
  }
}
