import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import '../../configs/configs.dart';

import '../presentation.dart';

class SplashScreen extends StatelessWidget {

  SplashViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<SplashViewModel>(
        viewModel: SplashViewModel(repository: Provider.of(context)),
        onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
        builder: (context, viewModel, child) {
          return Scaffold(
            backgroundColor: Color(0xffADBFA3),
            body: _buildBody(context),
          );
        });
  }

  Widget _buildBodyMobile(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Center(
        child: Text(
          AppLocalizations.of(context).translate("app_name"),
          style: GoogleFonts.nobile(textStyle: AppStyles.DEFAULT_LARGE_BOLD),
        ),
      ),
    );
  }

  Widget _buildBody(BuildContext context) {
    return Center(
      child: InkWell(
          child: WidgetLogo()));
  }
}
