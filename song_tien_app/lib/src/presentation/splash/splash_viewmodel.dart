import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:song_tien_app/src/resource/model/app_valid.dart';
import '../../resource/resource.dart';
import '../base/base.dart';
import '../presentation.dart';
class SplashViewModel extends BaseViewModel{

  final OtherRepository repository;

  SplashViewModel({@required this.repository});

  init()async{
    configLoading();

    // EasyLoading.show(status: 'loading...');
    await Future.delayed(Duration(seconds: 3),(){
      Navigator.pushNamed(context, Routers.Login);

    });
  }

  configLoading(){
    return    EasyLoading.instance
      ..displayDuration = const Duration(milliseconds: 2000)
      ..indicatorType = EasyLoadingIndicatorType.fadingCircle
      ..loadingStyle = EasyLoadingStyle.dark
      ..indicatorSize = 45.0
      ..radius = 10.0
      ..progressColor = Colors.yellow
      ..backgroundColor = Colors.green
      ..indicatorColor = Colors.yellow
      ..textColor = Colors.yellow
      ..maskColor = Colors.blue.withOpacity(0.5)
      ..userInteractions = true
      ..dismissOnTap = false
      ..customAnimation = CustomAnimation();
  }
}

class CustomAnimation extends EasyLoadingAnimation {
  CustomAnimation();

  @override
  Widget buildWidget(
      Widget child,
      AnimationController controller,
      AlignmentGeometry alignment,
      ) {
    return Opacity(
      opacity: controller.value,
      child: RotationTransition(
        turns: controller,
        child: Text("mssssssssssss"),
      ),
    );
  }
}