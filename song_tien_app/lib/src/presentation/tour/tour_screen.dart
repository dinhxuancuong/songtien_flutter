import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:song_tien_app/src/presentation/tour/tour.dart';
import 'package:song_tien_app/src/resource/model/model.dart';
import '../../configs/configs.dart';
import '../presentation.dart';



class TourScreen extends StatefulWidget {

  @override
  _TourScreenState createState() => _TourScreenState();
}

class _TourScreenState extends State<TourScreen> with ResponsiveWidget {
  DeviceScreenType type;
  final keyRefresh = GlobalKey<RefreshIndicatorState>();
  List<Person> data = [];
  var _scrollController = ScrollController();
  var _iVisible = true;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadList();
  }
  TourViewModel _viewModel;
 @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    AppSizeConfig().init(context);
    return BaseWidget<TourViewModel>(
        viewModel: TourViewModel(repository: Provider.of(context)),
        onViewModelReady: (viewModel)  => _viewModel = viewModel..init(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: buildUi(context: context),
          );
        });
  }

  Future loadList() async {
    keyRefresh.currentState?.show();
    await Future.delayed(Duration(milliseconds: 1000));
    if (this.mounted) { // check whether the state object is in tree
      setState(() {
        this.data = listData;
      });
    }
  }

  Widget _buildBody(BuildContext context) {
    return SafeArea(
      child: Column(
        children: [
          SizedBox(
            height:80,
            child: WidgetAppBar(
              isBack: false,
              keyTitle: "Chương Trình Tham Quan",
              trans: false,),
          ),
          Expanded(child: buildRefresh())
        ],
      )
    );
  }


  buildRefresh() => data.isEmpty
      ? Center(child: CircularProgressIndicator(backgroundColor: Colors.red,strokeWidth: 2,)):
     WidgetRefresh(
       keyRefresh: keyRefresh,
       onRefresh: loadList,
      child: ListView.builder(
        controller: _scrollController,
        primary: false,
        physics: ScrollPhysics(),
        padding: EdgeInsets.only(top: 5),
        itemCount: listData.length,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          Address address = listAdress[index];
          return InkWell(
            onTap: () {
              Navigator.pushNamed(context, Routers.DetailsTour,arguments: address);
            },
            child: Padding(
              padding: const EdgeInsets.only(top: 2, left: 5, right: 5),
              child: Column(
                children: [
                  _buildList(address),
                  Divider(
                    height: 13,
                    color: Colors.grey.withOpacity(0.2),
                    thickness: 10,
                  ),
                  SizedBox(height: 15,)
                ],
              ),
            ),
          );
        },
      ),
    );


  Widget buildList() => data.isEmpty
      ? Center(child: CircularProgressIndicator())
      : WidgetRefresh(
      keyRefresh: keyRefresh,
      onRefresh: loadList,
      child: ListView.builder(
          controller: _scrollController,
          primary: false,
          padding: const EdgeInsets.only(left: 12, right: 12, top: 20),
          physics: ScrollPhysics(),
          scrollDirection: Axis.vertical,
          itemCount: listAdress.length,
          shrinkWrap: true,
          itemBuilder: (context, index) =>
              _buildList(listAdress[index])));

  
  
  _buildList(Address address){
    return WidgetCustomListItem(
      thumbnail: ClipRRect(
          borderRadius: BorderRadius.circular(5),
          child: Image.asset(
            address.images,
            fit: BoxFit.fill,
            height: Get.height*0.2,
            width: Get.width,
          )),
      child: Programme(
        address: address,
      ),
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    type = DeviceScreenType.desktop;
    // TODO: implement buildDesktop
   return _buildBody(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    type = DeviceScreenType.mobile;
    // TODO: implement buildMobile
    return _buildBody(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    type = DeviceScreenType.tablet;
    // TODO: implement buildMobile
    return _buildBody(context);
  }

}
