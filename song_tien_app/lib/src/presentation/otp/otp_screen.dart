import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pinput/pin_put/pin_put.dart';
import 'package:song_tien_app/src/presentation/otp/otp.dart';
import 'package:song_tien_app/src/presentation/register/register.dart';
import 'package:song_tien_app/src/presentation/widgets/widget_button_gradientAni.dart';
import '../../configs/configs.dart';
import '../presentation.dart';

class OtpScreen extends StatefulWidget {
  final VerifyArguments verifyArguments;

  OtpScreen({Key key, this.verifyArguments}) : super(key: key);

  @override
  _OtpScreenScreenState createState() => _OtpScreenScreenState();
}

class _OtpScreenScreenState extends State<OtpScreen> {
  Timer _timer;
  int start = 60;




  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  OtpViewModel _viewModel;
  final FocusNode _pinPutFocusNode = FocusNode();
  @override
  Widget build(BuildContext context) {
    return BaseWidget<OtpViewModel>(
      viewModel: OtpViewModel(
        verifyArguments: widget.verifyArguments,
      ),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          body: SingleChildScrollView(
            child: Column(
              children: [
                _buildHeader(),
                _buildBody(),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildHeader() {
    return Column(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(60),
          ),
          child: Image.asset(
            AppImages.imgIsLand3,
            width: Get.width,
            height: 210,
            fit: BoxFit.fill,
          ),
        ),
        const SizedBox(height: 10),
        WidgetLogo(),
      ],
    );
  }
  void startTimer(){
    const oneSec = const Duration(seconds: 1);
    _timer =  new Timer.periodic(
      oneSec,
          (Timer timer) {
        if (start == 0) {
          setState(() {
            _viewModel.resendCheck();
            timer.cancel();
            start = 60;
          });
        } else {
          // _viewModel.sendPhoneNumberToFirebase();
          setState(() {
            start--;
          });

        }
      },
    );
  }
  Widget _buildBody() {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: GestureDetector(
        onTap: _viewModel.unFocus,
        child: StreamBuilder<bool>(
          stream: _viewModel.loadingSubject,
          builder: (context, snapshot) {
            bool loading = snapshot.data ?? false;
            return Container(
              width: Get.width,
              padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
              color: Colors.transparent,
              child: _buildEnterOTP(loading),
            );
          },
        ),
      ),
    );
  }

  Widget _buildEnterOTP(bool loading) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          width: Get.width,
          height: 85,
          alignment: Alignment.center,
          padding: EdgeInsets.only(top: 5),
          child: Text(
            AppLocalizations.of(context).translate("otp").toUpperCase(),
            style: AppStyles.DEFAULT_LARGE.copyWith(
              color: AppColors.primary,
              fontSize: 24,
            ),
          ),
        ),
        Text(
          AppLocalizations.of(context).translate("otp_request"),
          style: AppStyles.DEFAULT_MEDIUM.copyWith(
            color: AppColors.black,
            fontSize: 16,
          ),
          textAlign: TextAlign.center,
        ),
        SizedBox(height: 15),
        _buildPin(),
        SizedBox(height: 15),
        WidgetButtonGradientAni(
          width: Get.width - 15,
          title: "continue",
          colorStart: AppColors.primary,
          colorEnd: AppColors.primary,
          action: () {
            _viewModel.signInPhoneNumber(_viewModel.pinController.text,);
          },
          loading: loading,
          textStyle: AppStyles.DEFAULT_MEDIUM.copyWith(
            color: AppColors.white,
          ),
        ),
        SizedBox(height: 25),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GestureDetector(
                onTap: () {
                  if(start > 1 && start != 60) {
                  print("cosssssssss");
                  }else{
                    _viewModel.sendPhoneNumberToFirebase();
                    startTimer();
                  }
                  },
                child: Text(
                  AppLocalizations.of(context).translate("resend"),
                  style: AppStyles.DEFAULT_MEDIUM.copyWith(color: Colors.black),
                )),
            _viewModel.isCheck?Text("(${start})"):Container()
          ],
        )
      ],
    );
  }


  Widget _buildPin() {
    final BoxDecoration pinPutDecoration = BoxDecoration(
      border: Border.all(color: AppColors.primaryLight),
      borderRadius: BorderRadius.circular(5.0),
    );

    return PinPut(
      fieldsCount: 6,
      eachFieldHeight: 45,
      focusNode: _pinPutFocusNode,
      withCursor: true,
      controller: _viewModel.pinController,
      // onSubmit: _viewModel.signInPhoneNumber,
      submittedFieldDecoration: pinPutDecoration,
      selectedFieldDecoration: pinPutDecoration,
      followingFieldDecoration: pinPutDecoration,
      pinAnimationType: PinAnimationType.fade,
    );
  }
}
