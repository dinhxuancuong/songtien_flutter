import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:song_tien_app/src/presentation/presentation.dart';
import 'package:song_tien_app/src/presentation/register/register.dart';
import 'package:toast/toast.dart';
import '../../resource/resource.dart';
import '../base/base.dart';

class OtpViewModel extends BaseViewModel {
  TextEditingController pinController = TextEditingController();

  String verificationID;
  bool isCheck = false;
   bool selectOTP = false;
  Timer _timer;
  int start = 60;

  final  VerifyArguments verifyArguments;
  FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  OtpViewModel({@required this.verifyArguments});

  init() {
    print("init");
    // verifyPhoneNumber();
  }

   checkOtp(){
     selectOTP = !selectOTP;
    notifyListeners();
  }

  resendCheck()async{
     isCheck = !isCheck;
  }


  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
          (Timer timer) {
        if (start == 0) {
          // setState(() {
          //   timer.cancel();
          // });
          notifyListeners();
          _timer.cancel();
        } else {
          notifyListeners();
            start--;
        }
      },
    );
  }

  // @override
  // void dispose() {
  //   _timer.cancel();
  //   super.dispose();
  // }


  Future<void> signInPhoneNumber(String pin,) async {
    try {
      setLoading(false);
      await firebaseAuth
          .signInWithCredential(
        PhoneAuthProvider.credential(
          verificationId: selectOTP?verificationID:verifyArguments.verificationId,
          smsCode: pin,
        ),
      )
          .then((value) async {
        if (value.user != null) {
          setLoading(false);
          EasyLoading.show(status: 'loading...');
          await Future.delayed(Duration(milliseconds: 500), () {
            Toast.show("Chào mừng ${value.user.phoneNumber}", context);
            EasyLoading.dismiss();
          });
          await handleFailure(msg: "register_success");
          Navigator.pushNamedAndRemoveUntil(context, Routers.Navigation,(router) => false,
              arguments: value.user);
        } else {
          setLoading(false);
        }
      });
    } catch (e) {
      pinController.clear();
      unFocus();
      setLoading(false);
      await handleFailure(msg: "invalid_otp");
    }
  }


  String convertPhoneNumber(String phoneNumber) {
    if (phoneNumber.startsWith("+84")) return phoneNumber;
    var result = phoneNumber.replaceFirst("0", "+84");
    return result;
  }

  Future<dynamic> showNotification(
      {@required String keyTitle,
      bool trans = true,
      String keyAction,
      Function action}) async {
    await showDialog(
      context: context,
      builder: (context) => DialogError(
        keyTitle: keyTitle,
        action: action,
        trans: trans,
        keyAction: keyAction,
      ),
    );
  }

  get verificationCompleted => (AuthCredential authCredential) async {
        firebaseAuth
            .signInWithCredential(authCredential)
            .then((authResult) async {
          final user = authResult?.user;
          if (user == null) {
            handleFailure(msg: 'invalid_otp');
            setLoading(false);
          } else
            print("login");
          // login(user);
        }).catchError((Object error) {
          handleFailure(msg: "invalid_otp");
          setLoading(false);
        });
      };

// sự kiện khi quá trình xác thực lỗi
// thông báo lỗi ra app
  get verificationFailed => (FirebaseAuthException exception) async {
        handleFailure(msg: exception.message);
        print(exception.message);
        setLoading(false);
      };

  get codeSent => (String verificationId, [int forceResendingToken]) async {
        // verify code success
       verificationID = verificationId;
        setLoading(false);
      };

// sự kiện tự động timeout khi nhận code otp
  get codeAutoRetrievalTimeout => (String verificationId) async {
        setLoading(false);
        print('codeAutoRetrievalTimeout $verificationId');
      };


  // khi bấm vào nút login số điện thoại sẽ gọi hàm này để thực thi tất cả

  void sendPhoneNumberToFirebase() {

    firebaseAuth.verifyPhoneNumber(
        phoneNumber: convertPhoneNumber(verifyArguments.phoneNumber),
        verificationCompleted: verificationCompleted,
        verificationFailed: verificationFailed,
        codeAutoRetrievalTimeout: codeAutoRetrievalTimeout,
        codeSent: codeSent
    );
    checkOtp();
    resendCheck();
    // startTimer();
  }


  Future handleFailure({String msg}) async {
    await showNotification(
      keyTitle: msg ?? "login_failure",
      trans: msg != null,
    );
  }



}

