import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:song_tien_app/src/resource/model/app_valid.dart';
import 'package:song_tien_app/src/resource/model/model.dart';
import '../../configs/configs.dart';
import '../presentation.dart';
import 'details_tour.dart';

class DetailsTourScreen extends StatefulWidget {
  final Address address;

  DetailsTourScreen({Key key, this.address}) : super(key: key);

  @override
  _DetailsTourScreenState createState() => _DetailsTourScreenState();
}

class _DetailsTourScreenState extends State<DetailsTourScreen>
    with SingleTickerProviderStateMixin, ResponsiveWidget {
  DeviceScreenType type;
  ScrollController _scrollController;
  TabController _tabController;
  int currentIndex = 0;
  DetailsTourViewModel _viewModel;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _scrollController = ScrollController();
    _tabController = TabController(length: tabs.length, vsync: this);
    _tabController.addListener(smoothScrollToTop());
  }

  smoothScrollToTop() {
    if (_scrollController.hasClients)
      _scrollController.animateTo(
        0,
        duration: Duration(microseconds: 300),
        curve: Curves.ease,
      );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _tabController.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    AppSizeConfig().init(context);
    return BaseWidget<DetailsTourViewModel>(
        viewModel: DetailsTourViewModel(repository: Provider.of(context)),
        onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
        builder: (context, viewModel, child) {
          return Scaffold(
              backgroundColor: Color(0xffE8E8E8),
              body: SafeArea(child: buildUi(context: context),));
        });
  }

  _buildNestedScroll(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: NestedScrollView(
        controller: _scrollController,
        physics: NeverScrollableScrollPhysics(),
        headerSliverBuilder: (context, isSc) {
          return [
            SliverList(
              delegate: new SliverChildBuilderDelegate(
                (context, innerBoxIsScrolled) {
                  return _buildBody(context);
                },
                childCount: 1,
              ),
            ),
          ];
        },
        body: buildTabBar(),
      ),
    );
  }

  buildHeader() {
    return Container(
      height: type == DeviceScreenType.mobile?210:255,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: ExactAssetImage(
                AppImages.imgSongTien2,
              ),
              fit: BoxFit.fill)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [

          IconButton(
              icon: Icon(
                Icons.arrow_back_ios_outlined,
                color: Colors.white,
                size: 18,
              ),
              onPressed: () => Navigator.pop(context)),
          IconButton(
              icon: Icon(Icons.add, color: Colors.white),
              onPressed: () => Navigator.pushNamed(context, Routers.Trip,
                  arguments: widget.address))
        ],
      ),
    );
  }

  Widget _buildBody(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Color(0xffE8E8E8),
      ),
      child: Padding(
        padding: const EdgeInsets.only(bottom: 5),
        child: Stack(
          children: [
            buildHeader(),
            Padding(
              padding: const EdgeInsets.only(top: 160),
              child: Column(
                children: [
                  buildTitle(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  ///
  /// Tiều đề  Tham quan
  buildTitle() {
    var size = MediaQuery.of(context).size;
    return Container(
      height: AppSizeConfig.screenHeight * 0.2,
      width: double.infinity,
      margin: EdgeInsets.only(left: 10, right: 10, bottom: 8),
      decoration: buildDecoration(),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
              width: AppSizeConfig.screenWidth * 0.9,
              child: Text(
                widget.address.title,
                style: AppStyles.DEFAULT_LARGE_BOLD
                    .copyWith(color: Color(0xff5A7D48), fontSize: 17,fontFamily: "Sunshiney"),
                textAlign: TextAlign.center,
              )),
          SizedBox(
            height: 5,
          ),
          WidgetStart(
            rating: widget.address.raitting,
          )
        ],
      ),
    );
  }

  ///
  ///Thông tin các chuyến bay
  buildTabBar() {
    return Container(
        // color: Colors.red,
        margin: EdgeInsets.only(left: 10, right: 10, bottom: 5),
        width: double.infinity,
        decoration: buildDecoration(),
        child: Column(
          children: [buildTabs(), buildTabsView()],
        ));
  }

  buildTabs() {
    return Container(
        padding: EdgeInsets.only(left: 10, right: 10),
        decoration: BoxDecoration(
            color: Colors.white.withOpacity(0.0),
            border: Border(
                bottom: BorderSide(
              color: Colors.grey,
            ))),
        child: TabBar(
            labelStyle: AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(
                fontSize: 50, fontWeight: FontWeight.bold, color: Colors.black),
            unselectedLabelColor: Colors.red,
            unselectedLabelStyle: AppStyles.DEFAULT_MEDIUM,
            isScrollable: true,
            indicatorPadding: EdgeInsets.symmetric(vertical: 15),
            labelColor: Colors.black,
            indicatorSize: TabBarIndicatorSize.label,
            // controller: _tabController,
            indicator: UnderlineTabIndicator(
              borderSide: BorderSide(
                width: 1.5,
                color: AppColors.red,
              ),
              insets: EdgeInsets.symmetric(horizontal: 37, vertical: 0),
            ),
            physics: NeverScrollableScrollPhysics(),
            labelPadding: EdgeInsets.only(
              top: 5,
              right: 5,
            ),
            tabs: List.generate(
              tabs.length,
                  (index) => Tab(
                // iconMargin: EdgeInsets.only(left: 50,right: 50),
                icon: SizedBox(
                  width: Get.width * 0.3,
                  child: Text(
                    tabs[index],
                    textAlign: TextAlign.center,
                    style: _viewModel.textStyle,
                  ),
                ),
              ),
            )));
  }

  buildTabsView() {
    return Flexible(
        fit: FlexFit.tight,
        child: TabBarView(
          // controller: _tabController,
          children: [buildDescription(), buildProcessing(),  buildProcessing()],
        ));
  }


  List<String> tabs = [
    "Nội dung tour",
    "Các chuyến đang  thực hiện",
    "Các chuyến đã  thực hiện",
  ];

  Decoration buildDecoration([Color color = Colors.white]) {
    return BoxDecoration(
        color: color,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 2,
            blurRadius: 5,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
        borderRadius: BorderRadius.circular(5));
  }

  ////Các chuyến đang thực hiện
  Widget buildProcessing() {
    var size = MediaQuery.of(context).size;
    return StreamBuilder<bool>(
        stream: _viewModel.checkController.stream,
        builder: (context, snapshot) {
          if (snapshot.data == null) {
            return Container();
          }
          return Column(
            children: [
              Expanded(
                  child: ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                itemCount: listTicket.length,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  return WidgetRow(
                    onTap: () async {
                      await _viewModel.checkSink(
                          listTicket[index].id, snapshot.data);
                    },
                    address: widget.address,
                    select: snapshot.data,
                    ticket: listTicket[index],
                    decoration: buildDecoration(Color(0xffADBFA3)),
                  );
                },
              )),
            ],
          );
        });
  }

  //// Nội dung tour
  Widget buildDescription() {
    return WidgetDescription(
      address: widget.address,
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    type = DeviceScreenType.desktop;
    // TODO: implement buildDesktop
    return _buildNestedScroll(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    type = DeviceScreenType.mobile;
    // TODO: implement buildDesktop
    return _buildNestedScroll(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    type = DeviceScreenType.tablet;
    // TODO: implement buildDesktop
    return _buildNestedScroll(context);
  }
}
