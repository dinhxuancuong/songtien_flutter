import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:song_tien_app/src/configs/configs.dart';
import '../../resource/resource.dart';
import '../base/base.dart';
import 'package:rxdart/rxdart.dart';
class DetailsTourViewModel extends BaseViewModel{

  final OtherRepository repository;

  // final tabsController = new BehaviorSubject<int>();
  final ticketController = new BehaviorSubject<Ticket>();
  final checkController = new BehaviorSubject<bool>();

  DetailsTourViewModel({@required this.repository});

  init()async{
    checkController.sink.add(false);
    ticketController.sink.add(null);
  }




   checkSink(int id, bool select) async {
    if (select) {
      checkController.sink.add(false);
    } else {
      checkController.sink.add(true);
    }
  }

  TextStyle textStyle =
  AppStyles.DEFAULT_SMALL_HIDE.copyWith(color: Colors.black38);



  List<Object> listTitle = [
    "05/02/2021",
    DateFormat("HH:mm").format(DateTime.now()),
    DateFormat("HH:mm").format(DateTime.now()),
    "B001 - Saigon Water taxi",
    "Bến Thủy Bạch Đằng",
    "Nguyễn Thị Huyền",
    true,
    "2.5000.000",
  ];

  List<String> listString = [
    "Ngày khởi hành :",
    "Giờ khởi hành:",
    "Giờ kết thúc:",
    "Mã số tàu (Tên):",
    "Địa điểm xuất bến:",
    "Người hướng dẫn:",
    "Trạng thái:",
    "Phí (giá vé):"
  ];

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    ticketController.close();
    checkController.close();
    // tabsController.close();
  }

}