import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:song_tien_app/src/presentation/client/client.dart';
import 'package:song_tien_app/src/presentation/trip/trip.dart';
import 'package:song_tien_app/src/presentation/widgets/widget_button_gradientAni.dart';
import 'package:song_tien_app/src/resource/model/app_valid.dart';
import 'package:song_tien_app/src/resource/model/model.dart';
import '../../configs/configs.dart';
import '../presentation.dart';

class ClientScreen extends StatefulWidget {
  final Ticket ticket;

  ClientScreen({Key key, this.ticket}) : super(key: key);

  @override
  _ClientScreenState createState() => _ClientScreenState();
}

ClientViewModel _viewModel;

class _ClientScreenState extends State<ClientScreen>
    with SingleTickerProviderStateMixin, ResponsiveWidget {
  DeviceScreenType type;
  @override
  Widget build(BuildContext context) {
    AppSizeConfig().init(context);
    return BaseWidget<ClientViewModel>(
        viewModel: ClientViewModel(repository: Provider.of(context)),
        onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
        builder: (context, viewModel, child) {
          return Scaffold(body: buildUi(context: context));
        });
  }

  _buildScreens(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          buildHeader(),
          SizedBox(
            height: 15,
          ),
          buildBody()
        ],
      ),
    );
  }

  buildBody() {
    return Column(
      children: [
        RichText(
            text: TextSpan(
                style: AppStyles.DEFAULT_MEDIUM
                    .copyWith(color: Colors.black87, fontSize: 17),
                children: [
              TextSpan(text: "Mã xác nhận đặt chỗ:"),
              TextSpan(text: "${widget.ticket.code??"---"}")
            ])),
        SizedBox(
          height: 5,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 10,right: 10),
          child: Divider(
            height: 25,
          ),
        ),
        WidgetDeclaration(
          ticket: widget.ticket,
          list: listString,
          select: false,
          textStyle: AppStyles.DEFAULT_MEDIUM
              .copyWith(fontSize: 16, color: Colors.black87),
        )
      ],
    );
  }

  buildHeader() {
    return Container(
      height: Get.height / 1 / 2,
      padding: EdgeInsets.all(15),
      width: double.infinity,
      color: AppColors.primary,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            height: 50,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Icon(
                Icons.arrow_back_ios,
                color: Colors.white,
                size: 18,
              ),
              Text(
                "Thông tin hành khách",
                style: AppStyles.DEFAULT_MEDIUM
                    .copyWith(color: Colors.white60, fontSize: 19),
              ),
              Container(),
            ],
          ),
          SizedBox(
            height: 40,
          ),Expanded(child:   widget.ticket.code == null?QRBorderPainter():QrImage(
            data: "1234567890",
            version: QrVersions.auto,
            size: 220.0,
            foregroundColor: Colors.white,
          ),)

        ],
      ),
    );
  }

  List<String> listString = [
    "Mã chuyến",
    "Mã chỗ ngồi",
    "Khách hàng",
    "Số điện thoại",
    "Người liên hệ",
    "Người đặt",
    "Thời gian đặt",
    "Trạng thái",
    "Người duyệt",
    "Thời gian duyệt",
    "Ghi chú"
  ];

  Decoration buildDecoration([Color color = Colors.white]) {
    return BoxDecoration(
        color: color,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.0),
            spreadRadius: 2,
            blurRadius: 5,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
        borderRadius: BorderRadius.circular(5));
  }

  @override
  Widget buildDesktop(BuildContext context) {
    type = DeviceScreenType.desktop;
    // TODO: implement buildDesktop
    return _buildScreens(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    type = DeviceScreenType.mobile;
    // TODO: implement buildDesktop
    return _buildScreens(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    type = DeviceScreenType.tablet;
    // TODO: implement buildDesktop
    return _buildScreens(context);
  }
}
