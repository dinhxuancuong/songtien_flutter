import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';
import 'package:song_tien_app/src/configs/configs.dart';
import '../presentation.dart';
import 'contact.dart';

class ContactScreen extends StatefulWidget {
  @override
  _ContactScreenState createState() => _ContactScreenState();
}

class _ContactScreenState extends State<ContactScreen> {
  ContactViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<ContactViewModel>(
      viewModel: ContactViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          body: SafeArea(
            child: Column(
              children: [
                Container(
                  height:80,
                  child: WidgetAppBar(
                    keyTitle: "contact",
                    isBack: false,
                  ),
                ),
                Expanded(child: _buildBody()),
              ],
            ),
          ),
        );
        ;
      },
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: Column(
        children: [
          Image.asset(
            AppImages.imgIsLand3,
            width: Get.width,
            fit: BoxFit.fill,
          ),
          const SizedBox(height: 15),
          WidgetLogo(),
          Padding(
            padding: const EdgeInsets.all(12),
            child: Column(
              children: [
                Text(
                  AppLocalizations.of(context).translate("more_info"),
                  style: AppStyles.DEFAULT_MEDIUM.copyWith(
                    color: AppColors.red,
                  ),
                  textAlign: TextAlign.center,
                ),
                const SizedBox(height: 20),
                _buildTitle(
                  translateTitle: "address_title",
                  description:
                      "Tầng 9, Toà Nhà HUBA, 22 Võ Văn\nKiệt, P. Nguyễn Thái Bình, Quận 1,\nHồ Chí Minh",
                ),
                const SizedBox(height: 10),
                _buildTitle(
                  translateTitle: "phone_title",
                  description: "(028) 7101 6686\n0906 360 888",
                ),
                const SizedBox(height: 10),
                _buildTitle(
                  translateTitle: "email_title",
                  description: "info@songtiencorp.com",
                ),
                const SizedBox(height: 20),
                Row(
                  children: [
                    _buildDot(),
                    Expanded(
                      flex: 2,
                      child: Text(
                        AppLocalizations.of(context).translate("chat_with_us"),
                        style: AppStyles.DEFAULT_MEDIUM.copyWith(
                          color: AppColors.red,
                          fontSize: 15,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    _buildDot(),
                  ],
                ),
                SizedBox(height: 10,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CircleAvatar(
                      child: Icon(
                        FlutterIcons.facebook_f_faw,
                        size: 27,
                        color: Colors.white,
                      ),
                      backgroundColor: Colors.indigo,
                      minRadius: 16,
                    ),
                    SizedBox(width: 15,),
                    CircleAvatar(
                      child: Icon(
                        FlutterIcons.facebook_messenger_faw5d,
                        size: 22,
                        color: Colors.white,
                      ),
                      backgroundColor: Colors.blue,
                      minRadius: 16,
                    ),
                    SizedBox(width: 15,),
                    CircleAvatar(
                      child: Icon(
                        FlutterIcons.logo_bitcoin_ion,
                        size: 22,
                        color: Colors.white,
                      ),
                      backgroundColor: Colors.blue,
                      minRadius: 16,
                    ),
                  ],
                )

              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildDot() {
    return Expanded(
      child: Wrap(
        spacing: 5,
        children: List.generate(
          (Get.width / 60).toInt(),
          (index) => Container(
            width: 9,
            height: 1.5,
            color: AppColors.black,
          ),
        ),
      ),
    );
  }

  Widget _buildTitle({String translateTitle, String description}) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          AppLocalizations.of(context).translate(translateTitle).toUpperCase(),
          style: AppStyles.DEFAULT_LARGE,
        ),
        Expanded(
          child: Text(
            description,
            style: AppStyles.DEFAULT_MEDIUM.copyWith(
              fontSize: 17,
            ),
            textAlign: TextAlign.end,
            maxLines: 3,
            overflow: TextOverflow.ellipsis,
          ),
        ),
      ],
    );
  }
}
