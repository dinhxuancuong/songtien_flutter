import 'package:flutter/material.dart';
import 'package:song_tien_app/src/presentation/details_news/details_news.dart';
import 'package:song_tien_app/src/presentation/login/login.dart';
import 'package:song_tien_app/src/presentation/news/news.dart';
import 'package:song_tien_app/src/presentation/notification/notification.dart';
import 'package:song_tien_app/src/presentation/otp/otp.dart';
import 'package:song_tien_app/src/presentation/password/password.dart';
import 'package:song_tien_app/src/presentation/place/place.dart';
import 'package:song_tien_app/src/presentation/place_infomation/place_infomation.dart';
import 'package:song_tien_app/src/presentation/policy/policy.dart';
import 'package:song_tien_app/src/presentation/presentation.dart';
import 'package:song_tien_app/src/presentation/register/register.dart';
import 'package:song_tien_app/src/presentation/tour/tour.dart';
import 'package:song_tien_app/src/presentation/trip/trip.dart';

import 'client/client.dart';
import 'details_notification/details_notification.dart';
import 'details_tour/details_tour.dart';
import 'navigation/navigation.dart';


class Routers {
  static const String Navigation = "/navigator";
  static const String User = "/users";
  static const String Account = "/account";
  static const String District = "/district";
  static const String Error = "/error";
  static const String Province = "/province";
  static const String Policy = "/policy";
  static const String Password = "/password";
  static const String  Notification = "/notification";
  static const String  NotificationDetails = "/notificationdetails";
  static const String  DetailsTour = "/detaistour";
  static const String  Tour = "/tour";
  static const String  Places = "/placescrren";
  static const String  PlaceIfomation = "/placeinfomation";
  static const String  Login = "/login";
  static const String  Otp = "/otp";
  static const String  Register = "/register";
  static const String  News = "/news";
  static const String  DetailsNews = "/detailsnews";
  static const String  Trip = "/trip";
  static const String  Client = "/client";



  static Route<dynamic> generateRoute(RouteSettings settings) {
    var arguments = settings.arguments;
    switch (settings.name) {
      case User:
        return animRoute(UserScreen(), name: User);
        break;
      case Account:
        return animRoute(AccountScreen(), name: Account);
        break;
      case District:
        return animRoute(DistrictScreen(), name: District);
        break;
      case Error:
        return animRoute(DialogError(), name: Error);
        break;
      case Province:
        return animRoute(ProvinceScreen(), name: Province);
        break;
      case Policy:
        return animRoute(PolicyScreen(), name: Policy);
        break;
      case Password:
        return animRoute(PasswordScreen(), name: Password);
        break;
      case Notification:
        return animRoute(NotificationScreen(), name: Notification);
        break;
      case NotificationDetails:
        return animRoute(DetailsNotificationScreen(person: arguments,), name: NotificationDetails);
        break;
      case DetailsTour:
        return animRoute(DetailsTourScreen(address: arguments,), name: DetailsTour);
        break;
      case PlaceIfomation:
        return animRoute(PlaceInformationScreen(ticket: arguments,), name: PlaceIfomation);
        break;
      case Navigation:
        return animRoute(NavigationScreen(user: arguments,), name: Navigation);
        break;
      case Login:
        return animRoute(LoginScreen(), name: Login);
        break;
      case Otp:
        return animRoute(OtpScreen(verifyArguments: arguments,), name: Otp);
        break;
      case Places:
        return animRoute(PlaceScreen(address: arguments,), name: Places,beginOffset: left);
        break;
      case Register:
        return animRoute(RegisterScreen(), name: Register);
        break;
      case News:
        return animRoute(NewsScreen(), name: News,beginOffset: left);
        break;
      case DetailsNews:
        return animRoute(DetailsNewsScreen(), name: DetailsNews,beginOffset: left);
        break;
      case Trip:
        return animRoute(TripScreen(address: arguments,), name: Trip,beginOffset: left);
        break;
      case Client:
        return animRoute(ClientScreen(ticket: arguments,), name: Client,beginOffset: left);
        break;
      case Tour:
        return animRoute(TourScreen(), name: Tour,beginOffset: left);
        break;
      default:
        return animRoute(Container(
            child:
                Center(child: Text('No route defined for ${settings.name}'))
          )
        );
    }
  }

  static Route animRoute(Widget page,
      {Offset beginOffset, String name, Object arguments}) {
    return PageRouteBuilder(
      settings: RouteSettings(name: name, arguments: arguments),
      pageBuilder: (context, animation, secondaryAnimation) => page,
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = beginOffset ?? Offset(0.0, 0.0);
        var end = Offset.zero;
        var curve = Curves.ease;
        var tween = Tween(begin: begin, end: end).chain(
          CurveTween(curve: curve),
        );

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }

  static Offset center = Offset(0.0, 0.0);
  static Offset top = Offset(0.0, 1.0);
  static Offset bottom = Offset(0.0, -1.0);
  static Offset left = Offset(-1.0, 0.0);
  static Offset right = Offset(1.0, 0.0);
}
