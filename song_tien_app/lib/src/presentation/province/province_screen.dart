import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import '../../configs/configs.dart';
import '../presentation.dart';
import 'package:dvhcvn/dvhcvn.dart' as dvhcvn;
import 'package:rxdart/rxdart.dart';
class ProvinceScreen extends StatefulWidget {

  @override
  _ProvinceScreenState createState() => _ProvinceScreenState();
}

class _ProvinceScreenState extends State<ProvinceScreen> {

  final TextEditingController provinceController = TextEditingController();
  final searchProvince = new BehaviorSubject<List<String>>();

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    searchProvince.close();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    provinceController.addListener(searchListProvince);
  }

ProvinceViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<ProvinceViewModel>(
        viewModel: ProvinceViewModel(repository: Provider.of(context)),
        onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
        builder: (context, viewModel, child) {
          return Scaffold(
            backgroundColor: Color(0xffbE8E8E8),
            body: Column(
              children: [
                SizedBox(
                  height:80,
                  child: WidgetAppBar(
                    keyTitle: "Tỉnh/ thành phố",
                    trans: false,
                  ),
                ),
                Expanded(child: _buildBody(context))
              ],
            ),
          );
        });
  }


  Widget _buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(
            height: 20,
          ),
          buildTextFiledProvince(),

          ///List Search province
          buildListProvince(),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }

  //// TextFiled
  buildTextFiledProvince(){
    return  WidgetTextField(
      hintText: "Tỉnh/ thành",
      controller: provinceController,
      // onSaved: (value) => _viewModel.text = value,
      radius: 7,
      mglr: 15,
      suffixIcon: InkWell(
        onTap: () async{
          await searchListProvince();},
        child: Container(
            decoration: BoxDecoration(
                color: Color(0xffA43732),
                border: Border.all(width: 0.1),
                borderRadius: BorderRadius.circular(10)),
            child: Icon(
              Icons.search_outlined,
              color: Colors.white,
              size: 35,
            )),
      ),
    );
  }

  searchListProvince()async{
    List<String> list = [];
    String key = "";
    key = provinceController.text.toLowerCase();

    for(var i = 0 ; i <dvhcvn.level1s.length;i++){
      String level = dvhcvn.level1s.elementAt(i).name;
      if(level.toLowerCase().contains(key)){
        list.add(level);
      }
    }
    await searchProvince.sink.add(list);
  }


///List Province
  buildListProvince(){
    return  StreamBuilder<List<String>>(
        stream: searchProvince.stream,
        builder: (context, snapshot) {
          print(snapshot.data);
          if(snapshot.data != null){
            return Wrap(
                children: snapshot.data.map((e) =>
                    WidgetTextField(
                      maxLines: null,
                      textStyle: AppStyles.TEXT_ADRESS,
                      readOnly: true,
                      onTap: (){
                        Navigator.pop(context,e);
                        // _viewModel.searchProvince.sink.add(e);
                        print(e);
                      },
                      radius: 7,
                      hintText: "${e}",
                      icon: Icon(
                        Icons.title,
                        color: Colors.black,
                      ),
                      mglr: 15,
                    )
                ).toList()
            );
          }else
            return Wrap(
              children: dvhcvn.level1s
                  .map((e) =>
                  WidgetTextField(
                    maxLines: null,
                    textStyle: AppStyles.TEXT_ADRESS,
                    readOnly: true,
                    radius: 7,
                    onTap: ()async{
                    Navigator.pop(context,e.name);
                    },
                    hintText: "${e.name}",
                    icon: Icon(
                      Icons.title,
                      color: Colors.black,
                    ),
                    mglr: 15,
                  ))
                  .toList(),
            );
        }
    );
  }

}
