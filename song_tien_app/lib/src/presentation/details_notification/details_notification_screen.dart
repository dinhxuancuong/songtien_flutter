import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:song_tien_app/src/presentation/details_notification/details_notification_viewmodel.dart';
import 'package:song_tien_app/src/presentation/widgets/widget_appbarr.dart';
import 'package:song_tien_app/src/presentation/widgets/widget_dotborder.dart';
import 'package:song_tien_app/src/resource/model/model.dart';
import '../../configs/configs.dart';
import '../presentation.dart';


class DetailsNotificationScreen extends StatefulWidget {
  final Person person;

  const DetailsNotificationScreen({Key key, this.person}) : super(key: key);
  @override
  _DetailsNotificationState createState() => _DetailsNotificationState();
}

DetailsNotificationViewModel _viewModel;

class _DetailsNotificationState extends State<DetailsNotificationScreen> {
  @override
  Widget build(BuildContext context) {
    return BaseWidget<DetailsNotificationViewModel>(
        viewModel:
            DetailsNotificationViewModel(repository: Provider.of(context)),
        onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
        builder: (context, viewModel, child) {
          return Scaffold(
            backgroundColor: Colors.black26,
            body: SafeArea(
              child: Column(
                children: [
                  SizedBox(
                    height:80,
                    child:   WidgetAppBar(keyTitle: "Chi Tiết Thông Báo",trans: false,),
                  ),
                  Flexible(child: _buildBody(context))
                ],
              ),
            ),
          );
        });
  }

  

  Widget _buildBody(BuildContext context) {
    return Container(
        // padding: const EdgeInsets.all(15.0),
        color: Color(0xffbE8E8E8),
        height: MediaQuery.of(context).size.height,
        width: double.infinity,
        margin: const EdgeInsets.all(15.0),
        child: WidgetDotBorder(
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: ClipRRect(
                        borderRadius: BorderRadius.circular(50),
                        child: Image.asset(
                          AppImages.imgIsLand2,
                          height: 100,
                          width: 250,
                        )),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Center(
                      child: Text(
                    "Tiếp nhận đơn đặt chỗ",
                    style: AppStyles.DEFAULT_MEDIUM_BOLD,
                  )),
                  SizedBox(
                    height: 15,
                  ),
                  Text(widget.person.content),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    "${DateFormat("dd/MM/yyyy - hh:mm").format(widget.person.dateTime)}",
                    style: TextStyle(color: Colors.black38),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
          ),
          gap: 5,
          drawBottomBorder: true,
          drawLeftBorder: true,
          drawRightBorder: true,
          drawTopBorder: true,
          color: Color(0xff5A7D48),
        ));
  }
}
