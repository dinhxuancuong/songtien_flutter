import 'package:flutter/material.dart';
import '../../resource/resource.dart';
import '../base/base.dart';

class DetailsNotificationViewModel extends BaseViewModel{

  final OtherRepository repository;
 int currentTab = 0;
  DetailsNotificationViewModel({@required this.repository});

  init()async{

  }


  swipingTab(TabController controller) {
    currentTab = (controller.animation.value).round();
    notifyListeners();
  }
}