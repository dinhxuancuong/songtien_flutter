import 'dart:math';

import 'package:flutter/material.dart';
import '../../resource/resource.dart';
import '../base/base.dart';

class DetailsNewsViewModel extends BaseViewModel{

  final OtherRepository repository;
  int currentTab = 0;
  DetailsNewsViewModel({@required this.repository});

  init()async{

  }
  final keyRefresh = GlobalKey<RefreshIndicatorState>();
  List<int> data = [];


  Future loadList() async {
    keyRefresh.currentState?.show();
    await Future.delayed(Duration(milliseconds: 4000));

    final random = Random();
    final data = List.generate(100, (_) => random.nextInt(100));

    this.data = data;
  }

  swipingTab(TabController controller) {
    currentTab = (controller.animation.value).round();
    notifyListeners();
  }
}