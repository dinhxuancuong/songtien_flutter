import 'dart:math';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:song_tien_app/src/presentation/details_news/details_news.dart';
import 'package:song_tien_app/src/resource/model/app_valid.dart';
import 'package:song_tien_app/src/resource/model/model.dart';
import '../../configs/configs.dart';
import '../presentation.dart';

class DetailsNewsScreen extends StatefulWidget {
  final Person person;

  const DetailsNewsScreen({Key key, this.person}) : super(key: key);
  @override
  _DetailsNewsState createState() => _DetailsNewsState();
}

DetailsNewsViewModel _viewModel;

class _DetailsNewsState extends State<DetailsNewsScreen> {


  DetailsNewsViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<DetailsNewsViewModel>(
        viewModel: DetailsNewsViewModel(repository: Provider.of(context)),
        onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
        builder: (context, viewModel, child) {
          return Scaffold(
            backgroundColor:AppColors.whiteTwo ,
            body: SafeArea(
              child: Column(
                children: [
                  SizedBox(height:80,child: WidgetAppBar(keyTitle: "detail_news",trans: true,),),
                 Expanded(child: _buildScreen())
                ],
              ),
            ),
          );
        });
  }

  _buildScreen(){
    return SingleChildScrollView(
        child:Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildHeader(),
            _buildBody(context)
          ],
        )
    );
  }

  _buildHeader(){
    return  ClipRRect(
      borderRadius: BorderRadius.circular(2),
      child: Image.asset(
        AppImages.imgLand2,
        height: Get.height * 0.3,
        width: Get.width,
        fit: BoxFit.fill,
      ),
    );
  }


  Widget _buildBody(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _rangeHeight(),
          Text("${listAdress[0].title}",textAlign: TextAlign.start,),
          _rangeHeight(),
          Text("${DateFormat("dd/MM/yyyy").format(DateTime.now())}",style: TextStyle(color: Colors.black38),),
          _rangeHeight(),
          Flexible(child: Text("${listString[0]}",style: TextStyle(letterSpacing: 0.3,),)),
          _rangeHeight(),
          Image.asset(AppImages.imgIsLand3,fit: BoxFit.fill,width: double.infinity,height: 300,),
          _rangeHeight(),
          Text("${listAdress[1].title}",textAlign: TextAlign.center,style: AppStyles.DEFAULT_MEDIUM.copyWith(color: Colors.black87,fontSize: 12),),
          _rangeHeight(),
          Flexible(child: Text("${listString[1]}",textAlign: TextAlign.start,)),
          _rangeHeight(height: 10),
        ],
      ),
    );
  }

  ////
  ///khoảng cách
  _rangeHeight({double height = 7}) => SizedBox(
    height: height,
  );

  _formNews(Address address, BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(context, Routers.DetailsNews),
      child: Padding(
        padding: const EdgeInsets.only(left: 14, right: 14, top: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Image.asset(
                address.images,
                height: Get.height * 0.3,
                width: Get.width,
                fit: BoxFit.fill,
              ),
            ),
            SizedBox(
              height: 7,
            ),
            Text(address.title),
            SizedBox(
              height: 3,
            ),
            Text("${DateFormat("dd/MM/yyyy").format(DateTime.now())}")
          ],
        ),
      ),
    );
  }


  Widget buildList() => _viewModel.data.isEmpty
      ? Center(child: CircularProgressIndicator())
      : WidgetRefresh(
    keyRefresh: _viewModel.keyRefresh,
    onRefresh: _viewModel.loadList,
    child: ListView.builder(
      shrinkWrap: true,
      primary: false,
      padding: EdgeInsets.all(16),
      itemCount: _viewModel.data.length,
      itemBuilder: (context, index) {
        final number = _viewModel.data[index];

        return buildItem(number);
      },
    ),
  );

  Widget buildItem(int number) => ListTile(
    title: Center(
      child: Text('$number', style: TextStyle(fontSize: 32)),
    ),
  );
}
