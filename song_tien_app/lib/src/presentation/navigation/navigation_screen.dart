import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:song_tien_app/src/configs/configs.dart';
import 'package:song_tien_app/src/presentation/contact/contact.dart';
import 'package:song_tien_app/src/presentation/home/home.dart';
import 'package:song_tien_app/src/presentation/tour/tour.dart';
import 'package:song_tien_app/src/presentation/widgets/widget_fade_transitions.dart';
import '../presentation.dart';
import 'navigation.dart';

class NavigationScreen extends StatefulWidget {
 final User user;

  const NavigationScreen({Key key, this.user}) : super(key: key);
  @override
  _NavigationScreenState createState() => _NavigationScreenState();
}

class _NavigationScreenState extends State<NavigationScreen> {
  NavigationViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<NavigationViewModel>(
      viewModel: NavigationViewModel(repository: Provider.of(context)),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return WidgetFadeTransitions(
          child: Scaffold(
            backgroundColor: Colors.white,
            body: Column(
              children: [
                Expanded(child: _buildPages()),
                _buildBottomNavBar(),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildBottomNavBar() {
    return Container(
      width: Get.width,
      color: AppColors.primary,
      padding: EdgeInsets.only(top: 12, bottom: 12),
      child: Row(
        children: [
          _buildButton(
            index: 0,
            iconData: Icons.home,
            translateText: "home",
          ),
          _buildButton(
            index: 1,
            iconData: Icons.touch_app_outlined,
            translateText: "tour",
          ),
          _buildButton(
            index: 2,
            iconData: Icons.call,
            translateText: "contact",
          ),
          _buildButton(
            index: 3,
            iconData: Icons.perm_contact_cal_rounded,
            translateText: "account",
          ),
        ],
      ),
    );
  }


  Widget _buildButton({int index, IconData iconData, String translateText}) {
    return Expanded(
      child: InkWell(
        onTap: () => _viewModel.switchPage(index),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Icon(
              iconData,
              size: 30,
              color: _viewModel.currentPage == index ? AppColors.white : Colors.white60,
            ),
            Text(
              AppLocalizations.of(context).translate(translateText),
              style: AppStyles.DEFAULT_MEDIUM.copyWith(fontSize: 10,
                color: _viewModel.currentPage == index ? AppColors.white : Colors.white60,
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildPages() {
    switch (_viewModel.currentPage) {
      case 0:
        return HomeScreen();
      case 1:
        return TourScreen();
      case 2:
        return ContactScreen();
      case 3:
        return AccountScreen(user: widget.user,);
      default:
        return SizedBox();
    }
  }
}