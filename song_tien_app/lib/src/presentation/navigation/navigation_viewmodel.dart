import 'package:song_tien_app/src/resource/model/model.dart';
import 'package:song_tien_app/src/resource/repo/other_repository.dart';

import '../base/base.dart';

class NavigationViewModel extends BaseViewModel{

  int currentPage = 0;
  final OtherRepository repository;

  NavigationViewModel({@required this.repository});

  init(){}
  switchPage(int index) {
    currentPage = index;
    notifyListeners();
  }


  }



