import 'package:flutter/cupertino.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:song_tien_app/src/configs/configs.dart';
import 'package:song_tien_app/src/resource/resource.dart';
import 'package:song_tien_app/src/utils/app_shared.dart';
import 'package:toast/toast.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import '../presentation.dart';
import 'package:firebase_auth/firebase_auth.dart';

class LoginViewModel extends BaseViewModel {


  TextEditingController phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();

  GlobalKey<FormState> formSignIn = GlobalKey<FormState>();

  bool obSecureText = true;
  bool isSignIn = true;
  FirebaseAuth firebaseAuth = FirebaseAuth.instance;

  final googleSignIn = GoogleSignIn();
  final facebookLogin = FacebookLogin();



  init() {

  }

  switchScreen() {
    obSecureText = true;
    // isSignIn = !isSignIn;
    phoneController.clear();
    passwordController.clear();
    confirmPasswordController.clear();
    Navigator.pushNamed(context, Routers.Register);
    notifyListeners();
  }

  showPassword() {
    obSecureText = !obSecureText;
    notifyListeners();
  }



  signIn() {
    if (formSignIn.currentState.validate()) {
      handleLogin(
        phone: this.phoneController.text,
        password: this.passwordController.text,
      );
    }
  }


  void handleLogin({
    String accessToken,
    // LoginType type,
    User user,
    String password,
    String phone,
  }) async {
    try {
      setLoading(true);
      print(accessToken);
      print(user.photoURL);
      print(user.email);
      print(user.displayName);
      print(password);
      print(user.uid);
      print(phone);
      EasyLoading.show(status: 'loading...');
      await Future.delayed(Duration(milliseconds: 200),(){
        EasyLoading.dismiss();
        Toast.show("Chào mừng ${user.displayName}", context);
        Navigator.pushNamedAndRemoveUntil(
            context, Routers.Navigation,(router) => false,arguments: user);
      });


    } catch (e) {
      setLoading(false);
      print("out");
    }
  }


  void isDisconnect()async{
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect){setLoading(false); return await Future.delayed(Duration(milliseconds: 500), () {
      Toast.show(AppLocalizations.of(context).translate("disconnection"), context);
    });}
  }

  //
  void loginGoogleFirebase() async {
    isDisconnect();
    GoogleSignIn _googleSignIn = GoogleSignIn(
      scopes: ['email'],
    );
    try {
      GoogleSignInAccount account;
      account = await _googleSignIn.signIn();
      final GoogleSignInAuthentication googleAuth =
          await account.authentication;

      final credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
      AppShared.setAccessToken(googleAuth.accessToken);
      var user = await FirebaseAuth.instance.signInWithCredential(credential);
      if (user.user != null) {
        setLoading(true);
        assert(!user.user.isAnonymous);
        assert(await user.user.getIdToken() != null);

        final User currentUser = firebaseAuth.currentUser;
        assert(user.user.uid == currentUser.uid);

       return handleLogin(

          // type: LoginType.google,
          accessToken: googleAuth.accessToken,
           user: user.user
        );
      } else {
        setLoading(false);
      }
    } catch (error) {
      setLoading(false);
    }
  }


  void logoutGoogle() async {;
    await FirebaseAuth.instance.signOut();
  }


  Future<void> loginFaceBookFireBase() async {
    isDisconnect();
    final FacebookLoginResult result = await facebookLogin.logIn(['email']);
    try {
      setLoading(true);
      switch (result.status) {
        case FacebookLoginStatus.cancelledByUser:
          break;
        case FacebookLoginStatus.error:
          break;
        case FacebookLoginStatus.loggedIn:
          AppShared.setAccessToken(result.accessToken.token);
          try {
            await loginWithFaceBook(result);;
          } catch (e) {
            print(e);
          }
          break;
      }
    } catch (error) {
      print(error.toString());
      await handleFailure(msg: error.toString());
      setLoading(false);
    }


  }

  Future loginWithFaceBook(FacebookLoginResult result) async {
    FirebaseAuth _fireBase = FirebaseAuth.instance;
    AuthCredential credential =
        FacebookAuthProvider.credential(result.accessToken.token);
    var user = await _fireBase.signInWithCredential(credential);

  return  handleLogin(
      accessToken: result.accessToken.token,
       user: user.user,
       phone: user.user.phoneNumber
    );
    notifyListeners();
  }


  Future<dynamic> showNotification(
      {@required String keyTitle, bool trans = true, String keyAction, Function action}) async {
    await showDialog(
      context: context,
      builder: (context) => DialogError(
        keyTitle: keyTitle,
        action: action,
        trans: trans,
        keyAction: keyAction,
      ),
    );
  }
  Future handleFailure({String msg}) async {
    await showNotification(
      keyTitle: msg ?? "login_failure",
      trans: msg!=null,
    );
  }

  // Future<void> handleRegister({
  //   String phone,
  //   String password,
  // }) async {
  //   loadingSubject.add(true);
  //   final NetworkState<LoginResponse> networkState = await authRepository.registerByPhone(
  //     password: password,
  //   );
  //
  //   if (networkState.isSuccess && networkState.data != null) {
  //     await showNotification(keyTitle: "register_success");
  //     switchScreen();
  //   } else {
  //     await showNotification(keyTitle: "register_failure");
  //   }
  //
  //   loadingSubject.add(false);
  // }

  @override
  void dispose() async {
    phoneController.dispose();
    passwordController.dispose();
    super.dispose();
  }
}
