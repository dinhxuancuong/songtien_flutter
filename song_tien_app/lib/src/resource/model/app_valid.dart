export 'package:flutter/material.dart';
enum ValidateType{
  Email,
  Password,
  Number,
  EmailNumber
}

class AppValid{
  static checkValidate(ValidateType validateType,String value){
    switch(validateType){
      case ValidateType.EmailNumber:
        final pattern = r'(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)';
        final number = r'(^(?:[+0]9)?[0-9]{10,12}$)';
        final regExp = RegExp(pattern);
        final  RegExp regNumber = new RegExp(number);
        if (!regExp.hasMatch(value) || !regNumber.hasMatch(value)) {
          return 'Email hoặc số điện thoại không hợp lệ';
        } else {
          return null;
        }
        break;
      case ValidateType.Password:
        if (value.isEmpty || value.length < 7) {
          return 'Password must be at least 7 characters long.';
        } else {
          return null;
        }
        break;
      case ValidateType.Number:
        final number = r'(^(?:[+0]9)?[0-9]{10,12}$)';
        final  RegExp regNumber = new RegExp(number);
        if (!regNumber.hasMatch(value)) {
          return 'Số điện thoại không hợp lệ';
        } else {
          return null;
        }
        break;
      case ValidateType.Email:
        final email = r'(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)';
        final  RegExp regEmail = new RegExp(email);
        if (!regEmail.hasMatch(value)) {
          return 'Email không hợp lệ';
        } else {
          return null;
        }
        break;
    }
  }
}
