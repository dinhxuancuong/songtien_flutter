import 'package:flutter/material.dart';
import 'package:song_tien_app/src/configs/configs.dart';

class Person {
  final String title;
  final String content;
  final DateTime dateTime;

  Person(this.title, this.content, this.dateTime);
}

List<Person> listData =
    listKey.map((e) => Person(e["name"], e["content"], e["datetime"])).toList();

List listKey = [
  {
    "name": "Tiếp nhận đơn đặt tại chỗ",
    "content": "Lorem Ipsum"
        "is simply dummy text of the printing and "
        "typesetting industry. Lorem Ipsum has been the"
        " industry's standard dummy text ever since the"
        " 1500s, when an unknown printer took a galley of "
        "type and scrambled it to make a type specimen book."
        " It has survived not only five centuries, but also the "
        "leap into electronic typesetting, remaining essentially"
        " unchanged. It was popularised in the 1960s with the release "
        "of Letraset sheets containing Lorem Ipsum passages, and more "
        "recently with desktop publishing software like Aldus PageMaker "
        "including versions of Lorem Ipsum."
        "is simply dummy text of the printing and "
        "typesetting industry. Lorem Ipsum has been the"
        " industry's standard dummy text ever since the"
        " 1500s, when an unknown printer took a galley of "
        "type and scrambled it to make a type specimen book."
        " It has survived not only five centuries, but also the "
        "leap into electronic typesetting, remaining essentially"
        " unchanged. It was popularised in the 1960s with the release "
        "of Letraset sheets containing Lorem Ipsum passages, and more "
        "recently with desktop publishing software like Aldus PageMaker "
        "including versions of Lorem Ipsum.",
    "datetime": DateTime.now()
  },
  {
    "name": "Thay đổi lịch đặt chỗ",
    "content": "It is a long established fact that a reader will be distracted"
        " by the readable content of a page when looking at its layout. The "
        "point of using Lorem Ipsum is that it has a more-or-less normal "
        "distribution of letters, as opposed to using 'Content here, content"
        " here', making it look like readable English. Many desktop publishing "
        "packages and web page editors now use Lorem Ipsum as their default model"
        " text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. "
        "Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
    "datetime": DateTime.now()
  },
  {
    "name": "Thông tin đặt chỗ",
    "content": "Contrary to popular belief, Lorem Ipsum is not simply"
        " random text. It has roots in a piece of classical Latin literature"
        " from 45 BC, making it over 2000 years old. Richard McClintock, a Latin"
        " professor at Hampden-Sydney College in Virginia, looked up one of the "
        "more obscure Latin words, consectetur, from a Lorem Ipsum passage,"
        " and going through the cites of the word in classical literature, discovered the undoubtable source",
    "datetime": DateTime.now()
  },
  {
    "name": "Tiều đề thông báo",
    "content": "There are many variations of passages"
        " of Lorem Ipsum available, but the majority have suffered alteration in some "
        "form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a"
        " passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text",
    "datetime": DateTime.now()
  },
  {
    "name": "Tiều đề thông báo",
    "content": "There are many variations of passages"
        " of Lorem Ipsum available, but the majority have suffered alteration in some "
        "form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a"
        " passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text",
    "datetime": DateTime.now()
  },
  {
    "name": "Tiều đề thông báo",
    "content": "There are many variations of passages"
        " of Lorem Ipsum available, but the majority have suffered alteration in some "
        "form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a"
        " passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text",
    "datetime": DateTime.now()
  }
];

class Address {
  final String images;
  final String title;
  final double raitting;
  final String code;
  final String category;
  final String content;

  Address(this.images, this.title, this.raitting, this.code, this.category,
      this.content);
}

List<String> listString = [
  "Lorem Ipsum"
      "is simply dummy text of the printing and "
      "typesetting industry. Lorem Ipsum has been the"
      " industry's standard dummy text ever since the"
      " 1500s, when an unknown printer took a galley of "
      "type and scrambled it to make a type specimen book."
      " It has survived not only five centuries, but also the "
      "leap into electronic typesetting, remaining essentially"
      " unchanged. It was popularised in the 1960s with the release "
      "of Letraset sheets containing Lorem Ipsum passages, and more "
      "recently with desktop publishing software like Aldus PageMaker "
      "including versions of Lorem Ipsum. Lorem Ipsum is that it has a more-or-less normal "
      "distribution of letters, as opposed to using 'Content here, content"
      " here', making it look like readable English. Many desktop publishing "
      "packages and web page editors now use Lorem Ipsum as their default model"
      " text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. "
      "Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",

  "Contrary to popular belief, Lorem Ipsum is not simply Lorem Ipsum is that it has a more-or-less normal "
      "distribution of letters, as opposed to using 'Content here, content"
      " here', making it look like readable English. Many desktop publishing "
      "packages and web page editors now use Lorem Ipsum as their default model"
      " text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. "
      "Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
  "Contrary to popular belief, Lorem Ipsum is not simply",
  "It is a long established fact that a reader will be distracted"
      " by the readable content of a page when looking at its layout. The "
      "point of using Lorem Ipsum is that it has a more-or-less normal "
      "distribution of letters, as opposed to using 'Content here, content"
      " here', making it look like readable English. Many desktop publishing "
      "packages and web page editors now use Lorem Ipsum as their default model"
      " text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. "
      "Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
  "Contrary to popular belief, Lorem Ipsum is not simply"
      " random text. It has roots in a piece of classical Latin literature"
      " from 45 BC, making it over 2000 years old. Richard McClintock, a Latin"
      " professor at Hampden-Sydney College in Virginia, looked up one of the "
      "more obscure Latin words, consectetur, from a Lorem Ipsum passage,"
      " and going through the cites of the word in classical literature, discovered the undoubtable source",
  "There are many variations of passages"
      " of Lorem Ipsum available, but the majority have suffered alteration in some "
      "form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a"
      " passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text",
  "There are many variations of passages"
      " of Lorem Ipsum available, but the majority have suffered alteration in some "
      "form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a"
      " passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text",
  "There are many variations of passages"
      " of Lorem Ipsum available, but the majority have suffered alteration in some "
      "form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a"
      " passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text",
  "There are many variations of passages"
      " of Lorem Ipsum available, but the majority have suffered alteration in some "
      "form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a"
      " passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text",
];

List<Address> listAdress = [
  Address(AppImages.imgIsland1, "Biệt thự đảo - Xu hướng mới trên thị trường bất động sản siêu sang ", 4,
      "T001", "VIP", listString[0]),
  Address(
      AppImages.imgIsLand2,
      "Tham quan vòng quanh đảo Angel Island bằng du thuyền",
      5,
      "T002",
      "Thường",
      listString[1]),
  Address(AppImages.imgIsLand3, "Tham quan đảo Angelisland bằng du thuyền", 4,
      "T001", "VIP", listString[2]),
  Address(AppImages.imgIsland1, "Tham quan đảo Angelisland bằng du thuyền", 4,
      "T001", "VIP", listString[3]),
  Address(
      AppImages.imgIsLand2,
      "Tham quan vòng quanh đảo Angel Island bằng du thuyền",
      5,
      "T002",
      "Thường",
      listString[4]),
  Address(AppImages.imgIsLand3, "Tham quan đảo Angelisland bằng du thuyền", 4,
      "T001", "VIP", listString[5]),
];

class Ticket {
  int id;
   String departureDay;
   String departureTime;
   String TimeEnd;
   String codeName;
   String address;
   String instructorName;
   bool check;
   String pay;
   String code;
   String title;

  Ticket(this.id,this.departureDay, this.departureTime, this.TimeEnd, this.codeName, this.address, this.instructorName, this.check, this.pay, this.code, this.title);

}

List<Ticket> listTicket = [
  Ticket(
       1,
      "05/02/2021",
       "14:00",
       "18:00",
      "B001 - Saigon Water taxi",
      "Bến Thủy Bạch Đằng",
      "Nguyễn Thị Huyền",
      true,
      "2.5000.000",
      "C00000000001",
      "Chuyến tham quan"),
  Ticket(
      1,
      "05/02/2021",
      "14:00",
      "18:00",
      "B001 - Saigon Water taxi",
      "Bến Thủy Bạch Đằng",
      "Nguyễn Thị Huyền",
      true,
      "2.5000.000",
      "C00000000001",
      "Chuyến tham quan"),


];
