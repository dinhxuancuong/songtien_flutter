import 'dart:io';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:image/image.dart' as ImD;
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:song_tien_app/src/resource/model/user.dart';
import 'package:song_tien_app/src/utils/app_utils.dart';
import 'package:uuid/uuid.dart';

import '../resource.dart';

class UploadPage extends StatefulWidget {
  User user;

  UploadPage({this.user});

  @override
  _UploadPageState createState() => _UploadPageState();
}

class _UploadPageState extends State<UploadPage> with AutomaticKeepAliveClientMixin<UploadPage> {
  TextEditingController description = TextEditingController();
  TextEditingController location = TextEditingController();
  File imageFile;

  bool uploading = false;
  String postId = Uuid().v4();
  String imageUrl;

  compressingPhoto() async {
    final directory = await getTemporaryDirectory();
    final path = directory.path;

    ImD.Image image = ImD.decodeImage(imageFile.readAsBytesSync());
    final compressedImageFile = File('$path/img_$postId.jpg')..writeAsBytesSync(ImD.encodeJpg(image, quality: 60));



    setState(() {
      imageFile = compressedImageFile;
    });
  }

  controlUploadAndSave() async {
    setState(() {
      uploading = true;
    });

    await compressingPhoto();
    AppUtils.UploadAndSaveImage(imageFile,"profiles$postId.jpg")
        .then((downloadUrl) => imageUrl = downloadUrl);
    // savePostInfoToFireStore(url: imageUrl, location: location.text, description: description.text);

    location.clear();
    description.clear();

    setState(() {
      imageFile = null;
      uploading = false;
      postId = Uuid().v4();
    });
  }

  // savePostInfoToFireStore({String url, String location, String description}) async {
  //   await AppUtils.firestore
  //       .collection("posts")
  //       .doc(widget.user.id)
  //       .collection("userPosts")
  //       .doc(postId)
  //       .set({
  //     "postId": postId,
  //     "ownerId": widget.user.id,
  //     "timestamp": DateTime.now(),
  //     "likes": {},
  //     "username": widget.user.username,
  //     "description": description,
  //     "location": location,
  //     "url": url,
  //   });
  // }

  captureImageWithCamera() async {
    Navigator.pop(context);
    File file = await ImagePicker.pickImage(source: ImageSource.camera, maxWidth: 970, maxHeight: 680);
    setState(() {
      imageFile = file;
    });
  }

  pickerImage() async {
    Navigator.pop(context);
    File file = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      imageFile = file;
    });
  }

  takeImage(context) {
    return showDialog(
      context: context,
      builder: (context) {
        return SimpleDialog(
          title: Text("New Post", style: TextStyle(color: Colors.black87, fontWeight: FontWeight.bold)),
          children: [
            SimpleDialogOption(
              child: Text("Capture Image with Camera", style: TextStyle(color: Colors.black87)),
              onPressed: () => captureImageWithCamera(),
            ),
            SimpleDialogOption(
              child: Text("Select Image from Gallery", style: TextStyle(color: Colors.black87)),
              onPressed: () => pickerImage(),
            ),
            SimpleDialogOption(
              child: Text("Cancel", style: TextStyle(color: Colors.black87)),
              onPressed: () => Navigator.pop(context),
            ),
          ],
        );
      },
    );
  }

  displayUploadScreen() {
    return Container(
      color: Theme.of(context).accentColor.withOpacity(0.5),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(Icons.add_photo_alternate, color: Colors.grey, size: 200),
          Padding(
            padding: EdgeInsets.only(top: 20),
            child: RaisedButton(
              onPressed: () => takeImage(context),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
              child: Text("Upload Images", style: TextStyle(color: Colors.white, fontSize: 20)),
              color: Colors.green,
            ),
          )
        ],
      ),
    );
  }

  displayUploadFormScreen() {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () {
            location.clear();
            description.clear();

            setState(() {
              imageFile = null;
            });
          },
        ),
        title: Text("New Post", style: TextStyle(fontSize: 24, color: Colors.white, fontWeight: FontWeight.bold)),
        centerTitle: true,
        actions: [
          FlatButton(
            onPressed: () => uploading ? null : controlUploadAndSave(),
            child: Text(
              "Share",
              style: TextStyle(color: Colors.lightGreenAccent, fontWeight: FontWeight.bold, fontSize: 16),
            ),
          )
        ],
      ),
      body: ListView(
        children: [
          uploading ? AppUtils.LinearProgress() : Container(),
          Container(
            height: 230,
            width: AppUtils.Width(context) * 0.8,
            child: Center(
              child: AspectRatio(
                aspectRatio: 16 / 9,
                child: Container(
                    decoration: BoxDecoration(image: DecorationImage(image: FileImage(imageFile), fit: BoxFit.cover)),
                ),
              ),
            ),
          ),
          Padding(padding: EdgeInsets.only(top: 12)),
          ListTile(
            // leading: CircleAvatar(backgroundImage: CachedNetworkImageProvider(widget.user.url??"")),
            title: Container(
              width: 250,
              child: TextField(
                style: TextStyle(color: Colors.white),
                controller: description,
                decoration: InputDecoration(
                  hintText: "Say something about image.",
                  hintStyle: TextStyle(color: Colors.white),
                  border: InputBorder.none,
                ),
              ),
            ),
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.person_pin, color: Colors.white, size: 36),
            title: Container(
              width: 250,
              child: TextField(
                style: TextStyle(color: Colors.white),
                controller: location,
                decoration: InputDecoration(
                  hintText: "Write the lcoation here.",
                  hintStyle: TextStyle(color: Colors.white),
                  border: InputBorder.none,
                ),
              ),
            ),
          ),
          Container(
            width: 220,
            height: 110,
            alignment: Alignment.center,
            child: RaisedButton.icon(
              onPressed: () => getUserCurrentLocation(),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(35)),
              color: Colors.green,
              icon: Icon(Icons.location_on, color: Colors.white),
              label: Text("Get my Current Location", style: TextStyle(color: Colors.white)),
            ),
          ),
        ],
      ),
    );
  }

  getUserCurrentLocation() async {
    await AppUtils.GetUserCurrentLocation().then((address) {
      setState(() {
        print(address);
        location.text = address;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return imageFile == null ? displayUploadScreen() : displayUploadFormScreen();
  }

  @override
  bool get wantKeepAlive => true;
}
