import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


typedef providerCall = void Function();

enum TableStatus {
  EMPTY,
  BOOKED,
  CAME,
  SAME
}

class Furniture {
  final int Id;
  final int chairsSlot;
  final TableStatus status;
  final int booked;
  final providerCall add;
  Furniture({this.Id,@required this.chairsSlot, @required this.status, this.booked = 0, this.add});


}

var FAKE_FURNITUREDATATWO = [
  Furniture(Id:1,chairsSlot: 4,status: TableStatus.BOOKED,booked: 4),
  Furniture(Id:2,chairsSlot: 4,status: TableStatus.BOOKED,booked: 7),
  Furniture(Id:3,chairsSlot: 4,status: TableStatus.EMPTY,booked: -4),
  Furniture(Id:4,chairsSlot: 6,status: TableStatus.EMPTY,booked: 1),
  Furniture(Id:5,chairsSlot: 11,status: TableStatus.EMPTY,booked: -1),
  Furniture(Id:6,chairsSlot: 2,status: TableStatus.CAME,booked: -1),


];