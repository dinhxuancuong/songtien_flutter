import 'package:flutter/material.dart';

class AppColors {
  AppColors._();

  static final Color primary = Color(0xff445E37);
  // Color.fromRGBO(246, 133, 14, 1);
  static final Color grey = Colors.grey[300];
  static final Color black = Colors.black87;
  static final Color black26 = Colors.black26;
  static final Color enableButton = Color.fromRGBO(246, 133, 14, 1);
  static final Color disableButton = Color.fromRGBO(246, 133, 14, 0.65);
  static final Color red = Colors.red;
  static final Color white = Colors.white;
  static final Color primaryLight = Color(0xffC0C0C0);
  static final Color whiteTwo = Color(0xffE8E8E8);
  static final Color whiteThree = Color(0xffDEDEDE);
}
