class AppImages {
  AppImages._();

  static final String icMoon = 'assets/images/ic_moon.png';
  static final String imgPeople = "assets/images/anh-dai-dien-FB-200.jpg";
  static final String imgBackgrounder = "assets/images/Splash.png";
  static final String imgIsland1 = 'assets/images/Angel-island-2.jpg';
  static final String imgIsLand2 = "assets/images/isanledongnai.jpg";
  static final String imgIsLand3 = "assets/images/ISLAND.jpg";
  static final String imgSongTien = "assets/images/songtien.png";
  static final String imgSongTien2 = "assets/images/khu-do-thi-angel-island-4.png";
  static final String imgChair = "assets/images/chair (3).png";
  static final String icFaceBook = "assets/icons/facebook.png";
  static final String icGoggle = "assets/icons/google.png";
  static final String imgLogo= "assets/images/songtien.png";
  static final String imgLand1= "assets/images/Angel-island-song-tien-nhon-phuoc-nhon-trach-12-9-2.jpg";
  static final String imgLand2= "assets/images/angel-island-song-tien-nhon-trach-dong-nai-25.jpg";
  static final String imgLand3= "assets/images/du-an-angel-island-15.jpg";
}
