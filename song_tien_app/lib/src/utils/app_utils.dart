import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:geocoding/geocoding.dart';

class AppUtils {
  // enable it if you have use firebase
  //static SharedPreferences sharedPreferences;
  static GoogleSignIn googleSignIn;
  static User  firebaseUser;
  static FirebaseAuth firebaseAuth;
  static FirebaseFirestore  firestore;

  static Width(BuildContext context) => MediaQuery.of(context).size.width;

  static Height(BuildContext context) => MediaQuery.of(context).size.height;

  // fuction of Dialog
  static AlertDialog ErrorAlertDialog(BuildContext context, String message) {
    return AlertDialog(
      content: Text(message),
      actions: <Widget>[
        RaisedButton(
          onPressed: () => Navigator.pop(context),
          color: Colors.red,
          child: Center(child: Text("OK")),
        ),
      ],
    );
  }

  static AlertDialog LoadingAlertDialog(BuildContext context, String message) {
    return AlertDialog(
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          CircularProgress(Colors.lightGreen),
          SizedBox(height: 10),
          Text(message),
        ],
      ),
    );
  }

  // function of Show Loading
  static CircularProgress([Color colorAnimation = Colors.lightBlueAccent]) {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.only(top: 12),
      child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation(colorAnimation)),
    );
  }

  static LinearProgress([Color colorAnimation = Colors.lightBlueAccent]) {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.only(top: 12),
      child: LinearProgressIndicator(valueColor: AlwaysStoppedAnimation(colorAnimation)),
    );
  }

  // function of Gradient
  static BoxDecoration BackgroundGradient(
      [List<Color> colors,
        bool lerpOrPosition = true,
        Alignment beginAlign = Alignment.topRight,
        Alignment endAlign = Alignment.bottomLeft,
        double start = 0,
        double end = 1,
        TileMode tileMode = TileMode.clamp]) {
    colors ??= [Colors.pink, Colors.lightGreenAccent];
    return new BoxDecoration(
      gradient: new LinearGradient(
        colors: colors,
        begin: lerpOrPosition ? FractionalOffset(start, start) : beginAlign,
        end: lerpOrPosition ? FractionalOffset(end, start) : endAlign,
        stops: lerpOrPosition ? [start, end] : null,
        tileMode: tileMode,
      ),
    );
  }

  // function of Navigator
  static ScreenSwitcher(BuildContext context, Widget screen, bool pushOrPushReplacement, [bool pop = false]) {
    if (pop) Navigator.pop(context);
    Route route = MaterialPageRoute(builder: (_) => screen);
    if (pushOrPushReplacement)
      Navigator.push(context, route);
    else
      Navigator.pushReplacement(context, route);
  }

  // function of Save Image to firebase
  static Future<String> UploadAndSaveImage(File imageFile, String nameStorage) async {
     final Reference  storageReference = FirebaseStorage.instance.ref().child(nameStorage);
     UploadTask  uploadTask = storageReference.putFile(imageFile);
    TaskSnapshot taskSnapshot = await uploadTask.snapshot;
    String downloadUrl = await taskSnapshot.ref.getDownloadURL();
    return downloadUrl;
  }

  // function of Header-AppBar
  static AppBar HeaderPage(BuildContext context, String nameTitle,
      [bool disableBackBtn = false, bool centerTitle = true, double sizeTitle = 55]) {
    return AppBar(
      iconTheme: IconThemeData(color: Colors.white),
      automaticallyImplyLeading: disableBackBtn ? false : true,
      title: Text(
        nameTitle,
        style: TextStyle(fontSize: sizeTitle, color: Colors.white, fontFamily: "Signatra"),
        overflow: TextOverflow.ellipsis,
      ),
      centerTitle: centerTitle,
      backgroundColor: Theme.of(context).accentColor,
    );
  }

  // function of Location
  static Future<String> GetUserCurrentLocation() async {
    Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    List<Placemark> placeMarks = await placemarkFromCoordinates(position.latitude, position.longitude);
    Placemark placemark = placeMarks[0];
    String completeAddressInfo = '${placemark.subThoroughfare} ${placemark.thoroughfare}, '
        '${placemark.subLocality} ${placemark.locality}, '
        '${placemark.subAdministrativeArea} ${placemark.administrativeArea}, '
        '${placemark.postalCode} ${placemark.country}';
    String specificAddress = '${placemark.locality}, ${placemark.country}';
    return specificAddress;
  }

  ///function of Firestore
  static DocumentReference Document(String collection, String document, [String twoCollection = "", bool twoDoc = false]) {
    return (twoDoc)
        ? AppUtils.firestore.collection(collection).doc(document)
        : AppUtils.firestore.collection(twoCollection);
  }
}

class CustomTextField extends StatelessWidget {
  final TextEditingController controller;
  final IconData data;
  final String hintText;
  final String errorText;
  bool isObsecure = true;

  CustomTextField({Key key, this.controller, this.data, this.hintText, this.isObsecure, this.errorText}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(10))),
      padding: EdgeInsets.all(8),
      margin: EdgeInsets.all(10),
      child: TextFormField(
        validator: (value) {
          if (value.trim().length < 5 || value.isEmpty) return errorText;
        },
        controller: controller,
        obscureText: isObsecure,
        cursorColor: Theme.of(context).primaryColor,
        decoration: InputDecoration(
          border: InputBorder.none,
          prefixIcon: Icon(data, color: Theme.of(context).primaryColor),
          focusColor: Theme.of(context).primaryColor,
          hintText: hintText,
        ),
      ),
    );
  }
}
